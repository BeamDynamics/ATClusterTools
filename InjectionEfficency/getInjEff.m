function [injeff,beamcentersdistance,lostonsept,datafolder]=getInjEff(...
    ring,epsx,epsz,Npart,Nturns,Nproc,addlabel,DeltaOptics,seed,bl,es)
%function [injeff,beamcentersdistance,lostonsept,datafolder]=...
%               getInjEff(ring,...
%    epsx,epsz,Npart,Nturns,Nproc,addlabel,DeltaOptics)
% computes injection efficiency
%
% Inputs:
% ring        : AT lattice
% epsx        : emittance injected beam (default 60nm)
% epsy        : emittance vertical injected beam (default 5nm)
% Npart       : number of particles (default 1000)
% Nturns      : number of turns (default 2048)
% Nproc       : number of Cluster jobs (default 50)
% addlabel    : label for output folder
% DeltaOptics : structure of optics variation compared to analytc optimum
%               with 3 sigma stay clear from 3mm septum blade. 
%               DeltaOptics.beta=[0 0];
%               DeltaOptics.alpha=[0 0];
%               DeltaOptics.Dispersion=[0 0 0 0];
%               DeltaOptics.ClosedOrbit=[0 0 0 0];
%               DeltaOptics.Sigma=2;
% seed        : random number generator seed
% bl          : bunch length
% es          : energy spread
%
% injeff      : injection efficiency
% beamcentersdistance : injected stored beam centers distance
%                       injectedstoreedoffset + 2.5sigma injected beam
%                       distribution
% lostonsept  : fraction lost on septum
% datafolder  : folder for data. no data collection if this is an output.
%
% example: injeff=getInjEff(ring,120e-9,5e-9);
% example: injeff=getInjEff(ring,120e-9,5e-9,1e3);
% example: injeff=getInjEff(ring,120e-9,5e-9,1e2,2^11);
% example: injeff=getInjEff(ring,120e-9,5e-9,1e3,2^9,10);
% example: injeff=getInjEff(ring,120e-9,5e-9,1e3,2^9,10,'test');
%
%see also: getInjBeam RunInjEffArray CollectInjEffClusterData


if nargin<11, es=1.2e-3; end
if nargin<10, bl=20.4e-3;  end% 68-69 ps 13/03/2017
if nargin<9
    seed=1;
end

if nargin<8
    DeltaOptics.beta=[0 0];
    DeltaOptics.alpha=[0 0];
    DeltaOptics.Dispersion=[0 0 0 0];
    DeltaOptics.ClosedOrbit=[0 0 0 0];
    DeltaOptics.Sigma=2;
end

if nargin<7, addlabel=''; end
if nargin<6, Nproc=50; end
if nargin<5, Nturns=2^11; end
if nargin<4, Npart=10^3; end
if nargin<3, epsz=5e-9;  end
if nargin<2, epsx=120e-9;  end

InjIndex=[];%findcells(ring,'FamName','S3_Septum')+1;

[X0,~,beamcentersdistance,lostonsept,ringSeptum]=getInjBeam(...
    ring,...        %
    epsx,...        %
    epsz,...        %
    bl,...          %
    es,...          %
    Npart,...       %
    InjIndex,...    %
    DeltaOptics,... %
    seed);

ring=ringSeptum;

ringfile=[addlabel 'latCluster.mat'];
save(ringfile,'ring');

commands.time = num2str(12*60); % time in minutes
collect=false;
% run job on Cluster
datafolder=RunInjEffArray(...
    fullfile(ringfile),...  % file where the AT lattice is stored
    'ring',...    % name of lattice variable
    Nturns,...
    Npart,...
    Nproc,...
    X0,[addlabel],0,commands, collect); %linear

% delete lattice file used for tracking
delete(ringfile);

% collect data
injeff=NaN;
if nargout<4
    injeff=CollectInjEffClusterData(datafolder);
    
    disp(['Injection Efficency= ' num2str(injeff) '%']);
    
    save(['INJEFFdata' addlabel '.mat'],'injeff')
end

return
