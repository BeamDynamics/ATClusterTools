function [injeff,lost,X0,xend,lossdetails]=CollectInjEffClusterData(datafolder,outputfilename,trackingdata)
%
% [injeff,lost,x,y,xp,yp,ct,d]=CollectInjEffClusterData(datafolder,outputfilename,trackingdata)
%
% datafolder: folder of data generated with RunDynApArray
% outputfilename: file where to save the data
% trackingdata  : if exists, save tracking data. if 1 make video
%
% injeff:  injection efficency in percentage
% lost : flag , 1 if particle is lost
% X0: 6D coordinates of tracked particles.
% xend: (LARGE) all coordinated along tracking, saved only if asked or
%       video required.
%
%see also: RunInjEffArray

currdir=pwd;

cd(datafolder)
disp(['Extracting results from ' datafolder])

a=dir(fullfile(datafolder,'specfile*.mat'));
totproc=length(a);
b=dir(fullfile(datafolder,'outfile*.mat'));
finproc=length(b);

% wait for processes to finish
while finproc~=totproc
    b=dir(fullfile(datafolder,'outfile*.mat'));
    
    disp(['waiting for ' num2str(totproc-finproc) '/' num2str(totproc) ' processes to finish.'])
    
    % % % count runnning processes
    b=dir(fullfile(datafolder,'outfile*.mat'));
    finproc=length(b);
    
    % wait
    pause(10);
    
end



% if ~exist('AllData.mat','file')
x=[];
xend=[];

%    lossdetails=struct('lost', zeros(1,1), 'turn', inf(1,1),...
%             'element', nan(1,1), 'coordinates', nan(6,1,6));

lossdetails=struct('lost', [], 'turn',[],...
    'element', [], 'coordinates', []);
y=[];
xp=[];
yp=[];
d=[];
ct=[];
lost=[];

for iproc=1:totproc
    try
        load(fullfile(datafolder,['outfile_' num2str(iproc,'%0.4d') '.mat']),...
            'lostflag','X0','X0end','lossinfo');
        lost=[lost lostflag];
        x =[x  X0(1,:)];
        y =[y  X0(2,:)];
        xp=[xp X0(3,:)];
        yp=[yp X0(4,:)];
        d =[d  X0(5,:)];
        ct=[ct X0(6,:)];
        if nargin>2 || nargout>3
            xend(:,:,iproc)=X0end;
            lossdetails.lost = [lossdetails.lost lossinfo.lost];
            lossdetails.turn = [lossdetails.turn lossinfo.turn];
            lossdetails.element = [lossdetails.element lossinfo.element];
            lossdetails.coordinates = cat(2, lossdetails.coordinates, lossinfo.coordinates);
        end
    catch err
        disp(err)
        disp(['failed to load ' ['outfile_' num2str(iproc,'%0.4d') '.mat']])
    end
end

X0=[x;y;xp;yp;ct;d];

injeff=length(find(lost==0))/length(lost)*100;
if ~exist(fullfile(datafolder,'AllData.mat'),'file')
    save(fullfile(datafolder,'AllData.mat'),'x','y','xp','yp','d','ct','X0','lost','injeff','xend','lossdetails');
    
    % delete Cluster files
    delete(fullfile(datafolder,'*.stdout'));
    delete(fullfile(datafolder,'*.stderr'));
    % delete spec and out files
    delete(fullfile(datafolder,'spec*.mat'));
    delete(fullfile(datafolder,'out*.mat'));
    % delete SLURM files
    delete(fullfile(datafolder,'slurm*.out'));
    
else
    load(fullfile(datafolder,'AllData.mat'))
    %     disp('erase AllData.mat')
    %     injeff=[];
    %     lost=[];X0=[];xend=[];lossdetails=[];
    %    return
end


if nargin>1
    
    load(fullfile(datafolder,'GlobalTolParameters.mat'),'Npart','Nproc','Nturns');
    
    %     figure;
    %     quiver3(x(~lost),ct(~lost),y(~lost),xp(~lost),d(~lost),yp(~lost));
    %     xlabel('x');ylabel('ct');zlabel('y');
    %     saveas(gca,['notlost_' outputfilename '.fig'])
    %     figure;
    %     quiver3(x(lost==1),ct(lost==1),y(lost==1),xp(lost==1),d(lost==1),yp(lost==1));
    %     xlabel('x');ylabel('ct');zlabel('y');
    %     saveas(gca,['lost_' outputfilename '.fig'])
    %
    %     figure;
    %     quiver3(x(~lost),ct(~lost),y(~lost),xp(~lost),d(~lost),yp(~lost));
    %     xlabel('x');ylabel('ct');zlabel('y');
    %     hold on;
    %     quiver3(x(lost==1),ct(lost==1),y(lost==1),xp(lost==1),d(lost==1),yp(lost==1),'Color',[1,0,0]);
    %     xlabel('x');ylabel('ct');zlabel('y');
    %     saveas(gca,['all_' outputfilename '.fig'])
    
    if trackingdata==1
        disp('making video');
        tl2transport=outputfilename;
        
        %% save movie images
        ms=3;
        
        stepturns=50;
        turnlist=[1:stepturns,stepturns+1:stepturns:Nturns-11];
        for iturn=turnlist
            
            figure('visible','off');
            subplot(2,3,1:6);
            subplot(2,3,1);
            plot(X0(1,:),X0(2,:),'.','MarkerSize',ms);xlabel('x');ylabel('xp');axis equal;
            hold on;
            plot(X0(1,lost==1),X0(2,lost==1),'g.','MarkerSize',ms);xlabel('x');ylabel('xp');axis equal;
            %hold on;
            %plot(Xstored(1,:),Xstored(2,:),'r.','MarkerSize',ms);xlabel('x');ylabel('xp');axis equal;
            plot(reshape(xend(1,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),reshape(xend(2,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),'k.','MarkerSize',ms);xlabel('x');ylabel('xp');axis equal;
            title([tl2transport ' @inj turn: ' num2str(iturn)]);
            %legend('injected','lost','first turns')
            set(gca,'XLim',[-0.01 0.01]);
            set(gca,'YLim',[-0.005 0.005]);
            subplot(2,3,2);
            plot(X0(1,:),X0(3,:),'.','MarkerSize',ms); xlabel('x');ylabel('y');axis equal;
            hold on;
            plot(X0(1,lost==1),X0(3,lost==1),'g.','MarkerSize',ms);
            %hold on;
            %plot(Xstored(1,:),Xstored(3,:),'r.','MarkerSize',ms);xlabel('x');ylabel('y');axis equal;
            plot(reshape(xend(1,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),reshape(xend(3,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),'k.','MarkerSize',ms);xlabel('x');ylabel('y');axis equal;
            set(gca,'XLim',[-0.01 0.01]);
            set(gca,'YLim',[-0.005 0.005]);
            subplot(2,3,3);
            plot(X0(3,:),X0(4,:),'.','MarkerSize',ms); xlabel('y');ylabel('yp');axis equal;
            hold on;
            plot(X0(3,lost==1),X0(4,lost==1),'g.','MarkerSize',ms);
            %hold on;
            %plot(Xstored(3,:),Xstored(4,:),'r.','MarkerSize',ms);xlabel('y');ylabel('yp');axis equal;
            plot(reshape(xend(3,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),reshape(xend(4,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),'k.','MarkerSize',ms);xlabel('y');ylabel('yp');axis equal;
            set(gca,'XLim',[-0.003 0.003]);
            set(gca,'YLim',[-0.003 0.003]);
            subplot(2,3,4);
            plot(X0(6,:),X0(5,:),'.','MarkerSize',ms); xlabel('ct');ylabel('d');axis equal;
            hold on;
            plot(X0(6,lost==1),X0(5,lost==1),'g.','MarkerSize',ms);
            %hold on;
            %plot(Xstored(6,:),Xstored(5,:),'r.','MarkerSize',ms);xlabel('ct');ylabel('d');axis equal;
            plot(reshape(xend(6,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),reshape(xend(5,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),'k.','MarkerSize',ms);xlabel('ct');ylabel('d');axis equal;
            set(gca,'XLim',[-0.06 0.06]);
            set(gca,'YLim',[-0.03 0.03]);
            subplot(2,3,5);
            plot(X0(2,:),X0(4,:),'.','MarkerSize',ms); xlabel('xp');ylabel('yp');axis equal;
            hold on;
            plot(X0(2,lost==1),X0(4,lost==1),'g.','MarkerSize',ms);
            %hold on;
            %plot(Xstored(2,:),Xstored(4,:),'r.','MarkerSize',ms);xlabel('xp');ylabel('yp');axis equal;
            plot(reshape(xend(2,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),reshape(xend(4,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),'k.','MarkerSize',ms);xlabel('xp');ylabel('yp');axis equal;
            set(gca,'XLim',[-0.003 0.003]);
            set(gca,'YLim',[-0.003 0.003]);
            subplot(2,3,6);
            plot(X0(6,:),X0(1,:),'.','MarkerSize',ms); xlabel('ct');ylabel('x');axis equal;
            hold on;
            plot(X0(6,lost==1),X0(1,lost==1),'g.','MarkerSize',ms);
            %hold on;
            %plot(Xstored(6,:),Xstored(1,:),'r.','MarkerSize',ms);xlabel('ct');ylabel('x');axis equal;
            plot(reshape(xend(6,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),reshape(xend(1,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),'k.','MarkerSize',ms);xlabel('ct');ylabel('x');axis equal;
            set(gca,'XLim',[-0.06 0.06]);
            set(gca,'YLim',[-0.04 0.04]);
            
            %saveas(gca,[tl2transport num2str(iturn) '.jpg']);
            export_fig(gcf,[tl2transport num2str(iturn) '.jpg']);
            close all
            
            %%
            
            figure('visible','off','Position', [0.0 0.0 1 1]);
            h(1)=axes('Position',[0.1 0.2 0.8 0.7],'FontSize',8);
            
            [Xe,Ye,Ze]=ellipsoid(0,0,0,-8e-3,3.5e-3,0.1579,30);
            surf(Xe,Ze,Ye,'FaceColor','none','EdgeColor','black');
            xlabel('X'),ylabel('L'),zlabel('Y'); axis equal;
            hold on;
            plot3(X0(1,:),X0(6,:),X0(3,:),'b.','MarkerSize',ms);
            hold on;
            plot3(X0(1,lost==1),X0(6,lost==1),X0(3,lost==1),'g.','MarkerSize',ms);
            hold on;
            %plot3(Xstored(1,:),Xstored(6,:),Xstored(3,:),'r.','MarkerSize',ms);
            %hold on;
            plot3(reshape(xend(1,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),...
                reshape(xend(6,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),...
                reshape(xend(3,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),'k.','MarkerSize',ms);
            title({[tl2transport ' @inj turn: ' num2str(iturn)],['Efficency: ' num2str(injeff,'%2.2f') '%']},'FontSize',14);
            
            
            h(2)=axes('Position',[0.6 0.6 0.25 0.25],'FontSize',8);
            
            plot(X0(1,:),X0(2,:),'b.','MarkerSize',ms);
            hold on;
            %plot(Xstored(1,:),Xstored(2,:),'r.','MarkerSize',ms);
            %hold on;
            plot(X0(1,lost==1),X0(2,lost==1),'g.','MarkerSize',ms);
            hold on;
            plot(reshape(xend(1,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),...
                reshape(xend(2,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),'k.','MarkerSize',ms);
            set(h(2),'XLim',[-0.01 0.01]);
            set(h(2),'YLim',[-0.005 0.005]);
            % legend('injected','lost','first turns')
            
            xlabel('x'),ylabel('xp');
            %title({[tl2transport ' @inj turn: ' num2str(iturn)],[]});
            
            
            h(3)=axes('Position',[0.15 0.15 0.3 0.10],'FontSize',8);
            surf(Xe,Ze,Ye,'FaceColor','none','EdgeColor','black');
            xlabel('X'),ylabel('L'),zlabel('Y'); %axis equal;
            hold on;
            plot3(X0(1,:),X0(6,:),X0(3,:),'b.','MarkerSize',ms);
            hold on;
            plot3(X0(1,lost==1),X0(6,lost==1),X0(3,lost==1),'g.','MarkerSize',ms);
            hold on;
            %plot3(Xstored(1,:),Xstored(6,:),Xstored(3,:),'r.','MarkerSize',ms);
            %hold on;
            plot3(reshape(xend(1,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),...
                reshape(xend(6,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),...
                reshape(xend(3,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),'k.','MarkerSize',ms);
            %title([tl2transport ' @inj turn: ' num2str(iturn)]);
            view(-90,90)
            set(h(3),'XLim',[-0.01 0.01]);
            set(h(3),'YLim',[-0.2 0.2]);
            set(h(3),'ZLim',[-0.005 0.005]);
            
            h(4)=axes('Position',[0.15 0.35 0.3 0.10],'FontSize',8);
            surf(Xe,Ze,Ye,'FaceColor','none','EdgeColor','black');
            xlabel('X'),ylabel('L'),zlabel('Y'); %axis equal;
            hold on;
            plot3(X0(1,:),X0(6,:),X0(3,:),'b.','MarkerSize',ms);
            hold on;
            plot3(X0(1,lost==1),X0(6,lost==1),X0(3,lost==1),'g.','MarkerSize',ms);
            hold on;
            %plot3(Xstored(1,:),Xstored(6,:),Xstored(3,:),'r.','MarkerSize',ms);
            hold on;
            plot3(reshape(xend(1,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),...
                reshape(xend(6,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),...
                reshape(xend(3,iturn*Npart/Nproc:(iturn+1)*Npart/Nproc,:),1,[]),'k.','MarkerSize',ms);
            %title([tl2transport ' @inj turn: ' num2str(iturn)]);
            view(-90,180)
            set(h(4),'XLim',[-0.01 0.01]);
            set(h(4),'YLim',[-0.2 0.2]);
            set(h(4),'ZLim',[-0.005 0.005]);
            
            %
            export_fig(gcf,[tl2transport num2str(iturn) '_3D.jpg']);
            close all
            
            
            
        end
        
        
        %% write movie
        % Set up the movie.
        writerObj = VideoWriter('InjectedBeamStory.avi'); % Name it.
        writerObj.FrameRate = 3; % How many frames per second.
        writerObj.Quality = 25; % How many frames per second.
        open(writerObj);
        
        for iturn=turnlist(2:end)
            
            thisFrame = imread([tl2transport num2str(iturn) '.jpg']);
            %size(thisFrame)
            writeVideo(writerObj, thisFrame);
        end
        
        writerObj.close;
        close(writerObj); % Saves the movie.
        
        %% write movie
        % Set up the movie.
        writerObj1 = VideoWriter('InjectedBeamStory3D.avi'); % Name it.
        writerObj1.FrameRate = 3; % How many frames per second.
        writerObj1.Quality = 25; % How many frames per second.
        open(writerObj1);
        
        for iturn=turnlist
            
            thisFrame = imread([tl2transport num2str(iturn) '_3D.jpg']);
            %size(thisFrame)
            writeVideo(writerObj1, thisFrame);
        end
        
        writerObj1.close;
        close(writerObj1); % Saves the movie.
        
        
    end
    
end

cd(currdir)

if nargout>0
    % remove data direcotry
    system(['rm -r ' datafolder]);
end

return
