function InjEffClusterrunner(inputdatafile)
%InjEffClusterrunner( inputdatafile, outputdatafile)
%  
%   inputdatafile contains variables: 
%           ringFile           AT lattice file
%           ringFileVarName    AT lattice variable name
%           Nturns,     number of turns to track
%           X0
%           quantdiffFlag (optional, default is 1 for compatibility)
%  
%   produces a file named outputdatafile that contains the variables:
%           lost    flag for lost particle
%           X0      6XN vector of intial coordinate
%           X0end   6XN vector of final coordinate
% 
%see also   

a=load(inputdatafile);

b=load(a.ringFile,a.ringFileVarName);
r=b.(a.ringFileVarName);
outputdatafile=a.outfilename;
if isfield(a, 'quantdiffFlag')
    quantdiffFlag = a.quantdiffFlag;
else
    quantdiffFlag = 1;
end

% % % % set cavity to get quantum diffusion element
indrfc=find(atgetcells(r,'Class','RFCavity'));
V=sum(cellfun(@(a)a.Voltage,r(indrfc),'un',1));
H=r{indrfc(1)}.HarmNumber;
    
% quantum diffusion
if quantdiffFlag
    roff = atsetcavity(r,V,0,H);
    qde=atQuantDiff('QuantDiff',roff,'Seed',1); 
    % fixed quantum diffusion seed for reproducibility
end

if quantdiffFlag
    r=[r;{qde}];
end

[X0end,lostflag,survivedturns,lossinfo]=ringpass(r,a.X0,a.Nturns,'nhist',6);

X0=a.X0;

save(outputdatafile,'lostflag','survivedturns','X0','X0end','lossinfo');
if a.collect && a.indproc==1
    CollectInjEffClusterData(a.datafolder);
end

return


