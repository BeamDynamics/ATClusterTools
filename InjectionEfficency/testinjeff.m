clear all
close all

 load('../S28Dmerged.mat')
 rcor=LOW_EMIT_RING_INJ;

rfv=6.5;
npartinj=1e3;
radonflag=1;

r=atsetcavity(atradon(rcor),rfv*1e6,radonflag,992);
r=atrotatelattice(r,length(r)-3);

%% run processes
% [~,~,~,dfinjeffrb]=getInjEff(r,60e-9,5e-9,npartinj,2^9,200,['Newbeam']);
% CollectInjEffClusterData(dfinjeffrb);

InjEff=getInjEff(r,60e-9,5e-9,npartinj,2^9,200,['Newbeam']);

