function [betinj,storedinjecteddistance,storedinjectedcenters]=OptimalInjectionBeta(ei,es,bs,SW,Ni,storedclearance)
% function betinj=OptimalInjectionBeta(ei,es,bs,SW)
% 
% computes optimal injected beam horizontal beta given:
% ei: injected beam emittance [m]
% es: stored beam emittance [m]
% bs: beta at injection in the storage ring [m]
% SW: septum thickness [m]
% Ni: sigma injected beam (default 3 sigma)
% storedclearance: distance stored beam - septum (default 1mm)
%
% alpha and dispersion at injection are neglected
% 
%see also:  

if nargin<6
storedclearance=1e-3;% 1 mm between stored beam and blade
end

if nargin<5
Ni=3;   % sigmas for stored beam to be accomodated
end
Ns=storedclearance/sqrt(es*bs); % stored beam sigma clearance

% optimum condition is the following =1 
fun=@(bi)3*bi.^2./bs.^2+(2*Ns*sqrt(es.*bs)+SW)./(Ni.*sqrt(ei.*bs)).*bi.^(3/2)./bs.^(3/2);

biv=0.1:0.1:bs;
betinj=interp1(fun(biv),biv,1);

storedinjecteddistance=storedclearance+SW;%+Ni.*sqrt(ei.*betinj);
storedinjectedcenters=storedclearance+SW+Ni.*sqrt(ei.*betinj);
disp(['Stored-Injected centers : ' num2str(storedinjectedcenters*1e3) ' mm'])
disp(['Injected Beta x : ' num2str(betinj) ' m'])
% % plot
% plot(biv,fun(biv));
% hold on; plot(betinj,1,'ro')

return

