function scanbetaemit(latfile,latvarname,wfold,bx,ex,septpos,injstordist)
% scan injeff betax, emitx


curdir=pwd;

a=load(latfile);
ring=a.(latvarname);

mkdir(wfold);
cd(wfold);

Npart=3*1e3;

[betxgr,emitxgr]=meshgrid(bx,ex);
betx=betxgr(:);
emitx=emitxgr(:);
injeff=zeros(size(betx));
injstodis=zeros(size(betx));
lostonsept=zeros(size(betx));
df={};

septpos=4e-3;
injstordist=4e-3;

% run simulations
for inp=1:length(betx)
    [ie,isd,los,datafolder]=getInjEff(...
        ring,injstordist,septpos,...
        betx(inp),emitx(inp),5e-9,Npart,...
        ['_b' num2str(betx(inp))...
        '_e' num2str(emitx(inp)*1e9)]);
    injeff(inp)=ie;
    injstodis(inp)=isd;
    lostonsept(inp)=los;
    df{inp}=datafolder;
end
save('allscanbetaemitdatapart.mat');

%% collect data
injeff=zeros(size(betx));
for inp=1:length(betx)
    ie=CollectInjEffClusterData(df{inp});
    
    disp(['Injection Efficency= ' num2str(ie) '%']);
    injeff(inp)=ie;
    
end


save('allscanbetaemitdata.mat');

%%
betxgr=reshape(betx,length(ex),length(bx));
emitxgr=reshape(emitx,length(ex),length(bx));
injeff=reshape(injeff,length(ex),length(bx));
injstodis=reshape(injstodis,length(ex),length(bx));
lostonsept=reshape(lostonsept,length(ex),length(bx));

figure;
surf(betxgr,emitxgr,injeff);
xlabel({[],'beta'});
ylabel({'emittance',[]}); 
zlabel('I.E. [%]');
colorbar;
caxis([0 100]);
view(2);
axis tight;
title({'Injection Efficiency',[]})
saveas(gca,'betaemitInjEff.fig');
saveas(gca,'betaemitInjEff.jpg');
export_fig('betaemitInjEff.jpg');

%%
figure;
surf(betxgr,emitxgr,injstodis);
xlabel('beta'); ylabel('emittance'); zlabel('inj-stored [m]');
saveas(gca,'betaemitInjDist.fig');
saveas(gca,'betaemitInjDist.jpg');
%export_fig('betaemitInjDist.jpg');

figure;
surf(betxgr,emitxgr,lostonsept);
xlabel('beta'); ylabel('emittance'); zlabel('lost on septum [%]');

saveas(gca,'betaemitlostonsept.fig');
saveas(gca,'betaemitlostonsept.jpg');
%export_fig('betaemitlostonsept.jpg');


figure; plot(betxgr',injeff')
legend(arrayfun(@(b)['\epsilon_x=' num2str(b*1e9)],ex,'un',0),'Location','EastOutside')
xlabel({[],'\beta_x injected beam'})
ylabel({'Injection Efficency [%]',[]})
saveas(gca,'betaijneff2D.fig');
saveas(gca,'betaijneff2D.jpg');

figure; plot(emitxgr(:,1:4:end),injeff(:,1:4:end))
legend(arrayfun(@(b)['\beta_x=' num2str(b)],bx(1:4:end),'un',0),'Location','EastOutside');
xlabel({[],'\epsilon_x injected beam'})
ylabel({'Injection Efficency [%]',[]})
saveas(gca,'emitijneff2D.fig');
saveas(gca,'emitijneff2D.jpg');



cd(curdir);
