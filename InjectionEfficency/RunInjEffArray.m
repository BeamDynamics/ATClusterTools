function datafolder=RunInjEffArray(...
    ringfile,...
    varringname,...
    Nturns,...
    Npart,...
    Nproc,...
    X6,...
    foldnamespec,...
    quantdiffFlag,...
    commands,...
    collect)
% function RunInjEffArray(ringfile,varringname)
%
% created specification files to be used by FMapClusterrunner
% and run the processes on the ESRF Cluster
%
%     ringfile       : file where the AT lattice is stored
%     varringname    : name of AT lattice variable
%
%     Nturns=2^11;    % total tracking turns
%     Npart=200;     %  particles to be tracked
%     Nproc=200;      % number of Cluster processors to use.
%     X6=;      %6XNpart coordinates of input particles.
%               %8x1 [ex,ey,bx,by,ax,ay,x_cent,y_cent]
%               %cellarray, lattice.
%     foldnamespec:  folder name additional label
%     quantdiffFlag: if 0 no quantum diffusion is added. default is 1
%     commands       %slurm cluster commands
%     collect        if true, the collect is done by the first job
% routine used should be compiled with
% mcc('-vm','routinename','passmethodlist.m')
% in the computer where you wish to use this function.
% ex: mcc('-vm','InjEffClusterrunner.m','passmethodlistfringes.m')
% passmethodlistfringes.m contains the names of functions used by the
% routine but not explicitly called (as AT passmethods).
%
% RunInjEffArray runs Cluster jobs as ARRAYS!
%see also: InjEffClusterrunner CollectInjEffClusterData

% this should be input.
routine='InjEffClusterrunner'; % compiled routine file name.
%pathtoroutine='/mntdirect/_users/liuzzo/Matlab_Work/ATWORK/atwork-code/physics/Fmaps_findtunetom2'; % compiled routine path.

[pathtoroutine,~,exte]=fileparts(which(routine));

if ~exist(fullfile(pathtoroutine,[routine exte]),'file')
    error([routine ' does not exist in: ' pathtoroutine...
        ' . If this is not the correct directory,'...
        ' you may need to add the directory that contains'...
        routine ' to the path.']);
end

%% LATTICE MANIPULATIONS
a=load(ringfile,varringname);
ring=a.(varringname);

if nargin<10
    collect=false;
end

if nargin<9
    commands.time=num2str(12*60);
end

if nargin<8
    quantdiffFlag=1;
end

if nargin<7
    foldnamespec='';
end

[~,ringfile]=fileparts(ringfile);

commonwork=...
    [ringfile '_nt' num2str(Nturns) '_ns'...
    num2str(Npart) '_' foldnamespec];%
commonworkdir=fullfile(pwd,commonwork);%
mkdir(commonworkdir);

cd(commonworkdir);

datafolder=pwd;

% place here lattice file and RM file
save(fullfile(datafolder,'latticefile.mat'),'ring');

%% define error maximum ranges

totprocnum=1;

workdir=pwd;
pfile=fopen('params.txt','w+');


[pointsperprocess]=DivideClusterJobs(Npart,Nproc);

save(fullfile(datafolder,'GlobalTolParameters.mat'),...
    'Npart','Nturns','ringfile','varringname','Nproc',...
    'pointsperprocess');

%% LOOP FMA points
for indproc=1:Nproc %
    
    % prepare SpecsFile
    
    % do not store lattice and RM in this file. they are always the same.
    ringFile=fullfile(datafolder,'latticefile.mat');                       %#ok<*NASGU> % AT lattice
    ringFileVarName='ring';                       % AT lattice
    numberofturns=Nturns;     % number of turns for tracking
    outfilename=fullfile(datafolder,['outfile_' num2str(indproc,'%0.4d') '.mat']);
    
    
    if indproc<Nproc
        X0=X6(:,sum(pointsperprocess(1:indproc-1))+1:sum(pointsperprocess(1:indproc)));
    else %last proces takes left overs
        X0=X6(:,sum(pointsperprocess(1:indproc-1))+1:end);
    end
    
    spfn=['specfile_' num2str(indproc,'%0.4d') '.mat'];
    save(spfn,'ringFile','ringFileVarName','outfilename',...
        'X0','Nturns','quantdiffFlag','collect','datafolder','indproc');
    
    % prepare Cluster script to run
    %script=[' /opt/matlab_' version('-release') fullfile(pwd,spfn) '\n'];
    
    script=[' /sware/com/matlab_2020a ' fullfile(datafolder,spfn) '\n'];
    
    %fopen(fullfile(workdir,'params.txt'),'w+')
    fprintf(pfile,script');
    
    totprocnum=totprocnum+1;
    
end% END LOOP processes

totprocnum=totprocnum-1;

routinetorun=[pathtoroutine '/run_' routine '.sh' ];

% hpc
label=[ringfile '_St' num2str(Npart) '_P' num2str(Nproc) '_']; % label for files.

fclose('all');

% run on slurm
commands.job_name=label;
submit_matlab_to_slurm(routinetorun(1:end-3),'params.txt',commands)

cd('..') % back to Parent of Working Directory

disp([num2str(totprocnum) ' processes with label: ' label ' are running on Cluster']);

return