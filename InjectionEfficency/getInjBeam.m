function [X0,latoff,beamcentersdistance,lostonsept,ringInjIndex,XlostSept]=getInjBeam(...
    ring,...        %1
    epsx,...        %2
    epsz,...        %3
    Blength,...     %4
    Edev,...        %5
    Npart,...       %6
    InjIndex,...    %7
    DeltaOpt,...    %8
    seed)           %9  
%function [X0,latoff,ringS3]=getInjBeam(ring,epsx,epsz,Npart,InjIndex,DeltaOpt,seed)
% computes injected beam distibution 
% 
% optimal injected beta x computed assuming 3 sigma stay clear from septum 
% (A.Streun SLS-TME-TA-2002-0193,2005)
% 
% beam displacement and angle consider alpha at the injection point
% 
% Inputs:
% ring    : AT lattice 
% epsx    : emittance injected beam (default 60nm)
% epsy    : emittance vertical injected beam (default 5nm)
% Blength : injected beam bunch length (default 20.4e-3 m)
% Edev    : injected beam energy spread (default 1.2e-3)
% Npart   : number of particles (default 3000)
% InjIndex: index for injection, if empty or missing look for element after \w*Septum\w*
% DeltaOpt: injected beam optics errors
%           DeltaOpt.beta=[0 0], 
%           DeltaOpt.alpha=[0 0], 
%           DeltaOpt.Dispersion=[0 0 0 0], 
%           DeltaOpt.ClosedOrbit=[0 0 0 0] or [0 0 0 0 0 0]
%           DeltaOpt.Sigma=2; inject beam sigma stay clear
%           DeltaOpt.SeptumTickness = 3e-3
%           DeltaOpt.StoredToSeptum = 1e-3
% 
% seed    : random generator seed. default=1
% 
% Outputs:
% X0      : injected particles distribution
% latoff  : lattice offset at injection point (T1([1,3]))
% ringInjIndex: ring rotated at injection point.
% XlostSept : coordinates of particles lost on spetum
%
%see also: atbeam atx atlinopt

if nargin<9
    seed=1;
end

if nargin<8
    DeltaOpt.beta=[0 0];
    DeltaOpt.alpha=[0 0];
    DeltaOpt.Dispersion=[0 0 0 0];
    DeltaOpt.ClosedOrbit=[0 0 0 0];
    DeltaOpt.Sigma=2;
end

if nargin<7
    InjIndex=[];
end

if nargin<6, Npart=3*10^3; end
if nargin<5, Edev=1.2e-3; end
if nargin<4, Blength=20.4e-3;  end% 68-69 ps 13/03/2017
if nargin<3, epsz=5e-9;  end
if nargin<2, epsx=120e-9;  end

% stored beam optics at injection point
if isempty(InjIndex)
    
    ind=find(atgetcells(ring,'FamName','\w*S3_Septum\w*'),1,'first');
    if isempty(ind)
        warning('no \w*Septum\w*, using first index as injection point')
        InjIndex=1;
    else
        InjIndex=ind+1;
    end
    
    if InjIndex>length(ring)
        InjIndex=1;
    end

end

[lsrinj,~,~]=atlinopt(ring,0,InjIndex); % optics at storage ring ingection

try
    [~,b]=atx(ring);
catch
    disp('atx failed!')
    b.modemittance(1)=135*1e-12;
    b.modemittance(2)=5*1e-12;
end

if  b.modemittance(2)<1e-14
    disp('assuming 5 pm vertical emittance for stored beam')
    vemit=5*1e-12;
else
    vemit=b.modemittance(2);
end

% get optimal beta at injection and position of injected beam
bs=lsrinj.beta(1);
es=b.modemittance(1);
if isfield(DeltaOpt,'SeptumTickness')
septblade = DeltaOpt.SeptumTickness;
else
septblade=3e-3;
end

if isfield(DeltaOpt,'StoredToSeptum')
StoredToSeptum = DeltaOpt.StoredToSeptum;
else
StoredToSeptum=1e-3;
end

Nsigstored=DeltaOpt.Sigma;

[betaxopt,d]=OptimalInjectionBeta(epsx,es,bs,septblade,Nsigstored,StoredToSeptum); % assumes alpha=0! and 1mm stay clear for the circulating beam

injectedstoreedoffset=[d,0.0]; % distance between stored beam center and edge of injected beam
septpos=d;

disp(['septum blade : ' num2str(septblade) ' m']);
disp(['X emittance : ' num2str(epsx) ' m']);
disp(['Y emittance : ' num2str(epsz) ' m']);
disp(['beta inj : ' num2str(betaxopt) ' m']);
disp(['X emit. stored : ' num2str(es) ' m']);
disp(['Y emit. stored : ' num2str(vemit) ' m']);
disp(['beta stored : ' num2str(bs) ' m']);
disp(['Number of particle: ' num2str(Npart) ' ']);
disp(['position respect to stored beam: ']);
disp(injectedstoreedoffset);



%% generate particle distribution

%using thomas function
numbpart=Npart;
betax=betaxopt+DeltaOpt.beta(1);
alphax=lsrinj.alpha(1)+DeltaOpt.alpha(1);
%epsx=60e-9;
betaz=lsrinj.beta(2)+DeltaOpt.beta(2);
alphaz=lsrinj.alpha(2)+DeltaOpt.alpha(2);
%epsz=5e-9;
%Edev=1.2e-3;
% Blength=17.1e-3; % 57 ps
%Blength=20.4e-3; % 68-69 ps 13/03/2017

% set seed
s = RandStream('mcg16807','Seed',seed);
RandStream.setGlobalStream(s);

Dx = lsrinj.Dispersion(1)+DeltaOpt.Dispersion(1);
Dxp = lsrinj.Dispersion(2)+DeltaOpt.Dispersion(2);
Dy = lsrinj.Dispersion(3)+DeltaOpt.Dispersion(3);
Dyp = lsrinj.Dispersion(4)+DeltaOpt.Dispersion(4);

SigMat = SigmaMatrix6D(...
    epsx,alphax,betax,Dx,Dxp,...
    epsz,alphaz,betaz,Dy,Dyp,...
    Edev,Blength);

% generate beam 6D coordinate uncoupled

[~,notposdef]=chol(SigMat);

if notposdef
    error('OPTICS NOT CONVERGED! sigma matrix not positive definite.')
%     warning('OPTICS NOT CONVERGED! sigma matrix not positive definite. using default beam.')
%     
%     epsx=134e-12;
%     betax=6.9;
%     alphax=0;
%     epsz=5e-12;
%     betaz=2.6;
%     alphaz=0;
%     
%     %[~,X0]=atmkdist(numbpart,betax,alphax,epsx,betaz,alphaz,epsz,Edev,Blength,angle);
%     
%     SigMatX=[epsx*betax,  -epsx*alphax;...
%         -epsx*alphax, epsx*(1+alphax^2)/betax ];
%     SigMatZ=[epsz*betaz,  -epsz*alphaz;...
%         -epsz*alphaz, epsz*(1+alphaz^2)/betaz ];
%     SigMatL=[Edev^2,  0;...
%         0, Blength^2 ];
%     
%     SigMat=[SigMatX,zeros(2),zeros(2);...
%         zeros(2),SigMatZ,zeros(2);...
%         zeros(2),zeros(2),SigMatL;...
%         ];
end


X0=atbeam(numbpart,SigMat);

ringInjIndex=atrotatelattice(ring,InjIndex);

% given distance + lattice displacement + N*sigma of injected beam.
latoff=zeros(2,1);
latang=zeros(2,1);
if isfield(ringInjIndex{1},'T1')
    latoff(1)=ringInjIndex{1}.T1(1); % assume that Septum is aligned to the chamber
    latoff(2)=ringInjIndex{1}.T1(3);
    disp(['Lattice offset: ' num2str(latoff') ]);
    latang(1)=(ringInjIndex{2}.T1(1)-ringInjIndex{1}.T1(1))/ringInjIndex{1}.Length; % assume that Septum is aligned to the chamber
    latang(2)=(ringInjIndex{2}.T1(3)-ringInjIndex{1}.T1(3))/ringInjIndex{1}.Length;
    disp(['Lattice angles (not used): ' num2str(latang') ]);
end

% injected - stored beam distance

beamINJsig=Nsigstored*sqrt(betaxopt*epsx);

injstof(1)=injectedstoreedoffset(1)+beamINJsig;% add 3 sigma beam

if length(injectedstoreedoffset)>1
    injstof(2)=injectedstoreedoffset(2);%
else
    injstof(2)=0;
end

% compute angle for injection for non zero alpha.
xpos=injstof(1);
xsig=sqrt(b.modemittance(1)*lsrinj.beta(1));
xang=-lsrinj.alpha(1)*sqrt(b.modemittance(1)/lsrinj.beta(1))*xpos/xsig;

ypos=injstof(2);
ysig=sqrt(vemit*lsrinj.beta(2));
yang=-lsrinj.alpha(2)*sqrt(vemit/lsrinj.beta(2))*ypos/ysig;

% shift beam off axis
%  beam + (blade+Nsigma) + alignment errors + additional errors
X0(1,:)=X0(1,:) -xpos -latoff(1) +DeltaOpt.ClosedOrbit(1);
%  beam + Angle(blade+Nsigma) (due to alpha @ inj) + additional errors
X0(2,:)=X0(2,:) -xang            +DeltaOpt.ClosedOrbit(2); % alpha in x

%  beam + blade+Nsigma + alignment errors + additional errors
X0(3,:)=X0(3,:) -ypos -latoff(2) +DeltaOpt.ClosedOrbit(3); %
%  beam + Angle(blade+Nsigma) (due to alpha @ inj) + additional errors
X0(4,:)=X0(4,:) -yang            +DeltaOpt.ClosedOrbit(4); % alpha in y

if length(DeltaOpt.ClosedOrbit)==6
    %  beam + blade+Nsigma + alignment errors + additional errors
    X0(5,:)=X0(5,:)  +DeltaOpt.ClosedOrbit(5); %
    %  beam + Angle(blade+Nsigma) (due to alpha @ inj) + additional errors
    X0(6,:)=X0(6,:)  +DeltaOpt.ClosedOrbit(6); % alpha in y
end

% set 1 m to loose particles that are below septum
chopdistance=septpos+latoff(1);
disp(['Stored beam position: ' num2str(-latoff(1)*1e3) ' mm']);
disp(['Septum position: ' num2str(-chopdistance*1e3) ' mm']);
disp(['Septum - stored beam: ' num2str(-(chopdistance-latoff(1))*1e3) ' mm']);
disp(['Inj Beam position: ' num2str(mean(X0(1,:))*1e3) ' mm']);
disp(['Inj Beam - stored beam: ' num2str(mean(X0(1,:)+latoff(1))*1e3) ' mm']);
XlostSept=X0(:,X0(1,:)>-(chopdistance));
X0(1,X0(1,:)>-(chopdistance))=1;
lostonsept=length(find(X0(1,:)>-(chopdistance)))/length(X0(1,:))*100;
disp(['Lost on septum: ' num2str(lostonsept) '%']);

beamcentersdistance=sqrt(sum(injstof.^2));

% %% phase space plots
% addlabel='InjBeam';
% save([addlabel '_X0.mat'],'X0');
% ff=figure('visible','on');
% plotphasespace(X0);
% saveas(gca,[addlabel '_phasespace.fig']);
% saveas(gca,[addlabel '_phasespace.jpg']);
%close(ff);




return
