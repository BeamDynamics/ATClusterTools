function rmatch=MatchLast2CorForFirstBPM(ring,inCOD,indBPM,indHCor,indVCor)
%takes the last two correctors to match the orbit and angle trajectory at
%the first BPM.

% get trajectory
[t0]=findtrajectory6Err(ring,indBPM,inCOD);
 
% trn4=linepass([ring; ring; ring; ring],[inCOD(1:4),0,0]',[indBPM length(ring)+indBPM  length(ring)*2+indBPM length(ring)*3+indBPM]);
%  figure('name','initial matched 4 turn'); plot(trn4');ylim([-3e-3 3e-3]);
% export_fig('InitialTrajectory4turnRF.jpg')

%figure('name','initial'); plot(t0');ylim([-3e-3 3e-3]);
     
% match angle and position at BPM 1 of the un rotated lattice ( BPM n of the rotated
% lattice) to be identical to those of the initial trajectory
h0=atVariableBuilder(ring,indHCor(end-2),{'PolynomB',{1,1}});
h1=atVariableBuilder(ring,indHCor(end-1),{'PolynomB',{1,1}});
h2=atVariableBuilder(ring,indHCor(end),{'PolynomB',{1,1}});
v0=atVariableBuilder(ring,indVCor(end-2),{'PolynomA',{1,1}});
v1=atVariableBuilder(ring,indVCor(end-1),{'PolynomA',{1,1}});
v2=atVariableBuilder(ring,indVCor(end),{'PolynomA',{1,1}});
VariabH = [h0 h1 h2];
VariabV = [v0 v1 v2];

bpmmatchind=[2,3];

for iibb = 1:length(bpmmatchind)
ConstrH(iibb)=struct(...
    'Fun',@(r,~,~)transpose(findtrajectory6Err([r;r],length(r)+indBPM(bpmmatchind(iibb)),inCOD)),... % bpmmatchind BPM of second turn
    'Weight',[ones(2,1)',1e15,1e15,1e15,1e15],...
    'RefPoints',[1],...
    'Min',t0(:,bpmmatchind(iibb))',...
    'Max',t0(:,bpmmatchind(iibb))');

ConstrV(iibb)=struct(...
    'Fun',@(r,~,~)transpose(findtrajectory6Err([r;r],length(r)+indBPM(bpmmatchind(iibb)),inCOD)),... % bpmmatchind BPM of second turn
    'Weight',[1e15,1e15, ones(2,1)',1e15,1e15],...
    'RefPoints',[1],...
    'Min',t0(:,bpmmatchind(iibb))',...
    'Max',t0(:,bpmmatchind(iibb))');
end

kickers_ind = indHCor(end-2:end);
for ik = 1:length(kickers_ind)
 kicklimitH(ik) = struct(...
        'Fun',@(r,~,~) max(abs(...
        atgetfieldvalues(r,kickers_ind(ik),'PolynomB',{1,1})...
        .*atgetfieldvalues(r,kickers_ind(ik),'Length'))),...
        'Min',-0.008,...
        'Max',0.008,...
        'RefPoints',kickers_ind(ik),...
        'Weight',1e-6); % correctors within 0.4mrad . very important
end


    kickers_ind = indVCor(end-2:end);
kickers_ind = indHCor(end-2:end);
for ik = 1:length(kickers_ind)
kicklimitV(ik) = struct(...
        'Fun',@(r,~,~) max(abs(...
        atgetfieldvalues(r,kickers_ind(ik),'PolynomA',{1,1})...
        .*atgetfieldvalues(r,kickers_ind(ik),'Length'))),...
        'Min',-0.008,...
        'Max',0.008,...
        'RefPoints',kickers_ind(ik),...
        'Weight',1e-6); % correctors within 0.4mrad . very important

end
    
    
% input optics and COD
[intwi,~,~]=atlinopt(ring,0,1);
intwi.ClosedOrbit=inCOD(1:4);
intwi.mu=[0 0];
intwi.beta = [1 1];
intwi.alpha = [0 0];

[rmatch]=atmatch(...
     ring,VariabH,[ConstrH,kicklimitH],1e-10,6,3,@lsqnonlin);
[rmatch]=atmatch(...
     rmatch,VariabV,[ConstrV,kicklimitV],1e-10,6,3,@lsqnonlin);

%[rmatch]=atmatch(...
%     rmatch,[VariabH,VariabV],[ConstrH ConstrV],1e-10,50,3,@fminsearch,intwi);
 
% [rmatch]=atmatch(...
%     rmatch,Variab,[ConstrH ConstrV],1e-10,50,3,@lsqnonlin,intwi);
 
 [tm]=findtrajectory6Err(rmatch,indBPM,inCOD);
%figure('name','rotated matched'); plot(tm');ylim([-3e-3 3e-3]);


% % check multi turns.
% ring=rmatch;
% trn4=linepass([ring; ring; ring; ring],[inCOD(1:4),0,0]',[indBPM length(ring)+indBPM  length(ring)*2+indBPM length(ring)*3+indBPM]);
%  figure('name','initial matched 4 turn'); plot(trn4');ylim([-3e-3 3e-3]);
% export_fig('MatchedTrajectory4turnRF.jpg')

% % check that now a COD exists.
% ring=rmatch;
%findorbit4Err(ring,0,indBPM,[inCOD 0 0]');

% % 6D ok if cavity is correctly set before setting errors in the lattice
%findorbit6Err(atsetRFCavity(ring,6.5e6,0,992,0.0),indBPM,[inCOD 0 0]'); 

return
