function [dq,dq_err]=qemsvd_mod(a,b,neig,plot_flag)
%function dq=qemsvd_mod(a,b,neig,plot)
% given response matrix a and vector b to be corrected the function finds
% the vector dq so that b-a*dq=0
%
% if the input plot is = 1 the plot of correction effect and correctors
% strengths versus number of eigenvectors is also computed.
%
% svd part is carbon copy of qemsvd by L.Farvacque in qempanel

[u,s,v]=svd(a,0);
lambda=diag(s);
nmax=length(lambda);
eigsorb=u'*b;
if neig > nmax
    neig=nmax;
    warning('Svd:maxsize',['number of vectors limited to ' num2str(nmax)]);
end
eigscor=eigsorb(1:neig)./lambda(1:neig);
dq=v(:,1:neig)*eigscor;

Amod = u(:,1:neig) * s(1:neig,1:neig);

err = v(:,1:neig)*inv(Amod'*Amod)*v(:,1:neig)';

st = s;
st([(neig+1):end;(neig+1):end])=0; %zero eigenvalues above cut
R = u*st*v; %truncated pseudo inverse
dx = (b - R*dq);

% dq_err = Rinv * dx; %repmat(dx,size(b));
dq_err = sqrt(diag(err)*mean(dx.^2))/200;

disp(std(dq));
disp(std(dq_err));


% plot correction effect to set appropriate number of eigenvectors.
if nargin<4
    plot_flag=0;
end

plot_verbose = false;

if plot_flag
    numeigen0=neig;
    neig=sort([1:5:nmax,numeigen0]);
    dqstd=zeros(size(neig));
    ostd=zeros(size(neig));
    for ineig=1:length(neig)% loop number of eigenvectors used in the correction
        eigscor=eigsorb(1:neig(ineig))./lambda(1:neig(ineig));
        dqe=v(:,1:neig(ineig))*eigscor;
        o=b-a*dqe;
        Amod = u(:,1:neig(ineig)) * s(1:neig(ineig),1:neig(ineig));
        
        err = v(:,1:neig(ineig))*inv(Amod'*Amod)*v(:,1:neig(ineig))';
        
        st = s;
        st([(neig(ineig)+1):end;(neig(ineig)+1):end])=0; %zero eigenvalues above cut
        R = u*st*v; %truncated pseudo inverse
        dx = (b - R*dq);
        
        % dq_err = Rinv * dx; %repmat(dx,size(b));
        dqe_err = sqrt(diag(err)*mean(dx.^2))/200;
        
        dqstd(ineig)=std(dqe);
        dqerrstd(ineig)=std(dqe_err);
        ostd(ineig)=std(o);
    end
    
    f=figure('name','eigenvectors truncation choice','visible','on');
    AX(1) = plot(neig,ostd);
    ylabel('rms vector to correct')
    yyaxis right
    AX(2) = errorbar(neig,dqstd,dqerrstd);
    xlabel('number of eigenvectors');
    ylabel('rms correctors');
    %set(AX(1),'YScale','log');
    % set(AX(2),'YScale','log');
    title('Correctors strength and residual vs number of eigenvectors')
    %     saveas(gca,[datestr(now,'yyyymmddTHHMMSS') '_eigplot.fig']);
    %     export_fig([datestr(now,'yyyymmddTHHMMSS') '_eigplot.jpg']);
    % close(f);
    figure('name','eigenvalues');
    plot(lambda);
    xlabel('#');
    ylabel('value');
    
    ax=gca;
    set(ax,'YScale','log');
    
    if plot_verbose
        
        figure('name','eigenvectors');
        surf(v);
        shading flat;view(2); colormap('jet');
        xlabel('eigenvector #');
        ylabel('corrector #');
        
        figure('name','harmonic analysis of eigenvectors');
        surf(abs(fft(v(1:end,:))));
        shading flat;view(2); colormap('jet');
        xlabel('eigenvector #');
        ylabel('fft eigenvector #');
    end
    
end
