% oad ESRF measured RM
addpath('/machfs/liuzzo/EBS/MatlabCode/matlab');
addpath(genpath('/machfs/liuzzo/EBS/MatlabCode/matlab/optics'));
addpath('/machfs/liuzzo/EBS/MatlabCode/matlab/qem');
addpath('/machfs/liuzzo/EBS/MatlabCode/ESRF_SR_1994_i2gl');

measfold = '/mntdirect/_machfs/MDT/2018/Feb20/resp1';
sel =1:6:96;
resph=sr.load_normresp(measfold,[measfold '/optics'],'h',sel);   % Load normalized response
respv=sr.load_normresp(measfold,[measfold '/optics'],'v',sel);
respv2h=sr.load_normresp(measfold,[measfold '/optics'],'v2h',sel);   % Load normalized response
resph2v=sr.load_normresp(measfold,[measfold '/optics'],'h2v',sel);
[oh,ov]=sr.load_fresp(measfold);

[qemb,qemres] = qemextract(measfold);
r0 = qemb(1).at;
rfitope = qemb(2).at;

save('MeasurementESRF_SR_2018Feb20_resp1','resph','respv','resph2v','respv2h','oh','ov','rfitope','r0');

rmpath('/machfs/liuzzo/EBS/MatlabCode/matlab');
rmpath(genpath('/machfs/liuzzo/EBS/MatlabCode/matlab/optics'));
rmpath('/machfs/liuzzo/EBS/MatlabCode/matlab/qem');
rmpath('/machfs/liuzzo/EBS/MatlabCode/ESRF_SR_1994_i2gl');

