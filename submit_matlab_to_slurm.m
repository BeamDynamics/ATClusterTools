function submit_matlab_to_slurm(jobname,paramf,commands)
% This function allows to submit compiled matlab jobs to SLURM from a matlab
% scritp running on rnice
% This function works only for login to slurm-asd-access11 without password,
% please add you rsa public key to authorized keys in $HOME/.ssh:
% ssh-keygen -t rsa
% cat id_rsa.pub >> authorized_keys
% 
% Inputs:
% jobname is the name of the compiled matlab script without the extension
% .mat
% paramf is your input parameter file, the number of jobs submitted
% corresponds to the number of lines in this file
% commands is a structure containing the options send to the SLURM sbatch
% field names should be the same as the otpion names with '-' replace by
% '_'

fid=fopen(paramf,'r');
njobs=0;
tline = fgetl(fid);
while ischar(tline)
  tline = fgetl(fid);
  njobs = njobs+1;
end
fclose(fid);

commands.array=['1-' num2str(njobs)];
thisdir = pwd;
% sshc = ['ssh -t slurm-asd-access "cd ' thisdir ' ;"'];
sshc = ['ssh -t cluster-access "cd ' thisdir ' ;"'];

exitc = 'exit';
shname = 'sbatch.sh';
batchc = ['sbatch ' shname];
runc = {['' jobname '.sh $(sed -n ${SLURM_ARRAY_TASK_ID}p ' paramf ')']};
%runc = {'pwd'};

fid = fopen(shname,'w');
fprintf(fid,'#!/bin/sh\n');
fieldsn = fieldnames(commands);
for i = 1:length(fieldsn)
   fprintf(fid,['#SBATCH --' strrep(fieldsn{i},'_','-') ' ' getfield(commands,fieldsn{i}) '\n']);
end

for i =1:length(runc)
    fprintf(fid,[runc{i} '\n']);
end
fclose(fid);

system([sshc ' ' batchc '; ' exitc]);
end

