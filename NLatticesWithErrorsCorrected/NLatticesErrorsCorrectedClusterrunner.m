function NLatticesErrorsCorrectedClusterrunner(inputdatafile)
%NLatticesErrorsCorrectedOARrunner( inputdatafile, outputdatafile)
%
%  this is the function evaluated by each single process on the OAR
%
%   inputdatafile contains variables: 
%           ringFile            AT lattice file
%           ringFileVarName     AT lattice variable name
%           cororder,             number of turns to track
%           neig,               [1-6, 1-6] dimensions to scan
%           outputdatafile      file name for output
%
%   produces a file named outputdatafile that contains the variables:
%           rcor          1XN final dimen(1) coordinate, NaN if lost
% 
%see also: CorrectionChain

path

a=load(inputdatafile);

b=load(a.ringFile); % all variables in ringFile and in b structure
ring=b.(a.ringFileVarName);

outputdatafile=a.outfilename;

%% set errors
rerr=GenericErrorList(ring,a.seed,2.5,b.errampl);

[...
    rcor,...            % corrected lattice
    ch,...              % final H cor values
    cv,...              % final V cor values
    cq,...              % final Quad cor values
    cs,...              % final Skew Quad cor values
    inCOD,...
    d0,de,dc...         % lattice data structures d0=no err, de=err, dc=cor
    ]=CorrectionChain(...
    rerr,...          1) lattice to be corrected
    ring,...             2) lattice without error
    b.indBPM,...          3) monitor indexes in r0 and rerr
    b.indHCor,...         4) h steerers indexed in r0 and rerr
    b.indVCor,...         5) v steerers indexed in r0 and rerr
    b.indSCor,...         6) normal quad correctors indexed in r0 and rerr
    b.indQCor,...         7) skew quad correctors indexed in r0 and rerr
    b.neigenvectors,...   8) eigenvectors for correction and fit
    b.cororder,...        9) correction parameters, order, eigenvectors #
    b.ModelRM,...        10) response matrice structure, if [], rm are computed
    '',...
    false); %#ok<*ASGLU>
 
save(outputdatafile,'rerr','rcor','d0','de','dc','ch','cv','cq','cs');
 
% delete('*.stdout');
% delete('*.stderr');

return
