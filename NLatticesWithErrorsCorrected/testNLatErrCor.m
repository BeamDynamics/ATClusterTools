
% test N lattices with errors
% addpath(genpath('/machfs/liuzzo/EBS/beamdyn/at/atmat/pubtools/LatticeTuningFunctions'))

% test N lattices with errors

warning('This code requires functions not officially part of AT, but in an AT code branch named errors_correction. ')
warning('This code is based on lattice naming used for ESRF-EBS')

% corrects RDT assuming perfect knowledge of lattice. 
% does not measure and fit model with errors. 
cororder=[0 2 1 2 3 6 1 2 3 6 1 2 -1];  

% % corrects RDT based on an RM measurement and fit of model with errors. 
% cororder=[0 2 1 2 3 7 1 2 3 7 1 2 -1];  

neigenvectors=[...
    200,... % n eig orbit H
    200,... % n eig orbit V
    150,... % skew quadrupole vertical dispersion correction
    250,... % quadrupole horizontal dispersion correction
    200,... % quad fit
    100,... % dip fit
    200,... % skew fit
    ];

errampl= 0.01 * [...
    [100e-6, 100e-6, 1e-4];... Dip HV, rot, field
    [50e-6, 50e-6, 5e-4];... Dip-Quad HV, rot, field
    [50e-6, 50e-6, 5e-4];... Quad HV, rot, field
    [50e-6, 50e-6, 35e-4];... Sext HV, rot, field
    [100e-6,    0,      0];... BPM HV
    ];

commands.partition='nice-long';
commands.time=num2str(72*60);

a = load('S28Dmerged.mat');
ring = a.LOW_EMIT_RING_INJ;

save('LatForErrCor.mat','ring');

RunNLatticesErrorCorrectionArray(...
    'LatForErrCor.mat',...        1
    'ring',...     2
    10,...           5
    cororder,...
    neigenvectors,...
    errampl,... % [] to use default
    commands);


