function datafolder=RunNLatticesErrorCorrectionArray(...
    ringfile,...        1
    varringname,...     2
    Nseeds,...          3
    cororder,...        4
    neigenvectors,...   5
    errampl,...         6 
    commands)      %11
% function RunNLatticesErrorCorrectionArray(ringfile,varringname)
%
% created specification files to be used by DynApOARrunner
% and run the processes on the ESRF OAR cluster
%
%     ringfile       : file where the AT lattice is stored
%     varringname    : name of AT lattice variable
%
%     oarspecstring   % optional, string of comands for oar submision.
%                     % default= '-l walltime=6 -q asd'
%                     % usefull:
%                     % -l walltime=72 (automtic que is nice not asd)
%                     % -p "host like ''hpc2-01%%'' OR host like ''hpc2-02%%''"
%
%
%
%
% RunDynApArray runs OAR jobs as ARRAYS!
% oarsub --array 4 ./get_host_info
% cat params.txt
%   Here we put the input parameters of the 1st subjob
%   Here we put the input parameters of the 2nd subjob
%   Here we put the input parameters of the 3rd subjob
% oarsub --array-param-file ./params.txt ./get_host_info
%
%see also:

% this should be input.
routine='NLatticesErrorsCorrectedClusterrunner'; % compiled routine file name.
%pathtoroutine='/mntdirect/_users/liuzzo/Matlab_Work/ATWORK/atwork-code/physics/DA_OAR'; % compiled routine path.
[pathtoroutine,~,exte]=fileparts(which(routine));

if ~exist(fullfile(pathtoroutine,[routine exte]),'file')
    error([routine ' does not exist in: ' pathtoroutine...
        ' . If this is not the correct directory,'...
        ' you may need to add the directory that contains '...
        routine ' to the path.']);
end

%% LATTICE MANIPULATIONS
a=load(ringfile,varringname);
ring=a.(varringname);

[~,ringfile]=fileparts(ringfile);

commonwork=...
    [ringfile '_ns' num2str(Nseeds) ''];%
commonworkdir=fullfile(pwd,commonwork);%
mkdir(commonworkdir);

cd(commonworkdir);

datafolder=pwd;

indHCor = findcells(ring,'iscorH','H');
indVCor = findcells(ring,'iscorV','V');
indSCor = findcells(ring,'iscorS','S');
indQCor = findcells(ring,'iscorQ','Q');
if isempty(indQCor)
indQCor = findcells(ring,'Class','Quadrupole');
end
indBPM  = findcells(ring,'Class','Monitor');

ring=PadPolynomAB(ring);

% initialize BPM errors
zz=zeros(size(indBPM))';
ring=atsetbpmerr(ring,indBPM,zz,zz,zz,zz,zz,zz,zz);

% get RM model

inCOD=[0 0 0 0 0 0]';

if not(exist(fullfile(pwd,'NoErrLatticeMatricesCorParam.mat'),'file'))
ModelRM...
    =getresponsematrices(...
    ring,...
    indBPM,...
    indHCor,...
    indVCor,...
    indSCor,...
    indQCor,...
    [],...
    inCOD,...
    1:12); % all RM
else
    load('NoErrLatticeMatricesCorParam.mat','ModelRM')
end
indHCor_opticsfit = indHCor(1:9*2:end);
indVCor_opticsfit = indHCor(1:9*2:end);

% prepare and save matrices for fit of lattice errors model 
% (uses OAR Cluster!)
FitResponseMatrixAndDispersion(...
    ring,... % nothing to fit. just computes RMs before hands for all seeds
    ring,...
    inCOD,...
    indBPM,...
    indHCor_opticsfit,... % 4 correctors, 1 every 8 cells  THIS HAS TO BE IDENTICAL TO CorrectionChain line 455
    indVCor_opticsfit,...  % 4 correctors, 1 every 8 cells
    [1e6,1e6,1e6,1e6],...
    4,...
    ['fitrm']);

if nargin<4
cororder=[0 2 1 2 3 6 1 2 3 6 1 2 -1];
end

if nargin<5
neigenvectors=[...
    100,... % n eig orbit H
    100,... % n eig orbit V
    150,... % skew quadrupole vertical dispersion correction
    250,... % quadrupole horizontal dispersion correction
    200,... % quad fit
    100,... % dip fit
    200,... % skew fit
    ]; % number of eigenvectors orbit, quadrdt, disp v disp h skewRDT
end

if nargin<6

errampl=[...
    [50e-6, 50e-6, 1e-4];... Dip HV, rot, field
    [50e-6, 50e-6, 5e-4];... DIP-Quad HV, rot, field
    [50e-6, 50e-6, 5e-4];... Quad HV, rot, field
    [50e-6, 50e-6, 35e-4];... Sext HV, rot, field
    [100e-6,    0,      0];... BPM HV
    ];

end

% save here lattice file and RM file once and for all
save('NoErrLatticeMatricesCorParam.mat',...
    'ring',...
    'indBPM',...
    'indHCor','indVCor',...
    ...'indHCor_opticsfit','indVCor_opticsfit',... % unused
    'indSCor','indQCor',...
    'ModelRM',...
    'cororder','errampl','neigenvectors');

%% define error maximum ranges

totprocnum=1;

workdir=pwd;
pfile=fopen('params.txt','w+');

%% LOOP seeds
for indproc=1:Nseeds %
   
    % 1 seed per process
    seed=indproc;
   
    % do not store lattice and RM in this file. they are always the same.
    ringFile=fullfile(workdir,'NoErrLatticeMatricesCorParam.mat');                       %#ok<*NASGU> % AT lattice
    ringFileVarName='ring';                       % AT lattice
    outfilename=fullfile(pwd,['Errors_' num2str(indproc,'%0.4d') '.mat']);
        
    spfn=['specfile_' num2str(indproc,'%0.4d') '.mat'];
    save(spfn,'ringFile','ringFileVarName','outfilename','seed');
    
    % prepare OAR script to run TolErrorSetEvaluatorOARrunner
    %script=[' /opt/matlab_2013a ' fullfile(pwd,spfn) '\n'];
    script=[' /sware/com/matlab_2020a ' fullfile(pwd,spfn) '\n'];
    
    %fopen(fullfile(workdir,'params.txt'),'w+')
    fprintf(pfile,script');
    
    totprocnum=totprocnum+1;
    
end% END LOOP processes
totprocnum=totprocnum-1;% remove last update of totprocnum

routinetorun=[pathtoroutine '/run_' routine '.sh' ];

% hpc
label=[ringfile '_Seed' num2str(Nseeds,'%.4d_') '_']; % label for files.

if nargin<7
    commands.time=num2str(3*60);
end


% run slurm
commands.job_name=label;
submit_matlab_to_slurm(routinetorun(1:end-3),'params.txt',commands)


disp([num2str(totprocnum) ' processes with label: ' label ' are running on cluster (' commands.time ' min)']);

% wait for jobs to finish
a=dir('specfile*.mat');
totproc=length(a);
b=dir('outfile*.mat');
finproc=length(b);

% wait for processes to finish
while finproc~=totproc
    b=dir('Errors_*.mat');

    disp(['waiting for ' num2str(totproc-finproc) '/' num2str(totproc) ' processes to finish.'])
    
    % % % count runnning processes
    b=dir('Errors_*.mat');
    finproc=length(b);

    % wait
    pause(10);
    
end


cd('..') % back to Parent of Working Directory


return