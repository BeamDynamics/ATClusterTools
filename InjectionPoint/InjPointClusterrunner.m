function InjPointClusterrunner(inputdatafile)
%InjEffOARrunner( inputdatafile, outputdatafile)
%  
%   inputdatafile contains variables: 
%           ringFile           AT lattice file
%           ringFileVarName    AT lattice variable name
%           Nturns,     number of turns to track
%           X0
%           quantdiffFlag (optional, default is 1 for compatibility)
%  
%   produces a file named outputdatafile that contains the variables:
%           lost    flag for lost particle
%           X0      6XN vector of intial coordinate
%           X0end   6XN vector of final coordinate
% 
%see also   

a=load(inputdatafile);

b=load(a.ringFile,a.ringFileVarName);
r=b.(a.ringFileVarName);
outputdatafile=a.outfilename;

% 
% if isfield(a, 'quantdiffFlag')
%     quantdiffFlag = a.quantdiffFlag;
% else
%     quantdiffFlag = 1;
% end
% 
% % quantum diffusion
% if quantdiffFlag
%     r = atradoff(r);
%     qde=atQuantDiff('QuantDiff',r);
% end
% 
% % radiation and cavity ON
% 
% % % % % atsetRFCavity
% ring=r;
% DHzmod=0;
% 
% indrfc=find(atgetcells(ring,'Class','RFCavity'));
% 
% if ~isempty(indrfc)
%     
%     f0=ring{indrfc(1)}.Frequency;
%     V=sum(cellfun(@(a)a.Voltage,ring(indrfc),'un',1));
%     H=ring{indrfc(1)}.HarmNumber;
%     alpha=mcf(ring);
%     ring=atsetRFCavity(ring,V,0,H,0);
%     
%     % compute DRF untill convergence.
%     tt=findorbit6(ring,1);
%     DHzmod=DHzmod+tt(5)*alpha*f0;
%     ring=atsetRFCavity(ring,V,0,H,DHzmod);
%     
%     tt=findorbit6(ring,1);
%     DHzmod=DHzmod+tt(5)*alpha*f0;
%     ring=atsetRFCavity(ring,V,0,H,DHzmod);
%     
%     tt=findorbit6(ring,1);
%     DHzmod=DHzmod+tt(5)*alpha*f0;
%     ring=atsetRFCavity(ring,V,0,H,DHzmod);
%     
%     disp(['Delta RF : ' num2str(DHzmod,'%2.1f') ' Hz'])
% else
%     warning('No RF cavity');
% end
% 
% r=atsetRFCavity(ring,V,1,H,DHzmod);
% 
% % % % % normal atsetcavity
% % indrf=findcells(r,'Class','RFCavity');
% %
% % r=atsetcavity(atradon(r),...
% %     sum(getcellstruct(r,'Voltage',indrf)),...
% %     1,...
% %     r{indrf(1)}.HarmNumber);
% 
% 
% 
% 
% 
% if quantdiffFlag
%     r=[r;{qde}];
% end

[~,~,NT,lossinfo]=ringpass(r,a.X0,a.Nturns);

nelpas=lossinfo.element+length(r).*NT;
X0=a.X0;

codstd=cellfun(@(a)std(findorbit4Err(r,0,1:length(r),a),0,2),mat2cell(X0,[6],ones(1,length(X0))),'un',0);

codmat=[codstd{:}]; % 4xlengh(incoor) 4=x,x',y,y' COD std for each incoor
codx=codmat(1,:);
cody=codmat(2,:); % 


save(outputdatafile,'lossinfo','NT','X0','nelpas','codx','cody');


if a.collect && a.indproc==1
    CollectInjPointClusterData(a.datafolder,'Results.mat');
end


return


