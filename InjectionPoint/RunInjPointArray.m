function datafolder=RunInjPointArray(...
    ringfile,...
    varringname,...
    Nturns,...
    NGrid,...
    NProc,...
    DIM,...
    foldnamespec,...
    quantdiffFlag,...
    slurmcommands,...
    collect)
% function RunInjPointArray(ringfile,varringname)
%
% created specification files to be used by InjPointClusterrunner
% and run the processes on the ESRF cluster
% 
%     ringfile       : file where the AT lattice is stored
%     varringname    : name of AT lattice variable 
%
%     Nturns=2^11;          % total tracking turns
%     NGrid=20 ;            % grdi size N points scanned = Ngrid^length(DIM)  
%     Nproc=200;            % number of OAR processors to use. 
%     DIM=[1e-3 1e-5 1e-3 1e-4 0.1 0.1];    % 6D abs of maximum size in each dimension to scan
%     DIM=[1e-3 1e-5 1e-3 1e-4];          % 4D abs of maximum size in each dimension to scan
%     DIM=[1e-3 1e-3];                     % X-Y abs of maximum size in each dimension to scan
%     foldnamespec:        folder name additional label
%     quantdiffFlag:       if 0 no quantum diffusion is added. default is 1
%     slurmcommands : structure see submit_matlab_to_slurm
%     collect  (defualt = false)
% 
% routine used should be compiled with compileInjPoint.m
%
%see also: 

% this should be input.
routine='InjPointClusterrunner'; % compiled routine file name.
%pathtoroutine='/mntdirect/_users/liuzzo/Matlab_Work/ATWORK/atwork-code/physics/Fmaps_findtunetom2'; % compiled routine path.

[pathtoroutine,~,exte]=fileparts(which(routine));

if ~exist(fullfile(pathtoroutine,[routine exte]),'file')
    error([routine ' does not exist in: ' pathtoroutine...
        ' . If this is not the correct directory, '...
        ' you may need to add the directory that contains '...
        routine ' to the path.']);
end

%% LATTICE MANIPULATIONS
a=load(ringfile,varringname);
ring=a.(varringname);


if nargin<10
collect = false
end

if nargin<9
slurmcommands.time=num2str(60);
end

if nargin<8
    quantdiffFlag=1;
end

if nargin<7
    foldnamespec='';
end

[~,ringfile]=fileparts(ringfile);

commonwork=...
    [ringfile '_nt' num2str(Nturns) '_ns'...
    num2str(NGrid) '_' foldnamespec];%
commonworkdir=fullfile(pwd,commonwork);%
mkdir(commonworkdir);

cd(commonworkdir);

datafolder=pwd;

% place here lattice file and RM file
save('latticefile.mat','ring');

%% define error maximum ranges

totprocnum=1;

workdir=pwd;
pfile=fopen('params.txt','w+');

DX=[];
DXP=[];
DY=[];
DYP=[];
DDP=[];
DCT=[];

inCOD=[0 0 0 0 0 0]';

if length(DIM)==6
    DX=linspace(-1,1,NGrid)*DIM(1);
    DXP=linspace(-1,1,NGrid)*DIM(2);
    DY=linspace(-1,1,NGrid)*DIM(3);
    DYP=linspace(-1,1,NGrid)*DIM(4);
    DDP=linspace(-1,1,NGrid)*DIM(5);
    DCT=linspace(-1,1,NGrid)*DIM(6);
    
    % initial coordinate matrix
    [x,xp,y,yp,dd,ct]=ndgrid(DX,DXP,DY,DYP,DDP,DCT);
    X6=repmat(inCOD,1,length(x(:)))+...
        [x(:),xp(:),y(:),yp(:),dd(:),ct(:)]';
    
elseif length(DIM)==4
    DX=linspace(-1,1,NGrid)*DIM(1);
    DXP=linspace(-1,1,NGrid)*DIM(2);
    DY=linspace(-1,1,NGrid)*DIM(3);
    DYP=linspace(-1,1,NGrid)*DIM(4);
    
    % initial coordinate matrix
    [x,xp,y,yp]=ndgrid(DX,DXP,DY,DYP);
    X6=repmat(inCOD,1,length(x(:)))+...
        [x(:),xp(:),y(:),yp(:),zeros(size(x(:))),zeros(size(x(:)))]';
    
elseif length(DIM)==2
    DX=linspace(-1,1,NGrid)*DIM(1);
    DY=linspace(-1,1,NGrid)*DIM(2);
    
    % initial coordinate matrix
    [x,y]=ndgrid(DX,DY);
    X6=repmat(inCOD,1,length(x(:)))+...
        [x(:),zeros(size(x(:))),y(:),zeros(size(x(:))),zeros(size(x(:))),zeros(size(x(:)))]';
    
else
    error('size of DIM should be 2,4 or 6')
end

NPart=size(X6,2);

[pointsperprocess]=DivideClusterJobs(NPart,NProc);

save('GlobalTolParameters.mat',...
    'NGrid','NPart','Nturns','ringfile','varringname','NProc',...
    'pointsperprocess');

%% LOOP FMA points
for indproc=1:NProc %
    
    % prepare SpecsFile
    
    % do not store lattice and RM in this file. they are always the same.
    ringFile=fullfile(workdir,'latticefile.mat');                       %#ok<*NASGU> % AT lattice
    ringFileVarName='ring';                       % AT lattice
    numberofturns=Nturns;     % number of turns for tracking
    outfilename=fullfile(pwd,['outfile_' num2str(indproc,'%0.4d') '.mat']);


    if indproc<NProc
        X0=X6(:,sum(pointsperprocess(1:indproc-1))+1:sum(pointsperprocess(1:indproc)));
    else %last proces takes left overs
        X0=X6(:,sum(pointsperprocess(1:indproc-1))+1:end);
    end
    
    spfn=['specfile_' num2str(indproc,'%0.4d') '.mat'];
    save(spfn,'ringFile','ringFileVarName','outfilename',...
        'X0','Nturns','quantdiffFlag','collect','indproc','foldnamespec','workdir');
    
    % prepare cluster script to run 
    script=[' /sware/com/matlab_2020a ' fullfile(pwd,spfn) '\n'];
    
    %fopen(fullfile(workdir,'params.txt'),'w+')
    fprintf(pfile,script');
    
    totprocnum=totprocnum+1;
    
end% END LOOP processes
 
totprocnum=totprocnum-1;
 
routinetorun=[pathtoroutine '/run_' routine '.sh' ];
            
% hpc
label=[ringfile '_St' num2str(NGrid) '_P' num2str(NProc) '_']; % label for files.

slurmcommands.job_name=label;
submit_matlab_to_slurm(routinetorun(1:end-3),'params.txt',slurmcommands)


disp([num2str(totprocnum) ' processes with label: ' label ' are running on cluster']);

cd('..') % back to Parent of Working Directory

return