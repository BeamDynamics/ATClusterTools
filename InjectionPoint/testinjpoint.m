clear all
close all

 load('../S28Dmerged.mat')
 rcor=LOW_EMIT_RING_INJ;
 
 errampl=[...
    [100e-6, 100e-6, 1e-4];... Dip HV, rot, field
    [50e-6, 50e-6, 5e-4];... Dip-Quad HV, rot, field
    [50e-6, 50e-6, 5e-4];... Quad HV, rot, field
    [50e-6, 50e-6, 35e-4];... Sext HV, rot, field
    [100e-6,    0,      0];... BPM HV
    ];

 rerr=GenericErrorList(rcor,1,2.5,errampl);
 %rerr=rcor;
 
rfv=6.5;
npartinj=1e4;
radonflag=1;

r=atsetcavity(atradon(rerr),rfv*1e6,radonflag,992);

%% run processes

 [injpoint,dfinjeffrb]=getInjPoint(r,11,5,50,['beam' 'RFrb' '4Derr' 'COD']);
% injpoint=CollectInjPointClusterData(dfinjeffrb,'test');

