function [injpoint,datafolder]=getInjPoint(...
    ring,NGrid,Nturns,Nproc,addlabel)
%[injpoint,datafolder]=getInjPoint(...
%    ring,NGrid,Nturns,Nproc,addlabel)
%
% computes optimal injection point
%
% ring: AT lattice
% Ngrid : number of particles Ngrid^6
% Nturns : number of turns to test (3)
% Nproc : number of proces on OAR
% addlabel: label for output folder
%
%
% injpoint : injection point 6x1
% datafolder : folder for data. no data collection if this is an output.
%
%
% example: injpoint=getInjEff(ring,51);
%
%see also: RunInjPointArray
%          CollectInjPointOARData

% tracking parameters
% Nturns=2^11;
% Nproc=100;


if nargin<5, addlabel=''; end
if nargin<4, Nproc=100;   end
if nargin<3, Nturns=5;  end
if nargin<2, NGrid=51; end

DIM=[20e-3 2e-3 20e-3 2e-3 ]/10; 
ringfile=[addlabel 'latOAR.mat'];
save(ringfile,'ring');
%% run job on OAR
slurmspec.time='60';
datafolder=RunInjPointArray(...
    fullfile(ringfile),...  % file where the AT lattice is stored
    'ring',...    % name of lattice variable
    Nturns,...
    NGrid,...
    Nproc,...
    DIM,...
    [addlabel],...
    false,...
    slurmspec,...
    false); 

% delete lattice file used for tracking
delete(ringfile);

%% collect data
%injeff=CollectInjEffOARData(datafolder,'test');

injpoint=NaN;
if nargout<4
    injpoint=CollectInjPointClusterData(datafolder,['INJPOINTdata' addlabel '.mat']);
    
    disp(['Injection Point= ' num2str(injpoint) ' m']);
    
    save(['INJPOINTdata' addlabel '.mat'],'injpoint')

end
if nargout==1
system(['rm -rf ' datafolder]);
end
return
