function [injpoint,nelpas]=CollectInjPointClusterData(datafolder,outputfilename)
% [injpoint,nelpas]=CollectInjPointOARData(datafolder,outputfilename)
%
% datafolder: folder of data generated with RunDynApArray
% outputfilename: file where to save the data
%
% injpoint:  6D coordinates of best injection point
% nelpas: number of elements passed by each particle tested.
%
%see also: RunInjEffArray

currdir=pwd;

cd(datafolder)
disp(['Extracting results from ' datafolder])

a=dir('specfile*.mat');
totproc=length(a);
b=dir('outfile*.mat');
finproc=length(b);

% wait for processes to finish
while finproc~=totproc
    b=dir('outfile*.mat');
    
    disp(['waiting for ' num2str(totproc-finproc) '/' num2str(totproc) ' processes to finish.'])
    
    % % % count runnning processes
    b=dir('outfile*.mat');
    finproc=length(b);
    
    % wait
    pause(10);
    
end

% delete OAR files
%delete('*.stdout');
%delete('*.stderr');


% if ~exist('AllData.mat','file')
x=[];
np=[];
cx=[];
cy=[];

lossdetails=struct('lost', zeros(1,1), 'turn', inf(1,1),...
    'element', nan(1,1), 'coordinates', nan(6,1,6));
y=[];
xp=[];
yp=[];
d=[];
ct=[];
nt=[];

for iproc=1:totproc
    load(['outfile_' num2str(iproc,'%0.4d') '.mat'],'lossinfo','NT','X0','nelpas','codx','cody');
    nt=[nt NT];
    np=[np nelpas];
    cx=[cx codx];
    cy=[cy cody];
    x =[x X0(1,:) ];
    y =[y X0(2,:) ];
    xp=[xp X0(3,:)];
    yp=[yp X0(4,:)];
    d =[d X0(5,:) ];
    ct=[ct X0(6,:)];
    
    %if nargin>2 || nargout>3
    lossdetails.lost = [lossdetails.lost lossinfo.lost];
    lossdetails.turn = [lossdetails.turn lossinfo.turn];
    lossdetails.element = [lossdetails.element lossinfo.element];
    %                lossdetails.coordinates = cat(2, lossdetails.coordinates, lossinfo.coordinates);
    %end
    try
    catch
        disp(['failed to load ' ['outfile_' num2str(iproc,'%0.4d') '.mat']])
    end
end


X0=[x;y;xp;yp;ct;d];

nelpas=np;

if ~exist('AllData.mat','file')
    save('AllData.mat','X0','nelpas','nt','cx','cy','lossdetails');
    
    % delete OAR files
    delete('*.stdout');
    delete('*.stderr');
    % delete spec and out files
    delete('spec*.mat');
    delete('out*.mat');
    
else
    load('AllData.mat')
    %     disp('erase AllData.mat')
    %     injeff=[];
    %     lost=[];X0=[];xend=[];lossdetails=[];
    %    return
end


% if more then one point, take closest to center.

codstd=nan(size(cx));
indcod=~isnan(cx) & ~isnan(cy);
codstd(indcod)=cx(indcod)+cy(indcod);

[val,indb]=min(codstd);

if ~isnan(val)
    injpoint=X0(:,indb)';
else
    injpoint=[0 0 0 0 0 0];
end


% dist=sqrt(X0(1,:).^2+X0(2,:).^2+X0(3,:).^2+X0(4,:).^2);
% 
% [~,indb]=max(np);
% 
% injpoint=X0(:,indb)';

save(outputfilename,'injpoint');


if nargin>1
    
    load('GlobalTolParameters.mat','Npart','Nproc','Nturns');
    lost=[lossdetails.lost];
    x=X0(1,:);
    xp=X0(2,:);
    y=X0(3,:);
    yp=X0(4,:);
    d=X0(5,:);
    ct=X0(6,:);
    
    ux=unique(x);
    uy=unique(y);
    uyp=unique(yp);
    uxp=unique(xp);
    N=squeeze(reshape(nelpas,length(ux),length(uxp),length(uy),length(uyp)));
    T=squeeze(reshape(nt,length(ux),length(uxp),length(uy),length(uyp)));
    %N=squeeze(reshape(cx,length(ux),length(uxp),length(uy),length(uyp)));
    %T=squeeze(reshape(cy,length(ux),length(uxp),length(uy),length(uyp)));
    
    if length(ux)>1 && length(uxp)==1
        figure;
        contour(ux,uy,T);colorbar;
        figure;
        contour(ux,uy,N);colorbar;
    end
    
    if length(ux)>1 && length(uxp)>1
        
        for slice=1:1:length(unique(x))
            %slice=30;
            
            figure;
            subplot(1,2,1)
            contour(ux,uy,log2(squeeze(N(:,slice,:,slice))));colorbar;
            title(['xp: ' num2str(uxp(slice)) ' yp: ' num2str(uyp(slice))]);
            subplot(1,2,2)
            contour(uxp,uyp,log2(squeeze(N(slice,:,slice,:))));colorbar;
            title(['x: ' num2str(ux(slice)) ' y: ' num2str(uy(slice))]);
        end
        
        
    end
    
end

cd(currdir)

if nargout>0
    % remove data direcotry
    %system(['rm -r ' datafolder]);
end

return
