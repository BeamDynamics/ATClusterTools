function TouLTClusterrunner(inputdatafile)
%TouLTClusterrunner( inputdatafile )
%
%  this is the function evaluated by each single process on the Cluster cluster
%
%   inputdatafile contains variables: 
%           ringFile            AT lattice file
%           ringFileVarName     AT lattice variable name
%           Nturns,             number of turns to track
%           indMomAp            index where momentum aperture will be
%                               computed
%           outputdatafile      file name for output
%
%   produces a file named outputdatafile that contains the variables:
%           indMomAp            index where momentum aperture will be
%                               computed
%           Nturns              number of turns to track
%           deltap              positive momentum acceptance
%           deltan              negative momentum acceptance
%           s                   s coordinate where momap is computed
%
%  The function calls atmomap_manypos
%
%  see also: atmomap_manypos RunTouLTArray

a=load(inputdatafile);

b=load(a.ringFile,a.ringFileVarName);
ring=b.(a.ringFileVarName);
Nturns=a.Nturns;
indMomAp=a.indMomAp;
outputdatafile=a.outfilename;

[deltap,deltan]=atmomap_manypos(ring, Nturns, indMomAp);
s=findspos(ring,indMomAp);

save(outputdatafile,'s','deltap','deltan','Nturns','indMomAp');
if a.collect && a.indproc==1
    CollectTouLTClusterData(a.datafolder);
end

return
