function datafolder=RunTouLTArray(...
    ringfile,...        1
    varringname,...     2
    Nturns,...          3
    From,...            4
    To,...              5
    emitx,...           6
    emity,...           7
    BL,...              8
    sigma_delta,...     9
    Ib,...              10
    Nproc,...           11
    addlabel,...        12
    commands,...        13
    collect)           %14
%%
%  This function creates specification files to be used by TouLTClusterrunner
%  and runs the processes on the ESRF Cluster cluster
%  
%   datafolder = RunTouLTArray(...
%                   ringfile,...        1
%                   varringname,...     2
%                   Nturns,...          3
%                   From,...            4
%                   To,...              5
%                   emitx,...           6
%                   emity,...           7
%                   BL,...              8
%                   sigma_delta,...     9
%                   Ib,...              10
%                   Nproc,...           11
%                   addlabel,...        12
%                   commands,...        13
%                   collect)           %14
%
%     ringfile       % file where the AT lattice is stored (with full path)
%     varringname    % name of AT lattice variable 
%     Nturns         % total tracking turns
%     From           % index of first element where function computes momap
%     To             % index of last element where function computes momap
%     emitx          % hor emittance (m)
%     emity          % ver emittance (m)
%     BL             % bunch length (m) (may be a vector)
%     sigma_delta    % energy spread (may be a vector of the same size of BL)
%     Ib             % bunch current (A) (may be a vector of the same size of BL)
%     Nproc          % number of Cluster processors to use. 
%     addlabel       % default='', added to the folder name and process name
%     commands       % structure of commands for the slurm cluster
%     collect        % if true then the collect is done by the first job
% 
%  You must compile the function TouLTClusterrunner before using this function.
%  Example:  mcc('-vm','TouLTClusterrunner.m','passmethodlist.m'); exit
%  where passmethodlist.m is a file with all the passmethods
% 
%  RunTouLTArray runs Cluster jobs as ARRAYS! 
%
%  Momentum acceptance is computed in all dipoles, drifts, quadrupoles and
%  sextupoles of the ring between the indexes 'From' and 'To'.
%  The computation done in two cells is already good.
%  An interval of an integer number of cells is better.
%
%  see also: TouLTClusterrunner atmomap_manypos DivideClusterJobs

if nargin<12
    addlabel='';
end
if nargin<13 
    commands.time = num2str(12*60) ; % time in minutes
end
if nargin<14
    collect=false;
end

if ~isstruct(commands)
    warning('input ''commands'' must be a structure and it is not. Default values are used.');
    clear commands;
    commands.time = num2str(12*60) ; % time in minutes
end

if (length(BL)~=length(Ib)) || (length(BL)~=length(sigma_delta))
    warning('BL, Ib and sigma_delta must have the same length. using only BL(1), Ib(1) and sigma_delta(1)');
    BL=BL(1);
    Ib=Ib(1);
    sigma_delta=sigma_delta(1);
end

routine='TouLTClusterrunner'; % compiled routine file name.

[pathtoroutine,~,exte]=fileparts(which(routine));

if ~exist(fullfile(pathtoroutine,[routine exte]),'file')
    error([routine ' does not exist in: ' pathtoroutine...
        ' . If this is not the correct directory,'...
        ' you may need to add the directory that contains'...
        routine ' to the path.']);
end

%% LATTICE MANIPULATIONS
a=load(ringfile,varringname);
ring=a.(varringname);
%tflag=type;
 
[~,ringfile]=fileparts(ringfile);

commonwork=...
    [ringfile '_' varringname '_nt' num2str(Nturns) '_From' num2str(From) ...
    '_To' num2str(To) addlabel];%

commonworkdir=fullfile(pwd,commonwork);%
mkdir(commonworkdir);

cd(commonworkdir);

datafolder=pwd;

% place here lattice file and RM file
save(fullfile(datafolder,'latticefile.mat'),'ring');

pfile=fopen('params.txt','w+');

%% define indexes where you want to compute momentum aperture
% indDip=findcells(ring,'Class','Bend');
% indDrift=findcells(ring,'Class','Drift');
% indQuad=findcells(ring,'Class','Quadrupole');
% indSext=findcells(ring,'Class','Sextupole');
L=atgetfieldvalues(ring,'Length');
indLmorethan0=find(L>0)';

indID=findcells(ring,'FamName','ID\w*');

indMomApAll=indLmorethan0(From<=indLmorethan0 & indLmorethan0<=To);

pts=length(indMomApAll); %  points to scan
if (Nproc>pts)
    Nproc=pts;
end
[pointsperprocess]=DivideClusterJobs(pts,Nproc);



%% LOOP on processes
for indproc=1:Nproc %
    
    % prepare SpecsFile
    
    % do not store lattice and RM in this file. they are always the same.
    ringFile=fullfile(datafolder,'latticefile.mat');                       %#ok<*NASGU> % AT lattice
    ringFileVarName='ring';                       % AT lattice
    numberofturns=Nturns;     % number of turns for tracking
    outfilename=fullfile(pwd,['outfile_' num2str(indproc,'%0.4d') '.mat']);
        
    indMomAp=indMomApAll(sum(pointsperprocess(1:indproc-1))+1:sum(pointsperprocess(1:indproc)));
        
    spfn=['specfile_' num2str(indproc,'%0.4d') '.mat'];
    save(fullfile(datafolder,spfn),'ringFile','ringFileVarName','outfilename',...
        'Nturns','indMomAp','collect','datafolder','indproc');
    
    % % prepare Cluster script
    script=[' /sware/com/matlab_2020a ' fullfile(datafolder,spfn) '\n'];
    
    %fopen(fullfile(workdir,'params.txt'),'w+')
    fprintf(pfile,script');
    
    
end% END LOOP processes

%%
routinetorun=[pathtoroutine '/run_' routine '.sh' ];
save(fullfile(datafolder,'GlobalTolParameters.mat'),...
    'Nturns','ringfile','varringname','Nproc',...
    'pts','pointsperprocess','indMomApAll','From','To',...
    'emitx','emity','BL','sigma_delta','Ib');      

% hpc
label=[ 'TouLT_P' num2str(Nproc) addlabel]; % label for files.


fclose('all');

% run slurm
commands.job_name=label;
submit_matlab_to_slurm(routinetorun(1:end-3),'params.txt',commands)


cd('..') % back to Parent of Working Directory

disp([num2str(Nproc) ' processes with label: ' label ' are running on Cluster (' commands.time ' min)']);

return

