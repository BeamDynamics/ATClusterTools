function [TL,indMomAp,s,deltap,deltan]=CollectTouLTClusterData(datafolder)
%
%  [TL,indMomAp,s,deltap,deltan]=CollectTouLTClusterData(datafolder)
%
%  The input of this function is the name of the folder where all the
%  momentum acceptance datas have been stored by the function
%  RunTouLTArray. The output of RunTouLTArray is the input of
%  CollectTouLTClusterData.
%  datafolder: folder of data generated with RunTouLTArray
%
%  output:
%  TL is the Touschek lifetime in seconds, computed with Piwinski formula
%  indMomaAp is the vector of indexes where momap has been computed
%  s is the vector of s position where momap has been computed
%  deltap and deltan are the positive and negative momentum acceptances
%
%  TL is an array the same size of BL and Ib input of RunTouLTArray
%
%  see also: RunTouLTArray TouLTClusterrunner TouschekPiwinskiSimplified atmomap_manypos

curdir = pwd;

if exist(fullfile(datafolder,'ResultsLT.mat'),'file')
    
    disp('output already exists');
    load(fullfile(datafolder,'ResultsLT.mat'));
    
else
    
    cd(datafolder)
    a=dir(fullfile(datafolder,'specfile*.mat'));
    totproc=length(a);
    b=dir(fullfile(datafolder,'outfile*.mat'));
    finproc=length(b);
    load(fullfile(datafolder,'latticefile.mat'));
    collect_done_externally = false;
    
    % wait for processes to finish
    while finproc~=totproc & ~collect_done_externally
        b=dir(fullfile(datafolder,'outfile*.mat'));
        disp(['waiting for ' num2str(totproc-finproc) '/' num2str(totproc) ' processes to finish.'])
        
        % % % count running processes
        b=dir(fullfile(datafolder,'outfile*.mat'));
        finproc=length(b);
        
        % check if ResultsLT.mat exists, some other action may have created it.
        if exist(fullfile(datafolder,'ResultsLT.mat'),'file')
            collect_done_externally = true;
        end
        
        % wait
        pause(10);
    end
    
    if ~collect_done_externally

        load(fullfile(datafolder,'GlobalTolParameters.mat'),'emitx','emity','Ib','sigma_delta','BL');
        deltapAll=[];
        deltanAll=[];
        sAll=[];
        indMomApAll=[];
        for iproc=1:totproc
            load(['outfile_' num2str(iproc,'%0.4d') '.mat'],...
                'deltap','deltan','s','indMomAp');
            deltapAll=[deltapAll deltap];
            deltanAll=[deltanAll deltan];
            sAll=[sAll s];
            indMomApAll=[indMomApAll indMomAp];
        end
        deltapAll(deltapAll==0)=0.0001;
        deltanAll(deltanAll==0)=-0.0001;
        
        % rp=ringpara(ringNoRad);
        % sigmadelta=rp.sigma_E;%params.espread;
        TL=zeros(size(Ib));
        for iic=1:length(Ib)
            [TL(iic),~]=TouschekPiwinskiLifeTime(ring,...
                [deltapAll' deltanAll'],Ib(iic),...
                indMomApAll,emitx,emity,'integral',sigma_delta(iic),BL(iic));
        end
        
        indMomAp=indMomApAll;
        s=sAll;
        deltap=deltapAll;
        deltan=deltanAll;
        save('ResultsLT.mat','TL','indMomAp','s','deltan','deltap');
        
        % delete Cluster files
        delete('*.stdout');
        delete('*.stderr');
        % delete spec and out files
        delete('spec*.mat');
        delete('out*.mat');
        % delete SLURM files
        delete('slurm*.out');
        
        cd(curdir)

    else
        disp('Data already collected by an other call to CollectTouLTClusterData.')
        load(fullfile(datafolder,'ResultsLT.mat'))
    end
end

if nargout>0
    % remove data direcotry
    system(['rm -r ' datafolder]);
end

return