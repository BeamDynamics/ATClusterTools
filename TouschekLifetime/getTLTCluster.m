function [tlt_hours,datafolderlt]=getTLTCluster(...
    rcor,...   AT lattice 
    ...distinctive_label,... string to define a specific folder name to store computations
    ...From,...  index to start momentum acceptance (ID 4)
    ...To,...    index to end momentum accetnace (ID 8)
    ...Clusterspec,...  structure to specify cluster parameters (see submit_matlab_to_slurm.m)
    ...Nproc,...   number of processes to use for computation
    varargin....  optional inputs ...,'Name',value,...
    )
%
% computed Touscheck lifetime using cluster in hours
%
% [tlt,datafolderlt]=getTLTCluster(...
%     rcor,...   AT lattice 
%     distinctive_label,... string to define a specific folder name to store computations
%     From,...  index to start momentum acceptance (ID 4)
%     To,...    index to end momentum accetnace (ID 8)
%     Clusterspec,...  structure to specify cluster parameters (see submit_matlab_to_slurm.m)
%     Nproc,...   number of processes to use for computation
%     varargin....  optional inputs ...,'Name',value,...
%     )
%
% Required inputs:
%
% rcor,...   AT lattice with correctly set RF cavity
% 
%
% Positional inputs (if given, must be in this order):
%
% distinctive_label,... string to define a specific folder name to store computations
%               (default= '')
% From,...  index to start momentum acceptance 
%           (default: 4th element with name starting with ID, or 1 if missing)
% To,...    index to end momentum accetnace 
%           (default: 8th element with name starting with ID, or length(rcor) if missing)
% Clusterspec,...  structure to specify cluster parameters (see submit_matlab_to_slurm.m)
% Nproc,...   maximum number of processes to use for computation (defualt: 100)
% 
%
% Optional inputs ...,'Name',value,...
%
% ...,'Nturns', 2^11,...   turns to track to find momenetum aperture 
% ...,'DriftsToSplit',{'DR_01','DR_45'},... drifts to split for more
%                                           accurate momentum aperture
% ...,'CurrentPerBunch',[0.2/harmonic_number*7/8, 0.92/16 0.4/4],... array of current per
%                                           bunch to compute bunch length
% ...,'VerticalEmittance',5e-12,...
% ...,'Impedance',0.67,...
%
%
% Outputs:
%
% tlt_hours : array of Touschek lifetime values (in seconds) for each Ib.
%             a file named ['TLTdata' distinctive_label] is also saved
% datafolder:  if this output is requested the tlt output is NaN and has to
%      be retrived either via a call to CollectTouLTClusterData(datafolder)
%      or reading the file ResultsTLT.m (TL variable in seconds)
%         
% 
% examples: 
%
% % waits till TLT has been computed and returns an array of lifetimes. Also saves a file with all details.
% tlt=getTLTCluster(r,'test',230,342,'VerticalEmittance',10*1e-12);
% 
% % does not wait, data will be saved to file when ready. 
% [tlt,datafolder]=getTLTCluster(r,'CurrentPerBunch',0.035);
% tlt returns NaN, when ready the data will be stored in ./***distintinctive_label***/ResultsTLT.mat 
% 
%see also: RunTouLTArray CollectTouLTClusterData submit_matlab_to_slurm

rp=ringpara(rcor);
emitx=rp.emittx;%b.modemittance(1); %
sigmadelta=rp.sigma_E;%9.4669e-04;
harm = rp.harm;

indID=findcells(rcor,'FamName','ID\w*');
if length(indID)>8
    def_From=indID(4);            %4
    def_To=indID(8);
else
    def_From=1;            %4
    def_To=length(rcor);
end
def_Clusterspec.time = num2str(60);
    
p = inputParser;
addRequired(p,'rcor',@iscell);
addOptional(p,'distinctive_label','',@ischar);
addOptional(p,'From',def_From,@isscalar);
addOptional(p,'To',def_To,@isscalar);
addOptional(p,'Clusterspec',def_Clusterspec,@isstruct);
addOptional(p,'Nproc',100,@isscalar);
addParameter(p,'Nturns',2^10,@isscalar)
addParameter(p,'DriftsToSplit',{'DR_01','DR_45'},@iscell)
addParameter(p,'CurrentPerBunch',[0.2/floor(harm*7/8), 0.92/16 0.4/4],@isnumeric)
addParameter(p,'VerticalEmittance',[5e-12],@isnumeric)
addParameter(p,'Impedance',0.67,@isscalar)
addParameter(p,'auto_collect',true,@isscalar)


parse(p,rcor,varargin{:})

distinctive_label = p.Results.distinctive_label;
From = p.Results.From;
To = p.Results.To;
Clusterspec = p.Results.Clusterspec;
Nproc = p.Results.Nproc;
Nturns = p.Results.Nturns;
DriftsToSplit = p.Results.DriftsToSplit;
Ib = p.Results.CurrentPerBunch;
emity = p.Results.VerticalEmittance;
Zn = p.Results.Impedance;

if To<=From
    error(['requested computation on an empty list of indexes: ['...
        num2str(From) ', ' num2str(To) ']']);
end

% split drifts
refpts=findcells(rcor,'FamName',DriftsToSplit{:});
if ~isempty(refpts)
rcor=atinsertelems(rcor,refpts,...
    1/8,[],2/8,[],3/8,[],4/8,[],...
    5/8,[],6/8,[],7/8,[]...
    );
end

% save locally a lattice file with splitted drifts to use for computation
varringname='rcor';
latfilename=[distinctive_label '.mat'];
save(latfilename,varringname);

% compute bunch length for each current
try
    BL = atBunchLength(rcor,Ib,Zn);
catch
    error('Could not compute bunch length. The RF cavity is correctly set?')
end

if any(isnan(BL))
    error('Could not compute bunch length. The RF cavity is correctly set?')
end

if nargout==1
    collect = false;
end

if nargout==2
    collect = p.Results.auto_collect;
end

% run jobs on cluster
tic;
datafolderlt= RunTouLTArray(...
    latfilename,...
    varringname,...
    Nturns,...
    From,...
    To,...
    emitx,...
    emity,...
    BL,...
    sigmadelta*ones(size(BL)),...
    Ib,...
    Nproc,...
    '',...
    Clusterspec,...
    collect);
toc

% delete lattice file used for tracking
delete(latfilename);

tlt_hours=NaN;

if nargout==1 && collect==false
    [TL,indMomAp,s,deltap,deltan]=CollectTouLTClusterData(datafolderlt);
    tlt=TL/3600;
    tlt_hours = tlt;
    toc
    
    save(['TLTdata' latfilename],'tlt','TL','indMomAp','s','deltap','deltan',...
        'emitx','emity','BL','Ib','sigmadelta','Zn');
end
