function [deltap,deltan] = atmomap_manypos(ring, Nturn, index)
%
%   [deltap,deltan] = atmomap_manypos (ring, Nturn, index)
%
%   the function finds momentum aperture in positions index of the ring
%
%   ring is the AT ring, a ring with radiation and cavity is better
%   Nturn is the number of turns for tracking
%   index is a vector of indexes where you want the momentum aperture vaues
%
%   I compute the closed orbit of the ring and the momentum acceptance
%   starting from the closed orbit, not from [0;0;0;0;0;0]!
%
%   The resolution in delta is 0.001
%

ClO=findorbit6 (ring, index);
ClO(1,:)= ClO(1,:)+1e-6; % following M.Borland advice on Elegant guide to sample resonances
ClO(3,:)= ClO(3,:)+1e-6;

for ii=1:length(index)
    
    ring2=[ring(index(ii):end);ring(1:index(ii)-1)];
    
    %lin=atlinopt(ring2,0);
    %ClO=lin.ClosedOrbit;
    
    loss=0;
    i=0;
    j=0;
    k=0;
    while (loss==0&&i<10)
        i=i+1;
        dpP=0+i*0.01;
        [a,loss]=ringpass(ring2, [ClO(1:4,ii); ClO(5,ii)+dpP; ClO(6,ii)] ,Nturn);
    end
    
    loss=0;
    if i>1
        while (loss==0&&j<20)
            j=j+1;
            dpP=0+(i-2)*0.01+j*0.001;
            [a,loss]=ringpass(ring2,[ClO(1:4,ii); ClO(5,ii)+dpP; ClO(6,ii)],Nturn);
        end
        deltap(ii) = 0+(i-2)*0.01+(j-1)*0.001;
    else
        while (loss==0&&j<10)
            j=j+1;
            dpP=0+(i-1)*0.01+j*0.001;
            [a,loss]=ringpass(ring2,[ClO(1:4,ii); ClO(5,ii)+dpP; ClO(6,ii)],Nturn);
        end
        deltap(ii) = 0+(i-1)*0.01+(j-1)*0.001;
    end
    
%     loss=0;
%     while (loss==0&&k<20)
%         k=k+1;
%         dpP=0+(i-2)*0.01+(j-2)*0.001+k*0.0001;
%         [a,loss]=ringpass(ring2,[ClO(1:4,ii); ClO(5,ii)+dpP; ClO(6,ii)],Nturn);
%     end
%     
%     deltap(ii) = 0+(i-2)*0.01+(j-2)*0.001+(k-1)*0.0001;
    
    loss=0;
    i=0;
    j=0;
    k=0;
    while (loss==0&&i<10)
        i=i+1;
        dpN=0-i*0.01;
        [a,loss]=ringpass(ring2,[ClO(1:4,ii); ClO(5,ii)+dpN; ClO(6,ii)],Nturn);
    end
    loss=0;
    if i>1
        while (loss==0&&j<20)
            j=j+1;
            dpN=0-(i-2)*0.01-j*0.001;
            [a,loss]=ringpass(ring2,[ClO(1:4,ii); ClO(5,ii)+dpN; ClO(6,ii)],Nturn);
        end
        deltan(ii) = 0-(i-2)*0.01-(j-1)*0.001;
    else
        while (loss==0&&j<10)
            j=j+1;
            dpN=0-(i-1)*0.01-j*0.001;
            [a,loss]=ringpass(ring2,[ClO(1:4,ii); ClO(5,ii)+dpN; ClO(6,ii)],Nturn);
        end
        deltan(ii) = 0-(i-1)*0.01-(j-1)*0.001;
    end
%      loss=0;
%     while (loss==0&&k<20)
%         k=k+1;
%         dpN=0-(i-2)*0.01-(j-2)*0.001-k*0.0001;
%         [a,loss]=ringpass(ring2,[ClO(1:4,ii); ClO(5,ii)+dpN; ClO(6,ii)],Nturn);
%     end
%     
%     deltan(ii) = 0-(i-2)*0.01-(j-2)*0.001-(k-1)*0.0001;
end

