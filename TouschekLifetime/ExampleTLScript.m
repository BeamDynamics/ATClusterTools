% Simple TL script
Nturns=200;          %3
%ringfile='/mntdirect/_users/carmigna/ESRF/LowEmit/LowEmit_S28AINJ/ATLattices/ErrorsAtTolCorrectedLatticeDA_seed3_.mat';        %1
%varringname='ring_err';     %2

ringfile='../S28Dmerged.mat';
varringname='LOW_EMIT_RING_INJ';     %2

a=load(ringfile,varringname);

ring=a.(varringname);

indID=findcells(ring,'FamName','ID\w*');
From=indID(4);            %4
To=indID(6);              %5 
RFVolt_MV=6;       %6
harm_num=992;        %7
emitx=132e-12;          %8
emity=10e-12;            %9
sigma_delta=[0.94e-3 0.94e-3 0.94e-3];
Ib=[0.2/868 0.092/16 0.04/4];             %11
Zn=0.67; %Ohm
BL=atBunchLength(ring,Ib,Zn);
Nproc=100;           %2
addlabel='test1';
commands.time=num2str(2*60);
collect=true;
%here i run the function RunTouLTArray
datafolder1= RunTouLTArray(...
    ringfile,...        1
    varringname,...     2
    Nturns,...          3
    From,...            4
    To,...              5
    emitx,...           6
    emity,...           7
    BL,...              8
    sigma_delta,...     9
    Ib,...              10
    Nproc,...           11
    addlabel,...        12
    commands,...        13
    collect);
    
% comment line below to test other modes of use of this functions
return

% or: 
[~,datafolder2]=getTLTCluster(ring,'test2','CurrentPerBunch',0.035,'Nturns',10);

% or:
tlt3=getTLTCluster(ring,'test3',230,235,'VerticalEmittance',10*1e-12,'Nturns',12);

% or:
[tlt4]=getTLTCluster(ring,'test4',From,To,commands,Nproc,'Nturns',14);

%%
file1 = fullfile(datafolder1,'ResultsLT.mat');
while ~exist(file1,'file'), disp(['wait for : ' file1]); pause(1); end
load(file1,'TL');
tlt1 = TL;

file2 = fullfile(datafolder2,'ResultsLT.mat');
while ~exist(file2,'file'), disp(['wait for : ' file2]); pause(1); end
load(file2,'TL');
tlt2 = TL;

tlt_recollect=CollectTouLTClusterData(datafolder2);

disp('auto-collect cluster results, results saved to file ./**test1**/ResultsLT.mat')
disp([num2str(tlt1/3600) ' hours'])
disp('getTLTCluster NOT waiting mode, results saved to file ./**test2**/ResultsLT.mat')
disp([num2str(tlt2/3600) ' hours'])
disp('waiting mode, input example 1. Results saved to file ./TLTData**test3**.mat and numeric output given')
disp([num2str(tlt3) ' hours'])
disp('waiting mode, input example 2. Results saved to file ./TLTData**test4**.mat and numeric output given')
disp([num2str(tlt4) ' hours'])
