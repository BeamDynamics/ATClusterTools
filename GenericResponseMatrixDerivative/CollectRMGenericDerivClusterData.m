function [RM]=CollectRMGenericDerivClusterData(datafolder,outputfilename) %#ok<*STOUT>
%
%
% 
%see also: RunRMGenericDerivArray
curdir=pwd;

cd(datafolder)

a=dir('specfile*.mat');
totproc=length(a);
b=dir('outfile*.mat');
finproc=length(b);

% wait for processes to finish
while finproc~=totproc
    b=dir('outfile*.mat');

    disp(['waiting for ' num2str(totproc-finproc) '/' num2str(totproc) ' processes to finish.'])
    
    % % % count runnning processes
    b=dir('outfile*.mat');
    finproc=length(b);

    % wait
    pause(10);
    
end


load(fullfile(datafolder,'GlobalTolParameters.mat'),'indBPM','indHCor','indVCor','LongPertArray','rmcalcfun');

dresp=[];%zeros(length(indQuadsDeriv),length(indBPM)*(length(indHCor)+length(indVCor)));
PA=[];

for iproc=1:totproc

    load(fullfile(datafolder,['outfile_' num2str(iproc,'%0.4d') '.mat']),'dRM','PertArray');
    
    dresp=[dresp dRM];
    
    PA=[PA PertArray];
end

PertArray=PA;

if nargin>1
    [pp,~,~]=fileparts(outputfilename);
    if ~isempty(pp)
        save(outputfilename,'PertArray','dresp','indBPM','indHCor','indVCor','rmcalcfun');
    else
        save(fullfile(datafolder, outputfilename),'PertArray','dresp','indBPM','indHCor','indVCor','rmcalcfun');
    end
end

RM=load(outputfilename);

% 
% delete OAR files
delete('*.stdout');
delete('*.stderr');
% delete spec and out files
delete('spec*.mat');
delete('out*.mat');



cd(curdir)

return