function [rmvec,respvector,respvectorskew]=...
    getfullrespmatrixvector(r,indBPM,indHCor,indVCor,delta,msg,kind)
%
% prepares response matrix vecotor for error fit
%
% kind may be: Full (ALL RM), Skew (Off Diag), Norm (On Diag). 
%
%see also:  findrespm_mod

kval=1e-4;
% dispersion
[l,t,ch]=atlinopt(r,0,indBPM);
%Ox=arrayfun(@(a)a.ClosedOrbit(1),l);
%Oy=arrayfun(@(a)a.ClosedOrbit(3),l);
oo=findorbit4Err(r,0,indBPM); % bpm errors
Ox=oo(1,:);
Oy=oo(3,:);

Dx=arrayfun(@(a)a.Dispersion(1),l);
Dy=arrayfun(@(a)a.Dispersion(3),l);
Bx=arrayfun(@(a)a.beta(1),l);
By=arrayfun(@(a)a.beta(2),l);

% bpm resolution
bpmresx=ones(size(indBPM))*1e-5./Bx; % better resolution for larger beam.
bpmresy=ones(size(indBPM))*1e-5./By;

if nargin<6
    msg='Computed Response matrix vector';
end
if nargin<7
    kind='full';
end

disp(msg);

ormH=findrespm(r,indBPM,indHCor,kval,'PolynomB',1,1,'findorbit4',delta);
ormV=findrespm(r,indBPM,indVCor,kval,'PolynomA',1,1,'findorbit4',delta);

% set BPM errors, from BPM fields. NOT DONE.

OH=ormH{1}./kval;%./repmat(bpmresx,length(indHCor),1)';
OV=ormV{3}./kval;%./repmat(bpmresy,length(indVCor),1)';
OHV=ormH{3}./kval;%./repmat(bpmresy,length(indHCor),1)';
OVH=ormV{1}./kval;%./repmat(bpmresx,length(indVCor),1)';


respvector=[ OH(:) ;... H orm
    OV(:) ;... V orm
    ]; % response in a column vector

respvectorskew=[ OHV(:) ;... H orm
    OVH(:) ;... V orm
    ]; % response in a column vector


rm=[OH, OVH;... H orm
    OHV, OV;... V orm
    ];

size(rm)
% select wich rm is the first output.
switch kind
    case {'full','FULL','Full'}
        rmvec=rm(:);
    case {'fulldisp','FULLDISP','FullDisp'}
        rmvec=[rm(:);10*Dx';100*Dy';t'];
    case {'fulldisporb','FULLDISPORB','FullDispOrb'}
        rmvec=[rm(:);10*Dx';100*Dy';1000*Ox';1000*Oy';t'];
    case {'fulldisporbchrom','FULLDISPORBCHROM','FullDispOrbChrom'}
        rmvec=[rm(:);10*Dx';100*Dy';1000*Ox';1000*Oy';1*t';0.1*ch'];
    case {'skew','SKEW','Skew'}
        rmvec=respvectorskew(:);
    case {'norm','NORM','Norm'}
        rmvec=respvector(:);
    case {'skewdisp','SKEWDISP','SkewDisp'}
        rmvec=[respvectorskew(:);10*Dy'];
    case {'normdisp','NORMDISP','NormDisp'}
        rmvec=[respvector(:);10*Dx';t'];
    case {'skewdisporb','SKEWDISPORB','SkewDispOrb'}
        rmvec=[respvectorskew(:);10*Dy';1000*Ox';1000*Oy'];
    case {'normdisporb','NORMDISPORB','NormDispOrb'}
        rmvec=[respvector(:);10*Dx';1000*Ox';1000*Oy';t'];
end

%rmvec(isnan(rmvec))=0;
%rmvec(isinf(rmvec))=0;

return

