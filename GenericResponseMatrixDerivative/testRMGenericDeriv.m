%% prepare lattice and compute RM derivative  

close all
clear all

a=load('S28Dmerged.mat');
ring=a.LOW_EMIT_RING_INJ;

unique(atgetfieldvalues(ring,atgetcells(ring,'PassMethod'),'PassMethod'))

indBPM=findcells(ring,'Class','Monitor');
indHCor=findcells(ring,'iscorH','H');
indVCor=findcells(ring,'iscorV','V');
indSCor=findcells(ring,'iscorS','S');
indQuadsDeriv=findcells(ring,'Class','Quadrupole');


% define perturbation indexes and values
ipa=1;


speclab='GradOrbErrAtQuadSext';

PA(ipa).index=indQuadsDeriv;
PA(ipa).perturbation={'PolynomB',[1,1]}; % corrector error
PA(ipa).dk=1e-5;
ipa=ipa+1;

PA(ipa).index=indQuadsDeriv;
PA(ipa).perturbation={'PolynomA',[1,1]}; % corrector error
PA(ipa).dk=1e-5;
ipa=ipa+1;

%  gradient errors
PA(ipa).index=indQuadsDeriv;
PA(ipa).perturbation={'PolynomB',[1,2]}; % quad gradient error
PA(ipa).dk=1e-5;
ipa=ipa+1;

PA(ipa).index=indSCor;
PA(ipa).perturbation={'PolynomB',[1,3]}; % sext gradient error
PA(ipa).dk=1e-5;
ipa=ipa+1;

%% bpm resolution
bpmresx=ones(size(indBPM)).*1e-8;%.*1e-4./Bx;%./Bx; % better resolution for larger beam.
bpmresy=ones(size(indBPM)).*1e-8;%.*1e-4./By;%./By;

%rmfunct=@(r,ib,ih,iv,~,txt)getrespmatrixvectorkind(r,ib,ih,iv,txt);
rmfunct=@(r,ib,ih,iv,~,txt)getfullrespmatrixvectorOFFONEN(r,ib,ih,iv,0.01,txt,'full',bpmresx,bpmresy);
speclab=[speclab 'OffEn'];
%rmfunct=@(r,ib,ih,iv,d,txt)getfullrespmatrixvectorkind(r,ib,ih,iv,d,txt,'full'); % default
%rmfunct=@(r,ib,ih,iv,d,txt)getfullrespmatrixvectorkind(r,ib,ih,iv,d,txt,'skew');
%rmfunct=@(r,ib,ih,iv,d,txt)getfullrespmatrixvectorkind(r,ib,ih,iv,d,txt,'full');

%% run OAR cluster RM derivative
slurmspec.time=num2str(60);
tic;
datafolder=RunRMGenericDerivArray(...
    'S28Dmerged.mat',...
    'LOW_EMIT_RING_INJ',...
    100,...
    PA,...
    indHCor(1:40:end),...
    indVCor(1:40:end),...
    indBPM(1:1:end),...
    rmfunct,...
    speclab,...
    slurmspec,...
    true);            % dpp

disp('Waiting for all files to be appropriately saved');
pause(10);

% CollectRMGenericDerivClusterData(datafolder,fullfile(datafolder,['../' speclab '.mat']));
%rmdir(datafolder);
toc;


return
