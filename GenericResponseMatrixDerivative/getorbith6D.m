function dh=getorbith6D(r,ib,inCOD)

d=findorbit6Err(r,ib,inCOD);

dh=d(1,:)';

end