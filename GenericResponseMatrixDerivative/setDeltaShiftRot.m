function re=setDeltaShiftRot(r,ind,dx,dy,dpsi)

[X0,Y0,T0]=GetExistingErrors(r,ind);

re=setshift_THERING(r,ind,X0+dx,Y0+dy);
re=settilt_THERING_Dipole(re,ind,T0+dpsi);

return