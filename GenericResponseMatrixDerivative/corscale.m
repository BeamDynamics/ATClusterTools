function rr=corscale(rr,ind,val,plane)
%CORSCALE scale correctors polynoms a or b
%
% for elements of index ind,
% apply a scale factor val to Polynom A (v) or B (h) 
%
%see also: 

for ii = 1:length(ind)
    switch plane
        case 'h'
            rr{ind(ii)}.PolynomB = rr{ind(ii)}.PolynomB * (1 + val);
        case 'v'
            rr{ind(ii)}.PolynomA = rr{ind(ii)}.PolynomA * (1 + val);
    end
end

return
        

end

