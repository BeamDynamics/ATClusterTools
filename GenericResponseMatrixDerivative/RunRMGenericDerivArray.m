function datafolder=RunRMGenericDerivArray(...
    ringfile,...        1
    varringname,...     2
    Nproc,...           3
    PerturbationArray,...4 (structure of parameters for wich to compute RM derivative)
    indHCor,...         5 (default all iscorH)
    indVCor,...         6 (default all iscorV)
    indBPM,...          7 (default all Class Monitor)
    rmcalcfun,...       8( rm calc function default getfullrespmatrixvector) 
    addspeclabel,...    9
    commands, ...
    collect)      %#ok<*INUSL> %10
% function RunRMGenericDerivArray(ringfile,varringname)
% 
% creates specification files to be used by RMGenericDerivClusterrunner
% and run the processes on the slurm cluster
% 
%     ringfile       : file where the AT lattice is stored
%     varringname    : name of AT lattice variable 
%
%     Nproc           : number of OAR processes to use for the computation   
%     PerturbationArray,...   6 (default see testRMGenericDeriv)
%     indHCor,...         7 (default all iscorH)
%     indVCor,...         8 (default all iscorV)
%     indBPM,...          9 (default all Class Monitor)
%     addspeclabel    : additional label for data folder
%     rmcalcfun,...       ( rm calc function default getfullrespmatrixvector) 
%      
%     commands        % optional, string of comands for slurm submision. 
%                     % default= commands.time=num2str(60)
%     collect         % first job also takes care to collect all results. (default = false)              
% 
% routine used should be compiled with
% compileRMGenericDeriv.m
% 
% in the computer where you wish to use this function.
%
%see also:

% this should be input.
routine='RMGenericDerivClusterrunner'; % compiled routine file name.
%pathtoroutine='/mntdirect/_users/liuzzo/Matlab_Work/ATWORK/atwork-code/physics/DA_OAR'; % compiled routine path.
[pathtoroutine,~,exte]=fileparts(which(routine));

if ~exist(fullfile(pathtoroutine,[routine exte]),'file')
    error([routine ' does not exist in: ' pathtoroutine...
        ' . If this is not the correct directory,'...
        ' you may need to add the directory that contains '...
        routine ' to the path.']);
end

%% LATTICE MANIPULATIONS
a=load(ringfile,varringname);
ring=a.(varringname);
[~,ringfile]=fileparts(ringfile);

% default auto-collect
if nargin<11
    collect=false;
end

% default OAR string
if nargin<10
    commands.time=num2str(60);
end

% default label
if nargin<9
    addspeclabel='';
end

% default label
if nargin<8
    rmcalcfun=@getfullrespmatrixvector;
end

% defaults RM computation
if nargin<5
    indBPM=findcells(ring,'Class','Monitor');
    indHCor=findcells(ring,'iscorH','H');
    indVCor=findcells(ring,'iscorV','V');
    indQuadsDeriv=findcells(ring,'Class','Quadrupole');
    PerturbationArray(1).index=indQuadsDeriv;
    PerturbationArray(1).perturbation={'PolynomB',[1,2]};
end

% make PertArray an array with all index fields of size 1, for easy
% spliting later

LongPertArray=arrayfun(@(a)StretchPertArray(a),PerturbationArray,'un',0);
LongPertArray=[LongPertArray{:}];

% create directory
commonwork=...
    [ringfile '_' addspeclabel '_rmderiv'];%
commonworkdir=fullfile(pwd,commonwork);%
mkdir(commonworkdir);

cd(commonworkdir);

datafolder=pwd;

% place here lattice file and RM file
save('latticefile.mat','ring');

%% define error maximum ranges

totprocnum=1;

workdir=pwd;
pfile=fopen('params.txt','w+');

% get subdivion of jobs into processes. 
[pointsperprocess]=DivideClusterJobs(length(LongPertArray),Nproc);

IQ=LongPertArray;
DK=1e-5;

save('GlobalTolParameters.mat',...
    'ringfile','varringname','Nproc',...
    'indBPM','LongPertArray','PerturbationArray','rmcalcfun',...
    'indHCor','indVCor','pointsperprocess','collect');

%% LOOP Dynamic Aperture points
for indproc=1:Nproc %
    
    % prepare SpecsFile
    
    % do not store lattice and RM in this file. they are always the same.
    ringFile=fullfile(workdir,'latticefile.mat');                       %#ok<*NASGU> % AT lattice
    ringFileVarName='ring';                       % AT lattice
    outfilename=fullfile(pwd,['outfile_' num2str(indproc,'%0.4d') '.mat']);
    
    if indproc<Nproc
        PertArray=IQ(sum(pointsperprocess(1:indproc-1))+1:sum(pointsperprocess(1:indproc)));
    else %last proces takes left overs
        PertArray=IQ(sum(pointsperprocess(1:indproc-1))+1:end);
    end
    
    spfn=['specfile_' num2str(indproc,'%0.4d') '.mat'];
    save(spfn,'ringFile','ringFileVarName','outfilename',...
        'indBPM','PertArray','indHCor','indVCor','rmcalcfun','collect',...
        'addspeclabel','datafolder','indproc');
    
    % prepare OAR script to run TolErrorSetEvaluatorOARrunner
    %script=[' /opt/matlab_2012a ' fullfile(pwd,spfn) '\n'];
    script=[' /sware/com/matlab_2020a ' fullfile(pwd,spfn) '\n'];
    %script=[' /opt/matlab_' version('-release') fullfile(pwd,spfn) '\n'];
    
    %fopen(fullfile(workdir,'params.txt'),'w+')
    fprintf(pfile,script');
    
    totprocnum=totprocnum+1;
    
end% END LOOP processes
totprocnum=totprocnum-1;% remove last update of totprocnum

routinetorun=[pathtoroutine '/run_' routine '.sh' ];
   
save('GlobalTolParameters.mat',...
    'ringfile','varringname','Nproc',...
    'indBPM','LongPertArray','PerturbationArray','rmcalcfun',...
    'indHCor','indVCor','pointsperprocess');
         
label=[ringfile addspeclabel '_P' num2str(Nproc) '_']; % label for files.

fclose('all');

% run on slurm
% commands.time=num2str(3*60); % time in minutes
commands.job_name=label;
submit_matlab_to_slurm(routinetorun(1:end-3),'params.txt',commands)

cd('..') % back to Parent of Working Directory

disp([num2str(totprocnum) ' processes with label: ' label ' are running on cluster']);

return


