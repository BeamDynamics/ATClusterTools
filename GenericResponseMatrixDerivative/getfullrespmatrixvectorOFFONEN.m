function [rmvec]=...
    getfullrespmatrixvectorOFFONEN(r,indBPM,indHCor,indVCor,delta,msg,kind,...
    bpmresx,bpmresy)
%
% prepares response matrix vecotor for error fit
%

brx=repmat(bpmresx,length(indHCor),1)';
bry=repmat(bpmresy,length(indVCor),1)';

[rmvec0]=getfullrespmatrixvector(...
    r,indBPM,indHCor,indVCor,0,msg,kind);

switch kind
    case 'norm'
rmvec0=rmvec0./[brx(:);bry(:)];
    case 'skew'
 rmvec0=rmvec0./[bry(:);brx(:)];
    case 'full'
  rmvec0=rmvec0./[brx(:);bry(:);brx(:);bry(:)];  
end

[rmvecd]=getfullrespmatrixvector(...
    r,indBPM,indHCor,indVCor,delta,msg,kind);

switch kind
    case 'norm'
rmvecd=rmvecd./[brx(:);bry(:)];
    case 'skew'
 rmvecd=rmvecd./[bry(:);brx(:)];
    case 'full'
  rmvecd=rmvecd./[brx(:);bry(:);brx(:);bry(:)];  
end

disprm=(rmvecd-rmvec0);%/delta; %rmvecd;%

scale=1;%6/200;%max(rmvec0(:))/max(disprm(:));%

rmvec=[rmvec0; disprm*scale];%

return

