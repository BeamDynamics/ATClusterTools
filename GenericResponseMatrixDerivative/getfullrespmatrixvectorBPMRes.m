function [rmvecd]=...
    getfullrespmatrixvectorBPMRes(r,indBPM,indHCor,indVCor,delta,msg,kind,...
    bpmresx,bpmresy)
%
% prepares response matrix vecotor for error fit
%

brx=repmat(bpmresx,length(indHCor),1)';
bry=repmat(bpmresy,length(indVCor),1)';


[rmvecd]=getfullrespmatrixvector(...
    r,indBPM,indHCor,indVCor,delta,msg,kind);

switch kind
    case 'norm'
rmvecd=rmvecd./[brx(:);bry(:)];
    case 'skew'
 rmvecd=rmvecd./[bry(:);brx(:)];
  case 'normdisp'
rmvecd=rmvecd./[brx(:);bry(:);bpmresx(:);1;1];
    case 'skewdisp'
 rmvecd=rmvecd./[bry(:);brx(:);bpmresy(:)];
    case 'full'
  rmvecd=rmvecd./[brx(:);bry(:);brx(:);bry(:)];  
end


return

