function [dx]=getDispersionH(THERING,bpmindx,bpmresx)
% function [dx]=getDispersionH(THERING,bpmindx,bpmresx)
% 
% determines dispersion taking two orbits at plus and minus DE=0.0001
% 


DE=0.001;

Op=findorbit4(THERING,DE,bpmindx);
Opx=Op(1,:);

Om=findorbit4(THERING,-DE,bpmindx);
Omx=Om(1,:);

dx=(Opx-Omx)./(2*DE)./bpmresx(:)';
dx=dx';
return