function [dresp,RM]=RMGenericDeriv(...
    r,...  lattice
    indBPM,... RM bpm indexes
    indHCor,... RM corrector indexes
    indVCor,...
    PertFunArray,... function for perturbation
    rmcalcfun...     function to compute RM (default (getfullrespmatrixvector))
    )
%function [dresp]=RMGenericDeriv(...
%     r,...  lattice
%     indBPM,... RM bpm indexes
%     indHCor,... RM corrector indexes
%     indVCor,...
%     PertFunArray,... function for perturbation
%     rmcalcfun...     function to compute RM (default (getfullrespmatrixvector))
%    )
%
% computed derivative of response matrix to quadrupole gradient changes.
%
% PertFunArray is an array of structures
% PertFunArray(1).index=ind1
% PertFunArray(1).perturbation={'PolynomB',[1,2]}
% PertFunArray(1).delta (default=0)
% PertFunArray(1).dk    (default=1e-5)
%
% perturbation may also be a function handle
% PertFunArray(1).perturbation=...
%       @(r,ind,dk)setcellstruct(r,'PolynomB',...
%                   PertFunArray(1).index(ind),K0(ind)+dk,1,2)
% PertFunArray(1).perturbation=@fun(r,ind,dk)
% PertFunArray(1).perturbation=@(~,~,~)disp('hello')
%
% 
% rmcalcfun(r,indBPM,indHCor,indVCor,0,'model orm');
%
%see also: getfullrespmatrixvector

if nargin<6
    rmcalcfun=@getfullrespmatrixvector; % for future changes to this function, or input
end

% defaults
deltaDEF=0;
DKDEF=1e-5;

    % model response matrix on energy
    resp0=rmcalcfun(r,indBPM,indHCor,indVCor,deltaDEF,'model orm');
% try
% catch exc
%     disp('Something wrong in rmcalcfun!')
%     getReport(exc)
%     
%     rmcalcfun=@getfullrespmatrixvector; % for future changes to this function, or input
%     
%     resp0=rmcalcfun(r,indBPM,indHCor,indVCor,deltaDEF,'model orm');
% end

l=arrayfun(@(a)length(a.index),PertFunArray);

dresp=[];%zeros(length(resp0),sum(l));

% loop perturbations
for ipert=1:length(PertFunArray)
    
    PerturbFun=PertFunArray(ipert);
    
    % switch pertrubFun may be a function or a cell array {fieldname value}
    if iscell(PerturbFun.perturbation)
        % model quadrupole gradients
        K0=getcellstruct(r,...
            PerturbFun.perturbation{1},...
            PerturbFun.index,...
            PerturbFun.perturbation{2}(1),...
            PerturbFun.perturbation{2}(2));
        
        PerturbationFunction=@(rr,ind,ddkk)setcellstruct(rr,...
            PerturbFun.perturbation{1},...
            ind,ddkk,...
            PerturbFun.perturbation{2}(1),...
            PerturbFun.perturbation{2}(2));
    else
        PerturbationFunction=PerturbFun.perturbation;
        K0=zeros(size(PerturbFun.index));
    end
    
    
    if ~isfield(PerturbFun,'dk')
        DK=DKDEF;
    else
        DK=PerturbFun.dk;
    end
    
    if ~isfield(PerturbFun,'delta')
        delta=deltaDEF;
    else
        delta=PerturbFun.delta;
    end
    
        
    NQ=length(PerturbFun.index);
    
    % initialize derivative
    drespPert=zeros(length(resp0),NQ);
    
    for iq=1:NQ
        
        % change i-th parameter
        
        rdk=PerturbationFunction(r,PerturbFun.index(iq),K0(iq)+DK);
        
        if iscell(PerturbFun.perturbation)
            namepert=[PerturbFun.perturbation{1} '(' num2str(PerturbFun.perturbation{2},'%d,')  ')'];
        else
            namepert=func2str(PerturbFun.perturbation);
        end
        % recompute RM
        dispstringcount=[namepert ': ' num2str(iq,'%4d') '/' num2str(NQ,'%4d')];
        
        % recompute resp0, if delta is different in each curve
        resp0=rmcalcfun(r,indBPM,indHCor,indVCor,delta,'model orm');
        % compute rm with perturbation
        respdk=rmcalcfun(rdk,indBPM,indHCor,indVCor,delta,dispstringcount);
        
        %fprintf(repmat('\b',1,length(dispstringcount)+1));
        
        % compute derivative
        drespPert(:,iq)=(respdk-resp0)./DK;
        
    end
    %PertFunRMDerivArray(ipert)=PerturbFun;
    %PertFunRMDerivArray(ipert).RMderiv=drespPert;
    
    dresp=[dresp,drespPert];

    
end

if nargout==2
    RM.dresp=dresp;
    RM.indHCor=indHCor;
    RM.indVCor=indVCor;
    RM.indBPM=indBPM;
    RM.PertArray=PertFunArray;
    RM.rmcalcfun=rmcalcfun;
end

return