function [dy]=getDispersionV(THERING,bpmindx,bpmresy)
% function  [dy]=getDispersionV(THERING,bpmindx,bpmresy)
% 
% determines dispersion taking two orbits at plus and minus DE=0.0001
% 


DE=0.001;

Op=findorbit4(THERING,DE,bpmindx);
Opy=Op(3,:);

Om=findorbit4(THERING,-DE,bpmindx);
Omy=Om(3,:);

dy=(Opy-Omy)./(2*DE);
dy=dy./bpmresy(:)';
dy=dy';

return