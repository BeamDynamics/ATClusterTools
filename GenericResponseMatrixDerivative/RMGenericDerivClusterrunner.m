function RMGenericDerivClusterrunner(inputdatafile)
%RMQuadDerivOARrunner( inputdatafile, outputdatafile)
%
%  this is the function evaluated by each single process on the OAR
%
%   inputdatafile contains variables: 
%           ringFile            AT lattice file
%           ringFileVarName     AT lattice variable name
%           indQuads            quadrupole indexes (loop over this variable)
%           indBPM              bpm indexes for RM
%           indHCor             horizontal correctors for RM
%           indVCor             vertical correctors for RM
%           DK                  quadrupole gradient variation
%
%   produces a file named outputdatafile that contains the variables:
%           dRM
%
%see also:  RMGenericDeriv

a=load(inputdatafile);

b=load(a.ringFile,a.ringFileVarName);
r=b.(a.ringFileVarName);

indBPM=a.indBPM;
indHCor=a.indHCor;
indVCor=a.indVCor;
PertArray=a.PertArray;
rmcalcfunction=a.rmcalcfun;

outputdatafile=a.outfilename;

dRM=RMGenericDeriv(r,indBPM,indHCor,indVCor,PertArray,rmcalcfunction); %#ok<*NASGU>

save(outputdatafile,'PertArray','dRM');
 
% delete('*.stdout');
% delete('*.stderr');
if a.collect && a.indproc==1
    CollectRMGenericDerivClusterData(a.datafolder,[a.addspeclabel '.mat']);
end
return
