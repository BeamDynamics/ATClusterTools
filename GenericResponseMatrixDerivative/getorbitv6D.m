function dh=getorbitv6D(r,ib,inCOD)

d=findorbit6Err(r,ib,inCOD);

dh=d(3,:)';

end