function re=setDeltaShiftRotDipole(r,ind,dx,dy,dpsi)
% moves reference frame!

[X0,Y0,T0]=GetExistingErrors(r,ind);

re=atsetshift(r,ind,X0+dx,Y0+dy);
re=atsettilt(re,ind,T0+dpsi);

return