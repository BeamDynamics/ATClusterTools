#!/bin/bash
ssh -X -t slurm-asd-access11 salloc --x11 --ntasks=1 --cpus-per-task=1 --mem-per-cpu=$1 --time=$2 srun --pty matlab
