function [drespnorm,drespskew,indQuads,...
    indBPM,indHCor,indVCor]=...
    CollectRMQuadDerivClusterData(datafolder,outputfilename) %#ok<*STOUT>
%
% [xc,yc,x,y,xa,ya,notok]=CollectDynApClusterData(datafolder,outputfilename)
% 
% datafolder: folder of data generated with RunDynApArray
% outputfilename: file where to save the data
%
% 
%see also: RunRMQuadDerivArray

cd(datafolder)

a=dir(fullfile(datafolder,'specfile*.mat'));
totproc=length(a);
b=dir(fullfile(datafolder,'outfile*.mat'));
finproc=length(b);

% wait for processes to finish
while finproc~=totproc
    b=dir(fullfile(datafolder,'outfile*.mat'));

    disp(['waiting for ' num2str(totproc-finproc) '/' num2str(totproc) ' processes to finish.'])
    
    % % % count runnning processes
    b=dir(fullfile(datafolder,'outfile*.mat'));
    finproc=length(b);

    % wait
    pause(10);
    
end


load(fullfile(datafolder,'GlobalTolParameters.mat'),'indBPM','indHCor','indVCor','indQuadsDeriv');

drespnorm=[];%zeros(length(indQuadsDeriv),length(indBPM)*(length(indHCor)+length(indVCor)));
drespskew=[];
indQ=[];

for iproc=1:totproc

    load(fullfile(datafolder,['outfile_' num2str(iproc,'%0.4d') '.mat']),'dRMk2n','dRMk2s','indQuads');
    
    drespnorm=[drespnorm dRMk2n];
    drespskew=[drespskew dRMk2s];
    
    indQ=[indQ indQuads];
end

indQuads=indQ;

if nargin>1
save(outputfilename,'indQuads','drespnorm','drespskew','indBPM','indHCor','indVCor');
end


% delete OAR files
delete('*.stdout');
delete('*.stderr');
% delete spec and out files
delete('spec*.mat');
delete('out*.mat');


cd ..

return