function dresp=RMderivSkewQuad(r,indBPM,indHCor,indVCor,indQuads,DK)
% computed derivative of response matrix to quadrupole gradient changes.
if nargin<6
DK=1e-5;
end


NQ=length(indQuads);

% model response matrix
[~,resp0]=getrespmatrixvector(r,indBPM,indHCor,indVCor,'model orm');

% model quadrupole gradients
K0=getcellstruct(r,'PolynomA',indQuads,1,2);
   
% initialize derivative
dresp=zeros(length(resp0),NQ);

for iq=1:NQ
    
    % change i-th quadrupole gradient
    rdk=setcellstruct(r,'PolynomA',indQuads(iq),K0(iq)+DK,1,2);
    
    % recompute RM
    dispstringcount=[num2str(iq,'%4d') '/' num2str(NQ,'%4d')];
    
    [~,respdk]=getrespmatrixvector(rdk,indBPM,indHCor,indVCor,dispstringcount);
    
    %fprintf(repmat('\b',1,length(dispstringcount)+1));
    
    % compute derivative
    dresp(:,iq)=(respdk-resp0)./DK;
    
end


return