function [respvector,respvectorskew,OH,OV,OHV,OVH,dispersion,tune]=...
    getrespmatrixvector(r,indBPM,indHCor,indVCor,msg)
%
% prepares response matrix vecotor for correction in qemerrfit
% 

kval=1e-4;

if nargin<5
    msg='Computed Response matrix vector';
end

ormH=findrespm_mod(r,indBPM,indHCor,kval,'PolynomB',1,1,'findorbit4Err');
ormV=findrespm_mod(r,indBPM,indVCor,kval,'PolynomA',1,1,'findorbit4Err');
%disp(msg)
[l,tune,~]=atlinopt(r,0,indBPM);
dispersionx=arrayfun(@(a)a.Dispersion(1),l);
dispersiony=arrayfun(@(a)a.Dispersion(2),l);

% set BPM errors, from BPM fields. NOT DONE.

OH=ormH{1}./kval;
OV=ormV{3}./kval;
OHV=ormH{3}./kval;
OVH=ormV{1}./kval;

respvector=[ OH(:) ;... H orm
             OV(:) ;... V orm
           dispersionx';...  % H dispersion
           tune']; % response in a column vector
         
respvectorskew=[ OHV(:) ;... H orm
                 OVH(:) ;... V orm
               dispersiony';...  % H dispersion
           ]; % response in a column vector

    
return