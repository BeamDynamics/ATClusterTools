function RMQuadDerivClusterrunner(inputdatafile)
%RMQuadDerivOARrunner( inputdatafile, outputdatafile)
%
%  this is the function evaluated by each single process on the OAR
%
%   inputdatafile contains variables: 
%           ringFile            AT lattice file
%           ringFileVarName     AT lattice variable name
%           indQuads            quadrupole indexes (loop over this variable)
%           indBPM              bpm indexes for RM
%           indHCor             horizontal correctors for RM
%           indVCor             vertical correctors for RM
%           DK                  quadrupole gradient variation
%
%   produces a file named outputdatafile that contains the variables:
%           dRMk2n:         rm derivative to indQuads normal quadrupole
%           dRMk2s:         rm derivative to indQuads skew   quadrupole
%
%see also:  RunRMQuadDerivArray RMderivNormQuad RMderivSkewQuad

a=load(inputdatafile);

b=load(a.ringFile,a.ringFileVarName);
r=b.(a.ringFileVarName);

indQuads=a.indQuads;
indBPM=a.indBPM;
indHCor=a.indHCor;
indVCor=a.indVCor;
DK=a.DK;

outputdatafile=a.outfilename;

dRMk2n=RMderivNormQuad(r,indBPM,indHCor,indVCor,indQuads,DK); %#ok<*NASGU>
dRMk2s=RMderivSkewQuad(r,indBPM,indHCor,indVCor,indQuads,DK);

save(outputdatafile,'indQuads','dRMk2n','dRMk2s');
 
% delete('*.stdout');
% delete('*.stderr');

return
