function datafolder=RunRMQuadDerivArray(...
    ringfile,...        1
    varringname,...     2
    Nproc,...           3
    indQuadsDeriv,...   4 (default all Class Quadrupole)
    indHCor,...         5 (default all iscorH)
    indVCor,...         6 (default all iscorV)
    indBPM,...          7 (default all Class Monitor)
    addspeclabel,...    8
    commands)      %#ok<*INUSL> %9
% function RunRMQuadDerivArray(ringfile,varringname)
% 
% created specification files to be used by RMQuadDerivOARrunner
% and run the processes on the ESRF OAR cluster
% 
%     ringfile       : file where the AT lattice is stored
%     varringname    : name of AT lattice variable 
%
%     Nproc           : number of OAR processes to use for the computation   
%     indQuadsDeriv,...   6 (default all Class Quadrupole)
%     indHCor,...         7 (default all iscorH)
%     indVCor,...         8 (default all iscorV)
%     indBPM,...          9 (default all Class Monitor)
%     addspeclabel    : additional label for data folder
%       
%     oarspecstring   % optional, string of comands for oar submision. 
%                     % default= '-l walltime=6 -q asd'
%                     % usefull:
%                     % -l walltime=72 (automtic que is nice not asd) 
%                     % -p "host like ''hpc2-01%%'' OR host like ''hpc2-02%%''"
%
%
%
% 
% 
% routine used should be compiled with
% mcc('-vm','routinename','passmethodlist.m') 
% in the computer where you wish to use this function.
% ex: mcc('-vm','RMQuadDerivOARrunner.m','passmethodlistfringes.m') 
% passmethodlistfinges.m contains the names of functions used by the
% routine but not explicitly called (as AT passmethods).
% 
% RunRMQuadDerivArray runs OAR jobs as ARRAYS! 
% oarsub --array 4 ./get_host_info
% cat params.txt
%   Here we put the input parameters of the 1st subjob
%   Here we put the input parameters of the 2nd subjob
%   Here we put the input parameters of the 3rd subjob
% oarsub --array-param-file ./params.txt ./get_host_info
%
%see also: RMQuadDerivOARrunner  

% this should be input.
routine='RMQuadDerivClusterrunner'; % compiled routine file name.
%pathtoroutine='/mntdirect/_users/liuzzo/Matlab_Work/ATWORK/atwork-code/physics/DA_OAR'; % compiled routine path.
[pathtoroutine,~,exte]=fileparts(which(routine));

if ~exist(fullfile(pathtoroutine,[routine exte]),'file')
    error([routine ' does not exist in: ' pathtoroutine...
        ' . If this is not the correct directory,'...
        ' you may need to add the directory that contains'...
        routine ' to the path.']);
end

%% LATTICE MANIPULATIONS
a=load(ringfile,varringname);
ring=a.(varringname);
[~,ringfile]=fileparts(ringfile);

% default label
if nargin<8
    addspeclabel='';
end

if nargin<13
    addlabel='';
end
if nargin<14 
    commands.time = num2str(12*60) ; % time in minutes
end
if nargin<15
    collect=false;
end

if ~isstruct(commands)
    warning('input ''commands'' must be a structure and it is not. Default values are used.');
    clear commands;
    commands.time = num2str(12*60) ; % time in minutes
end



% defaults RM computation
if nargin<5
    indBPM=findcells(ring,'Class','Monitor');
    indHCor=findcells(ring,'iscorH','H');
    indVCor=findcells(ring,'iscorV','V');
    indQuadsDeriv=findcells(ring,'Class','Quadrupole');
end

% create directory
commonwork=...
    [ringfile '_' addspeclabel '_rmderiv'];%
commonworkdir=fullfile(pwd,commonwork);%
mkdir(commonworkdir);

cd(commonworkdir);

datafolder=pwd;

% place here lattice file and RM file
save('latticefile.mat','ring');

%% define error maximum ranges

totprocnum=1;

workdir=pwd;
pfile=fopen('params.txt','w+');

% get subdivion of jobs into processes. 
[pointsperprocess]=DivideClusterJobs(length(indQuadsDeriv),Nproc);

IQ=indQuadsDeriv;
DK=1e-4;

save('GlobalTolParameters.mat',...
    'ringfile','varringname','Nproc',...
    'indBPM','indQuadsDeriv','indHCor','indVCor','pointsperprocess');

%% LOOP Dynamic Aperture points
for indproc=1:Nproc %
    
    % prepare SpecsFile
    
    % do not store lattice and RM in this file. they are always the same.
    ringFile=fullfile(workdir,'latticefile.mat');                       %#ok<*NASGU> % AT lattice
    ringFileVarName='ring';                       % AT lattice
    outfilename=fullfile(pwd,['outfile_' num2str(indproc,'%0.4d') '.mat']);
    
    if indproc<Nproc
        indQuads=IQ(sum(pointsperprocess(1:indproc-1))+1:sum(pointsperprocess(1:indproc)));
    else %last proces takes left overs
        indQuads=IQ(sum(pointsperprocess(1:indproc-1))+1:end);
    end
    
    spfn=['specfile_' num2str(indproc,'%0.4d') '.mat'];
    save(spfn,'ringFile','ringFileVarName','outfilename',...
        'indBPM','indQuads','indHCor','indVCor','DK');
    
    % prepare OAR script to run TolErrorSetEvaluatorOARrunner
    % % % script=[' /opt/matlab_2013a ' fullfile(pwd,spfn) '\n'];
    script=[' /sware/com/matlab_2020a ' fullfile(pwd,spfn) '\n'];
    
    %fopen(fullfile(workdir,'params.txt'),'w+')
    fprintf(pfile,script');
    
    totprocnum=totprocnum+1;
    
end% END LOOP processes
totprocnum=totprocnum-1;% remove last update of totprocnum

routinetorun=[pathtoroutine '/run_' routine '.sh' ];
   
save('GlobalTolParameters.mat',...
    'ringfile','varringname','Nproc',...
    'indBPM','indQuadsDeriv','indHCor','indVCor','pointsperprocess');
         
label=[ringfile addspeclabel '_P' num2str(Nproc) '_']; % label for files.
% run slurm
commands.job_name=label;
submit_matlab_to_slurm(routinetorun(1:end-3),'params.txt',commands)


cd('..') % back to Parent of Working Directory

disp([num2str(Nproc) ' processes with label: ' label ' are running on the cluster (' commands.time ' min)']);


return