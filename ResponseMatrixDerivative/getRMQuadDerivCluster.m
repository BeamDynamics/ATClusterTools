function [x,y,dd,xd]=getRMQuadDerivCluster(rcor,foldlabel,maxX,maxDPP,maxY,oarspec)
% 
% computed Dynamic aperture x-y  and dpp-x using OAR ASD cluster
% 
%see also:  RunDynApArray CollectDynApOARData
if nargin<3
    maxX=0.017;
    maxDPP=0.05;
    maxY=0.004;
end
if nargin<6
    oarspec='-l walltime=6 -q asd';
end

latfilename=[foldlabel '.mat'];
save(latfilename,'rcor');

tic;
datafolderdpp=RunRMQuadDerivArray(...
    latfilename,...'latfile.mat',...
    'rcor',...
    512,...         % number of turns
    [60 60],...       % steps in dimension(1) and dimension(2)
    100,...          % number of processes on OAR
    [-maxDPP maxDPP],...% dimension(1) limits
    [0 maxX],...    % dimension(2) limits
    'grid',...      % grid in cartesian coordinates
    [5 1],...       % dimensions to scan
    0.0,...            % dpp
    oarspec);

[dd,xd,a_d,b_d,c_d,d_d]=CollectRMQuadDerivClusterData(datafolderdpp);
toc;
save(['DAdata' latfilename],'dd','xd','a_d','b_d','c_d','d_d');
%rmdir(datafolder);
%rmdir(datafolderdpp);

