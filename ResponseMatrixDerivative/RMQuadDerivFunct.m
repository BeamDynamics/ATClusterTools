function RMQuadDerivFunct(ringfilename,varringname,inputfilename,outputfilename)
% this function computes an RM 
% inputfile must contain r=lattice AT
% the response matrix is saved in outputfilename as 
% a matlab variable named RMHH RMHV RMVH and RMVV
% bpmxcorrectors
% RMHH RMHV
% RMVH RMVV
%

% load lattice
a=load(ringfilename,varringname);
r=a.(varringname);

% load simulation specific parameters
b=load(inputfilename,'indquads','DK');
indquads=b.indquads;
DK=b.DK;

for iq=1:length(indquads)
% set quadrupole change
K0=getcellstruct(r,'PolynomB',indquads(iq),1,2);
r=setcellstruct(r,'PolynomB',indquads(iq),K0+DK,1,2);

% get indexes
indBPM=findcells(r,'Class','Monitor');
indHCor=findcells(r,'iscorH','H');
indVCor=findcells(r,'iscorV','V');
kval=1e-5;


r=setcellstruct(r,'PolynomB',indquads(iq),K0,1,2);

end

% save output
save(outputfilename,'RMHH','RMHV','RMVH','RMVV','indquads','DK');

return
