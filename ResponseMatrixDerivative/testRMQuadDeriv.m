% test RunDynApArray.m

% generates various Gb of DATA!
% tic;
% datafolder=RunRMQuadDerivArray(...
%     pwd,...
%     'S28AINJ_fortol.mat',...
%     'LOW_EMIT_RING_INJ',...
%     100);            % dpp
% CollectRMQuadDerivOARData(datafolder,fullfile(datafolder,'../testquadrm.mat'));
% rmdir(datafolder);
% toc;

load ../S28Dmerged.mat
ring=LOW_EMIT_RING_INJ;
indBPM=findcells(ring,'Class','Monitor');
indHCor=findcells(ring,'iscorH','H');
indVCor=findcells(ring,'iscorV','V');
indQuadsDeriv=findcells(ring,'Class','Quadrupole');

tic;
datafolder=RunRMQuadDerivArray(...
    'S28F_PA.mat',...
    'LOW_EMIT_RING_INJ',...
    100,...
    indQuadsDeriv(1:50:end),...
    indHCor(1:20:end),...
    indVCor(1:20:end),...
    indBPM(1:20:end),...
    'fewmag');            % dpp
CollectRMQuadDerivClusterData(datafolder,fullfile(datafolder,'../testquadrmsmall.mat'));
%rmdir(datafolder);
toc;








