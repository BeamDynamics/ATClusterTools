function NLatticesErrorsCorrectedEBSClusterrunner(inputdatafile)
%NLatticesErrorsCorrectedClusterEBSrunner( inputdatafile, outputdatafile)
%
%  this is the function evaluated by each single process on the OAR
%
%   inputdatafile contains variables: 
%           ringFile            AT lattice file
%           ringFileVarName     AT lattice variable name
%           cororder,             number of turns to track
%           neig,               [1-6, 1-6] dimensions to scan
%           outputdatafile      file name for output
%           errampl             matrix of errors rms for SetEBSErrorList
%
%   produces a file named outputdatafile that contains the variables:
%           rcor          1XN final dimen(1) coordinate, NaN if lost
% 
%see also: CorrectionChain SetEBSErrorList

a=load(inputdatafile);

b=load(a.ringFile); % all variables in ringFile and in b structure
ring=b.(a.ringFileVarName);

outputdatafile=a.outfilename;

%% set errors

% set multipole errors, random errors, ALGE errors

%Nsig=2.5;
%rerrold=SetEBSErrorList(ring,a.seed,Nsig,1,[2:7,8,9,11],a.errampl);
       
[rerr,errtab]=SetEBSErrorTable(ring,a.seed,...
    'error_amplitudes',a.errampl,...
    'verbose',true,'survey_errors',a.surveyerrors,...
    'use_multipoles',a.use_multipoles);
      
b.cororder(b.cororder==1) = 100002; % use tikhonov regularization in orbit correction
b.cororder(b.cororder==0) = 10000 ; % use tikhonov regularization in trajectory correction
b.neigenvectors(1)=0.5;
b.neigenvectors(2)=0.5;

[...
    rcor,...            % corrected lattice
    chor,...              % final H cor values
    cver,...              % final V cor values
    cqua,...              % final Quad cor values
    cskw,...              % final Skew Quad cor values
    csxt,...              % final Quad cor values
    coct,...              % final Skew Quad cor values
    inCOD,...
    d0,de,dc...         % lattice data structures d0=no err, de=err, dc=cor
    ]=correctionchainebs(...
    rerr,...          1) lattice to be corrected
    ring,...             2) lattice without error
    b.indBPM,...          3) monitor indexes in r0 and rerr
    b.indHCor,...         4) h steerers indexed in r0 and rerr
    b.indVCor,...         5) v steerers indexed in r0 and rerr
    b.indSCor,...         6) skew quad correctors indexed in r0 and rerr
    b.indQCor,...         7) norm quad correctors indexed in r0 and rerr
    b.indSext,...         8) norm quad correctors indexed in r0 and rerr
    b.indOctu,...          9) norm quad correctors indexed in r0 and rerr
    b.neigenvectors,...   10) eigenvectors for correction and fit
    b.cororder,...        11) correction parameters, order, eigenvectors #
    b.ModelRM,...         12) response matrice structure, if [], rm are computed
    '',...['s' num2str(a.seed)],...
    false); %#ok<*ASGLU>


% create correctors table
FamNames = atgetfieldvalues(rerr,1:length(ring),'FamName');
DeviceNames = atgetfieldvalues(rerr,1:length(ring),'Device','Default','no/device/name');
Lengths = atgetfieldvalues(rerr,1:length(ring),'Length');
KL0n = zeros(size(ring)); % hor steerer
KL0s = KL0n; % ver steerer
KL1n = KL0n; % normal quadruple
KL1s = KL0n; % skew quadrupole
KL2n = KL0n; % sextupole
KL3n = KL0n; % octupole

% values for lattice without errors
cq0 = atgetfieldvalues(ring,b.indQCor,'PolynomB',{1,2});
cs0 = atgetfieldvalues(ring,b.indSext,'PolynomB',{1,3});
co0 = atgetfieldvalues(ring,b.indOctu,'PolynomB',{1,4});

cqe = atgetfieldvalues(rerr,b.indQCor,'PolynomB',{1,2});
cse = atgetfieldvalues(rerr,b.indSext,'PolynomB',{1,3});
coe = atgetfieldvalues(rerr,b.indOctu,'PolynomB',{1,4});

KL0n(b.indHCor) = chor.*Lengths(b.indHCor);
KL0s(b.indVCor) = cver.*Lengths(b.indVCor);
KL1n(b.indQCor) = (cqua-cqe).*Lengths(b.indQCor);
KL1s(b.indSCor) = cskw.*Lengths(b.indSCor);
KL2n(b.indSext) = (csxt-cse).*Lengths(b.indSext);
KL3n(b.indOctu) = (coct-coe).*Lengths(b.indOctu);

for ii = 1:length(DeviceNames)
    if isempty(DeviceNames{ii})
        DeviceNames{ii} = '-';
    end
end

FamNames = categorical(FamNames);
DevNames = categorical(DeviceNames);
cortab = table(...
    DevNames,...
    FamNames,...
    Lengths,...
    KL0n,...
    KL0s,...
    KL1n,...
    KL1s,...
    KL2n,...
    KL3n);

% add device names to errors table
errtab= [table(DevNames) errtab];


save(outputdatafile,'ring','rerr','rcor','d0','de','dc','chor','cver','cqua','cskw','csxt','coct','errtab','cortab');
S=load(outputdatafile);
P=S;
% P.(outputdatafile)=S.rcor; % for simulator, ring structure named as file name
save(outputdatafile, '-struct', 'P')

% delete('*.out');
% delete('*.err');

return
