function datafolder=RunNLatticesErrorCorrectionEBSArray(...
    ringfile,...        1
    varringname,...     2
    Nseeds,...          3
    cororder,...        4
    neigenvectors,...   5
    errampl,...         6 
    commands,...   7
    surveyerrors,...    8
    use_multipoles)      %9
% function RunNLatticesErrorCorrectionArrayEBS(ringfile,varringname)
%
% created specification files to be used by DynApOARrunner
% and run the processes on the ESRF OAR cluster
%
%     ringfile       : file where the AT lattice is stored
%     varringname    : name of AT lattice variable
%
%     commands        % optional, structure of comands for cluster submision.
%                       default commands.time='60'
%     surveyerrors: (false) boolean, set measured survey errors
%     use_multipoles: (true) boolean, set multipole errors
%
%see also: submit_matlab_to_slurm

% this should be input.
routine='NLatticesErrorsCorrectedEBSClusterrunner'; % compiled routine file name.
%pathtoroutine='/mntdirect/_users/liuzzo/Matlab_Work/ATWORK/atwork-code/physics/DA_OAR'; % compiled routine path.
[pathtoroutine,~,exte]=fileparts(which(routine));

if ~exist(fullfile(pathtoroutine,[routine exte]),'file')
    error([routine ' does not exist in: ' pathtoroutine...
        ' . If this is not the correct directory,'...
        ' you may need to add the directory that contains'...
        routine ' to the path.']);
end
    
%% LATTICE MANIPULATIONS
a=load(ringfile,varringname);
ring=a.(varringname);

% reduce ring
% keep=atgetcells(ring,'Class','RingParam|RFCavity') | ...
%     atgetcells(ring,'FamName','BPM\w*|BPM\w*-\w*|^ID.*|JL1[AE].*');
% ring=atreduce(ring,keep);

[~,ringfile]=fileparts(ringfile);

commonwork=...
    [ringfile '_ns' num2str(Nseeds) ''];%
commonworkdir=fullfile(pwd,commonwork);%
mkdir(commonworkdir);

cd(commonworkdir);

datafolder=pwd;

indHCor = findcells(ring,'FamName','S[FDHIJ]\w*');
indVCor = indHCor;
indSCor = indHCor;

indQCor = findcells(ring,'Class','Quadrupole');
indSext = findcells(ring,'FamName','S[FDIJ]\w*');
indOctu = findcells(ring,'FamName','O[FDIJ]\w*');
indBPM  = findcells(ring,'Class','Monitor');

ring=PadPolynomAB(ring);

% initialize BPM errors
zz=zeros(size(indBPM))';
ring=atsetbpmerr(ring,indBPM,zz,zz,zz,zz,zz,zz,zz);

% get RM model

inCOD=[0 0 0 0 0 0]';

if not(exist(fullfile(pwd,'NoErrLatticeMatricesCorParam.mat'),'file'))
ModelRM...
    =getresponsematrices(...
    ring,...
    indBPM,...
    indHCor,...
    indVCor,...
    indSCor,...
    indQCor,...
    [],...
    inCOD,...
    1:12); % all RM
else
    load('NoErrLatticeMatricesCorParam.mat','ModelRM')
end

% prepare and save matrices for fit of lattice errors model 
% (uses Cluster!)
FitResponseMatrixAndDispersion(...
    ring,... % nothing to fit. just computes RMs before hands for all seeds
    ring,...
    inCOD,...
    indBPM,...
    indHCor(1:9*2:end),... % 4 correctors, 1 every 8 cells
    indHCor(1:9*2:end),...  % 4 correctors, 1 every 8 cells
    [1e6,1e6,1e6,1e6],...
    4,...
    ['fitrm']);

if nargin<4
cororder=[0 2 1 2 3 6 1 2 3 6 1 2 -1];
end

if nargin<5
neigenvectors=[...
    100,... % n eig orbit H
    100,... % n eig orbit V
    150,... % skew quadrupole vertical dispersion correction
    250,... % quadrupole horizontal dispersion correction
    200,... % quad fit
    100,... % dip fit
    200,... % skew fit
    ]; % number of eigenvectors orbit, quadrdt, disp v disp h skewRDT
end

if nargin<6

errampl=[]; % use default in SetEBSErrorList.m

end

if nargin<7

commands.time=num2str(60);

end

if nargin<8
    surveyerrors = false;
end

if nargin<9
    use_multipoles = true;
end
    

disp(errampl)

% save here lattice file and RM file once and for all
save('NoErrLatticeMatricesCorParam.mat',...
    'ring',...
    'indBPM',...
    'indHCor','indVCor',...
    'indSCor','indQCor',...
    'indSext','indOctu',...
    'ModelRM',...
    'cororder','errampl','neigenvectors','surveyerrors','use_multipoles');

%% define error maximum ranges

totprocnum=1;

workdir=pwd;
pfile=fopen('params.txt','w+');

%% LOOP seeds
for indproc=1:Nseeds %
   
    % 1 seed per process
    seed=indproc;
   
    % do not store lattice and RM in this file. they are always the same.
    ringFile=fullfile(workdir,'NoErrLatticeMatricesCorParam.mat');                       %#ok<*NASGU> % AT lattice
    ringFileVarName='ring';                       % AT lattice
    outfilename=fullfile(pwd,['Errors_' num2str(indproc,'%0.4d') '.mat']);
        
    spfn=['specfile_' num2str(indproc,'%0.4d') '.mat'];
    save(spfn,'ringFile','ringFileVarName',...
        'outfilename','seed','errampl','surveyerrors','use_multipoles');
    
    % prepare OAR script to run TolErrorSetEvaluatorOARrunner
    %script=[' /opt/matlab_2013a ' fullfile(pwd,spfn) '\n'];
    script=[' /sware/com/matlab_2020a ' fullfile(pwd,spfn) '\n'];
    
    %fopen(fullfile(workdir,'params.txt'),'w+')
    fprintf(pfile,script');
    
    totprocnum=totprocnum+1;
    
end% END LOOP processes
totprocnum=totprocnum-1;% remove last update of totprocnum

routinetorun=[pathtoroutine '/run_' routine '.sh' ];

% hpc
label=[ringfile '_Seed' num2str(Nseeds,'%.4d_') '_']; % label for files.


fclose('all');


% run on slurm
% commands.time=num2str(3*60); % time in minutes
commands.job_name=label;
submit_matlab_to_slurm(routinetorun(1:end-3),'params.txt',commands)


disp([num2str(totprocnum) ' processes with label: ' label ' are running on cluster ']);

% wait for jobs to finish
a=dir('specfile*.mat');
totproc=length(a);
b=dir('outfile*.mat');
finproc=length(b);

% wait for processes to finish
while finproc~=totproc
    b=dir('Errors_*.mat');

    disp(['waiting for ' num2str(totproc-finproc) '/' num2str(totproc) ' processes to finish.'])
    
    % % % count runnning processes
    b=dir('Errors_*.mat');
    finproc=length(b);

    % wait
    pause(10);
    
end


cd('..') % back to Parent of Working Directory


return