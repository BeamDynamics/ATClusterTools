function [....
    BS,AS,BR,AR]=GetMultipoleErrorsMagDesign_S28F(...
    ring,...                %lattice without multipoles errors
    systematicerrorflag,... %set systematic errors
    randomerrorflag,...      %set random errors
    scalerandmulterr,... % scale random multipole errors
    dipquaderrors)       % set also dipole and quadrupole multipole errors
% function ringmultipoles=SetMultipoleErrorsMagDesign_S28BINJ(ring)
% assigns S28BINJ multipole errors
% [....
%     ringmultipoles,.... % ring with multipole errors
%     bnsyst,....         % normal systematic multipole components applied
%     bnrand,....         % normal random multipole components applied
%     anrand....          % skew   random multipole components applied
%     ]=SetMultipoleErrorsMagDesign_S28BINJ(...
%     ring,...                %lattice without multipoles errors
%     systematicerrorflag,... %set systematic errors
%     randomerrorflag...      %set random errors
%     scalerandmulterr) scale random multipole errors 
%           20/30 for bulk
%           40/30 for laminated
%
% update: 20/02/2015 (missing DL (rand), OF, DQ1, OF rand and syst)
% update: July/2015 (updated multipole values)
% update: September/2015 (updated multipole values, added DQ1 DQ2 random assumed DQ1=DQ2 and anr=bnr)
%
%
%see also: AssignFieldErr TruncatedGaussian

if nargin < 5
dipquaderrors=0;
end

if nargin==1
    disp('Setting All multipoles')
    systematicerrorflag=1;
    randomerrorflag=1;
end
warning('Injection magnets as cell magnets!');

magfamsnames={...
    'QF1',...
    'QD2',...
    'DL1',...
    'DL2',...
    'QD3',...
    'SD1',...
    'QF4',...
    'QF4E',...
    'SF ',...
    'OF ',...
    'QD5',...
    'QF6',...
    'DQ1',...
    'QF8',...
    'DQ2'};

magfamsrefradii=[...
    13,...'QF1'
    13,...'QD2'
    13,...'DL1_1'
    13,...'DL1_2_5'
    13,...'QD3'
    13,...'SD1'
    13,...'QF4'
    13,...'QF4E'
    13,...'SF '
    13,...'OF '
    13,...'QD5'
    7,...'QF6'
    7,...'DQ1'
    7,...'QF8'
    7,...'DQ2'...
    ]*1e-3;

magindexes={... % magnet indexes, in order of apperance in the lattice cell
    find(atgetcells(ring,'FamName','QF1\w*'))',...% [A-E] is to exclude the Injection magnets
    find(atgetcells(ring,'FamName','QD2\w*'))',...
    find(atgetcells(ring,'FamName','DL1\w*'))',...
    find(atgetcells(ring,'FamName','DL2\w*'))',...
    find(atgetcells(ring,'FamName','QD3\w*'))',...
    find(atgetcells(ring,'FamName','S[DIJ]1\w*'))',...
    find(atgetcells(ring,'FamName','QF4[ABD]'))',...
    find(atgetcells(ring,'FamName','QF4[E]'))',...
    find(atgetcells(ring,'FamName','S[FIJ]2\w*'))',...
    find(atgetcells(ring,'FamName','O[FIJ]\w*'))',...
    find(atgetcells(ring,'FamName','QD5\w*'))',...
    find(atgetcells(ring,'FamName','QF6\w*'))',...
    find(atgetcells(ring,'FamName','DQ1\w*'))',...
    find(atgetcells(ring,'FamName','QF8\w*'))',...
    find(atgetcells(ring,'FamName','DQ2\w*'))',...
    };

onoff=ones(1,length(magindexes));% all on by default

    
%% assign measured field errors.
%%%%% SYSTEMATIC = bn_ave, an_ave
%%%%% RANDOM = bn_std, an_ave

%% QF1, G.LeBec _stats.cvs, summaryMultipoleMeasurements.xlsx

% qf1
qf1an=[  
    0
10000
-1.2312
-0.2811
0.1366
-0.0604
0.0318
-0.0166
0.1185
];

qf1an_rand=[  
    0
10000
4.4829
1.2129
1.1323
0.2970
0.4277
0.3885
0.2149
];

qf1bn=[  
    0
10000
-0.2084
-2.6577
0.1800
-2.7184
0.1199
-0.2199
5.1538e-04
];

qf1bn_rand=[  
    0
10000
2.8655
3.9136
1.0416
3.0417
0.4670
0.5328
0.2367
];


%% QD2,QF4[ABD],QD5, G.LeBec _stats.cvs, summaryMultipoleMeasurements.xlsx
% Measurements


% qd2 qf4 qd5 
qd2qf4qd5an=[
   0
10000
-1.5062
0.1195
0.4677
-0.0066
-0.0364
-0.0872
0.0185
 ];

qd2qf4qd5bn=[
   0
10000
-0.2541
-3.6003
-0.0299
-0.7417
0.0505
0.0922
-0.0296
 ];

qd2qf4qd5an_rand=[
   0
10000
2.8870
1.0478
0.9643
0.3527
0.5339
0.3469
0.2985
 ];

qd2qf4qd5bn_rand=[
   0
10000
 2.5173
2.8459
0.8738
2.6214
0.5095
0.5882
0.3292
];




%% QD3,  G.LeBec _stats.cvs, summaryMultipoleMeasurements.xlsx

% qd3
qd3an=[
    0
10000
-3.0330
-0.1759
0.4800
-0.0368
-0.0024
-0.0748
0.1268
];

qd3bn=[
    0
10000
-1.1921
-3.7760
-0.0914
-1.9224
0.1393
-0.1442
0.0295
];

qd3an_rand=[
    0
10000
2.9150
1.1036
1.0263
0.3011
0.3998
0.3894
0.3098
];

qd3bn_rand=[
    0
10000
4.3107
15.4956
0.9005
2.7233
0.5379
0.6314
0.2730
];


%% QF4E, G.LeBec Quad_High_Order_Multipoles
% 	Measurements			
% r0 [mm]	13	13	13	13
% n	bn ave	bn stdev	an ave	an stdev
% 3	-0.52155465	2.4564576	-0.9896062	1.8944761
% 4	-2.1763661	2.6237473	0.00259523	0.7613849
% 5	0.13767241	0.83787155	0.16736498	0.68001324
% 6	-2.7544646	1.9444612	0.034741208	0.22111492
% 7	0.075009316	0.3665702	-0.055655919	0.43171975
% 8	-0.5247637	0.34867534	-0.00381771	0.24860463
% 9	-0.045636058	0.21675244	0.13433068	0.19395535

% qf4e
qf4ean=[
    0
10000
-0.9001
0.0868
0.3247
-3.7084e-04
-0.1134
-0.0475
0.0721
];

qf4ebn=[
    0
10000
-0.8592
-1.4290
0.0336
-2.5252
0.0661
-0.4637
-0.0400
];

qf4ean_rand=[
    0
10000
2.3950
0.7626
0.7496
0.2369
0.4387
0.2489
0.2303
];

qf4ebn_rand=[
    0
10000
2.5422
3.0379
0.8013
1.8984
0.3833
0.3908
0.2042
];

%% QF6, G.LeBec _stats.xlsx 


% qf6

qf6an=[
    0
10000
-0.8901
-0.0531
0.0620
-0.1054
-0.0483
-0.0292
0.0130
];

qf6bn=[
    0
10000
-0.9851
0.8884
0.0476
-0.5640
0.0402
-0.0027
-0.0047
];

qf6an_rand=[
    0
10000
2.7432
0.5573
0.3773
0.2101
0.0907
0.0345
0.0356
];

qf6bn_rand=[
    0
10000
1.6395
1.1970
0.4433
0.1849
0.1291
0.0749
0.0420
];



%% QF8, G.LeBec _stats.xlsx

%  qf8 
qf8an=[
0
10000    
0.1132
-0.0451
0.1633
-0.1382
-0.0257
-0.0397
0.0059
];

qf8bn=[
0
10000    
-0.7349
0.0431
-0.0184
0.5448
-0.0111
0.0204
0.0170
];

qf8an_rand=[
0
10000    
1.4653
0.3589
0.2495
0.3453
0.0686
0.0367
0.0283
];

qf8bn_rand=[
0
10000    
1.4618
0.9723
0.3830
0.1589
0.1101
0.0563
0.0417
];

%% BELOW NON UPDATED! no measurements data at the moment!

dq1bn=[%%%% relative to DIPOLE!
    10000
4499.22*0%% considered elesewhere
1.3194
-3.52302
-1.37691
9.23649
1.06358
-10.1486
-3.14353
2.6593
2.20996
-0.0810461
-1.00213
-0.534322
0.0956301
0.230463
0.0973052
-0.028557
-0.0408101
-0.0100374
0.0117989
0.016608
-0.0180537
-0.0091815
-0.0033418
0.00478292
0.017508
0.0104226
0.0563036
0.0103595
0.140688
0.0239935
];

dq2bn=[ % alfresco 09/07/2015
10000
5532.12*dipquaderrors*0%% considered elesewhere
-0.638178
25.8143
5.20062
-17.2604
-5.60715
-3.13825
0.700903
0.594722
-0.0168115
-0.0909287
-0.099857
0.126364
0.102901
0.0660892
];

dq1an = [10000 0 0];
dq2an = [10000 0 0];

% harm DLs, J.Chavanne, 18/june/2018

dl1bn=[
 10000
 0
0.6722
-7.3023
0.8656
-3.3357
0.7235
26.4293
-1.4064
-25.5397
0.8930
7.9890
-0.2766
1.5154
-0.1002
-3.6485
0.2620

 ];

 dl1an=[
 10000
 0.0731
0.4507
0.0753
-0.3047
-0.0996
0.4609
0.2865
-0.4209
-0.1026
0.3122
-0.1491
-0.3335
0.2453
0.5244
-0.2127
-0.8069
];

dl2bn=[
 10000
 0
0.6722
-7.3023
0.8656
-3.3357
0.7235
26.4293
-1.4064
-25.5397
0.8930
7.9890
-0.2766
1.5154
-0.1002
-3.6485
0.2620
 ];

 dl2an=[
 10000
 0.1512
0.1910
-0.1259
0.0241
0.0455
0.1374
0.3373
-0.1816
-0.2922
0.1740
0.0935
-0.3036
0.0547
0.6599
-0.1382
-1.1808
];


%% to be updated with measurement
sd1an=[
   -0.2925 *0
    0
    10000
    -0.2925
-0.5646
-0.2313
0.7270
-0.0273
-0.0273
];


sd1an_rand=[
    0.7959 *0
0
10000
0.7959
0.5591
1.0901
0.4101
1.8916
1.8916
];

sd1bn=[
   -0.1940*0
0
10000
-0.1940
0.9221
1.1426
39.4523
1.7067
1.7067
];


sd1bn_rand=[
   0.6621*0
0
10000
0.6621
0.7194
5.0932
0.5020
1.5215
1.5215
];

sf2an=[
    0.0795*0
0
10000
0.0795
-0.2387
0.0343
0.4891
0.5193
0.5193
];


sf2an_rand=[
   0.7367*0
0
10000
0.7367
0.6580
1.2735
0.3907
1.7891
1.7891
];

sf2bn=[
   0.0138*0
0
10000
0.0138
1.6374
3.7981
40.5464
2.2317
2.2317
];


sf2bn_rand=[
   0.7546*0
0
10000
0.7546
1.1582
6.4781
0.4190
2.4512
2.4512
];


%%
ofbn=[
    0
    0
    0
    10000
    0
    0
    0
    0
    0
    0];

ofan = ofbn;


%% build cellarray of multipole components

bnsyst={...% multipole field errors, in order of apperance in the lattice cell
    [...QF1
    qf1bn]*1e-4*onoff(1),...QF1
    [...QD2
    qd2qf4qd5bn]*1e-4*onoff(2),...QD2
    [...DL1
    dl1bn]*1e-4*onoff(3),...DL1
    [...DL2
    dl2bn]*1e-4*onoff(4),...DL2
    [...QD3
    qd3bn]*1e-4*onoff(5),...QD3
    [...SD1
    sd1bn]*1e-4*onoff(6),...SD1
    [...QF4
    qd2qf4qd5bn]*1e-4*onoff(7),...QF4
    [...QF4E
    qf4ebn]*1e-4*onoff(7),...QF4
    [...SF2
    sf2bn]*1e-4*onoff(8),...SF2
    [...OF1
    ofbn]*1e-4*onoff(9),...OF1
    [...QD5
    qd2qf4qd5bn]*1e-4*onoff(10),...QD5
    [...QF6
    qf6bn]*1e-4*onoff(11),...QF6
    [...DQ1 
    dq1bn]*1e-4*onoff(12),...DQ1 
    [...QF8
    qf8bn]*1e-4*onoff(13),...QF8
    [...DQ2
    dq2bn]*1e-4*onoff(14),...DQ2 
    };

ansyst={...% multipole field errors, in order of apperance in the lattice cell
    [...QF1
    qf1an]*1e-4*onoff(1),...QF1
    [...QD2
    qd2qf4qd5an]*1e-4*onoff(2),...QD2
    [...DL1
    dl1an]*1e-4*onoff(3),...DL1
    [...DL2
    dl2an]*1e-4*onoff(4),...DL2
    [...QD3
    qd3an]*1e-4*onoff(5),...QD3
    [...SD1
    sd1an]*1e-4*onoff(6),...SD1
    [...QF4
    qd2qf4qd5an]*1e-4*onoff(7),...QF4
    [...QF4E
    qf4ean]*1e-4*onoff(7),...QF4
    [...SF2
    sf2an]*1e-4*onoff(8),...SF2
    [...OF1
    ofan]*1e-4*onoff(9),...OF1
    [...QD5
    qd2qf4qd5an]*1e-4*onoff(10),...QD5
    [...QF6
    qf6an]*1e-4*onoff(11),...QF6
    [...DQ1 
    dq1an]*1e-4*onoff(12),...DQ1 
    [...QF8
    qf8an]*1e-4*onoff(13),...QF8
    [...DQ2
    dq2an]*1e-4*onoff(14),...DQ2 
    };


%% RANDOM
% 30 um

dq1bnr=[
    0
    10000
    9.360035
    6.231414
    4.040027
    2.390795
    1.598305];

dq2bnr=[
    0
    10000
    9.360035
    6.231414
    4.040027
    2.390795
    1.598305];

dlbnr=[
    10000
    0
    0
    0
    0
    0];

sextbnr=[
    0.0022*dipquaderrors
0.0017*dipquaderrors
0
7.2330e-04
3.9599e-04
1.4986e-04
4.2453e-05
1.2127e-05
2.7490e-05
3.5432e-05
3.1562e-05
2.0812e-05
1.5015e-05
8.5440e-06
3.9749e-06
2.8271e-06
1.3636e-06
4.1152e-07
]*1e4*30/50*scalerandmulterr;% computed for 50um

sextbnr(3)=1e4;

octbnr=[
    0
    0
    0
    10000
    0
    0
    0
    0
    0];

% skew

dq1anr=[
    0
    0
    9.360035
    6.231414
    4.040027
    2.390795
    1.598305];

dq2anr=[
    0
    0
    9.360035
    6.231414
    4.040027
    2.390795
    1.598305];

%% J.Chavanne 18/June/2018 harmon_DLs
dl1bn_rand=[
 10000
 0
1.2101
1.0076
1.0683
1.3921
0.7663
1.4625
1.2031
0.8168
0.7564
0.3567
0.3812
0.4376
0.3984
0.5204
0.6789
];

dl2bn_rand=[
 10000
 0
0.9409
0.5037
0.5323
0.9538
0.8242
1.4082
1.7205
0.9368
1.1520
0.5180
0.6891
0.5687
0.5455
0.7477
0.5623];

dl1an_rand=[
 10000
 2.3432
0.7576
0.5386
0.6066
0.4808
0.5518
0.6457
0.4217
0.5081
0.2885
0.3318
0.4877
0.3116
0.9398
0.3957
0.4563];

dl2an_rand=[
 10000
 1.4977
0.6931
0.3839
0.6194
0.4211
0.6095
0.5538
0.5901
0.5324
0.3640
0.5057
0.4011
0.5657
0.8504
0.7424
0.5789];



%% to be updated
sextanr=[
    0.0020*dipquaderrors
0.0018*dipquaderrors
0.0013 
7.4844e-04
3.2321e-04
1.6012e-04
3.8491e-05
1.2814e-05
3.4294e-05
3.6640e-05
2.5829e-05
2.2166e-05
1.3582e-05
9.0634e-06
5.5419e-06
2.9239e-06
1.1148e-06
4.3986e-07
]*1e4*30/50*scalerandmulterr;% computed for 50um

octanr=[
    0
    0
    0
    0
    0
    0
    0
    0
    0];


bnrand={...% multipole field errors, in order of apperance in the lattice cell
    [...QF1
    qf1bn_rand]*1e-4*onoff(1),...QF1
    [...QD2
    qd2qf4qd5bn_rand]*1e-4*onoff(2),...QD2
    [...DL1
    dl1bn_rand]*1e-4*onoff(3),...DL1
    [...DL1_2-5
    dl2bn_rand]*1e-4*onoff(4),...DL1
    [...QD3
    qd3bn_rand]*1e-4*onoff(5),...QD3
    [...SD1
    sd1bn_rand]*1e-4*onoff(6),...SD1
    [...QF4
    qd2qf4qd5bn_rand]*1e-4*onoff(7),...QF4
    [...QF4E
    qf4ebn_rand]*1e-4*onoff(7),...QF4E
    [...SF2
    sf2bn_rand]*1e-4*onoff(8),...SF2
    [...OF1
    octbnr]*1e-4*onoff(9),...OF1
    [...QD5
    qd2qf4qd5bn_rand]*1e-4*onoff(10),...QD5
    [...QF6
    qf6bn_rand]*1e-4*onoff(11),...QF6
    [...DQ1
    dq1bnr]*1e-4*onoff(12),...DQ1 
    [...QF8
    qf8bn_rand]*1e-4*onoff(13),...QF8
    [...DQ2
    dq2bnr]*1e-4*onoff(14),...DQ2 
    };


% skew random multipoles
anrand={...% multipole field errors, in order of apperance in the lattice cell
    [...QF1
    qf1an_rand]*1e-4*onoff(1),...QF1
    [...QD2
    qd2qf4qd5an_rand]*1e-4*onoff(2),...QD2
    [...DL1
    dl1an_rand]*1e-4*onoff(3),...DL1
    [...DL2
    dl2an_rand]*1e-4*onoff(4),...DL2
    [...QD3
    qd3an_rand]*1e-4*onoff(5),...QD3
    [...SD1
    sd1an_rand]*1e-4*onoff(6),...SD1
    [...QF4
    qd2qf4qd5an_rand]*1e-4*onoff(7),...QF4
    [...QF4E
    qf4ean_rand]*1e-4*onoff(7),...QF4E
    [...SF2
    sf2an_rand]*1e-4*onoff(8),...SF2
    [...OF1
    octanr]*1e-4*onoff(9),...OF1
    [...QD5
    qd2qf4qd5an_rand]*1e-4*onoff(10),...QD5
    [...QF6
    qf6an_rand]*1e-4*onoff(11),...QF6
    [...DQ1
    dq1anr]*1e-4*onoff(12),...DQ1 
    [...QF8
    qf8an_rand]*1e-4*onoff(13),...QF8
    [...DQ2
    dq2anr]*1e-4*onoff(14),...DQ2 
     };
 
%% define all systematic errors 

BS = zeros(length(ring),20);
AS=BS;
BR=BS;
AR=BS;

if systematicerrorflag
    
    for imag=1:length(magindexes)
        
        if ~isempty(bnsyst{imag}) %&& find(bnsyst{imag})
            % find reference multipole, set it to zero
            bs=bnsyst{imag};
            refMult=find(bs==1);
          
            if isempty(refMult)
                maxord=ring{magindexes{imag}(1)}.MaxOrder+1;
                %ring{magindexes{imag}(1)}
                [~,maxcoef]=max(abs(ring{magindexes{imag}(1)}.PolynomB));
                refMult=min(maxord,maxcoef);
            end
            
            bs(refMult)=0;
            if onoff(imag)
                disp(['Systematic: ' magfamsnames{imag}...
                    ', refmultipole: ' num2str(refMult)...
                    ', std(berr): ' num2str(std(bs))])
                
                % assign field errors
                rho=getcellstruct(ring,'RefRadius',magindexes{imag});
                rho(isnan(rho))=0.013; % default for magnet without refRadius =0.013
                %             find(isnan(rho),1,'first')
                %             unique(rho(~isnan(rho)))
                
                [pb,pa]=ComputeFieldErr(ring,magindexes{imag},refMult,rho,bs');%,0*bs
          
                L=min([length(pb(1,:)),length(pa(1,:)),20]);
          
                BS(magindexes{imag},1:L)=pb(:,1:L);
                AS(magindexes{imag},1:L)=pa(:,1:L);
                 
               if find(isnan(pb))
                
                 error(['NaN in PolynomB for ' magfamsnames{imag}])
                end
            end
            
         else
            warning(['Missing Systematic multipole data for: ' magfamsnames{imag} ])
         end
    end

end

%% assign all random errors 
if randomerrorflag
 %error('STILL DO NOT KNOW WHAT [arbitrary units] Means in the P folder data!')
    for imag=1:length(magindexes)
        
        if ~isempty(bnrand{imag}) || ~isempty(anrand{imag})
            % find reference multipole, set it to zero
            bs=bnrand{imag};
            refMult=find(bs==1);
            %bsN=bnsyst{imag};
            %refMult=find(bsN==1);
            if isempty(refMult)
                maxord=ring{magindexes{imag}(1)}.MaxOrder+1;
                [~,maxcoef]=max(abs(ring{magindexes{imag}(1)}.PolynomB));
                refMult=min(maxord,maxcoef);
            end
            
            bs(refMult)=0;
            as=anrand{imag};
            as(refMult)=0;
            
            if onoff(imag)
                disp(['Random: ' magfamsnames{imag} ', refmultipole: ' num2str(refMult)])
                indh=magindexes{imag};
                rho=getcellstruct(ring,'RefRadius',magindexes{imag});
                rho(isnan(rho))=0.013; % default for magnet without refRadius =0.013
                Nsig=2;
                
                if find(bs) % only if some exist.
                    ipolb=find(bs);
                    for indpolb=1:length(ipolb)%find(bs); % loop berr to get random errors.
                        bsr(ipolb(indpolb),:)=TruncatedGaussian(bs(ipolb(indpolb)),abs(Nsig*bs(ipolb(indpolb))),[1 length(indh)]);
                    end
                    
                    if find(as) % only if some exist.
                        ipolb=find(as);
                        for indpolb=1:length(ipolb)%ipolb=find(as); % loop berr to get random errors.
                            asr(ipolb(indpolb),:)=TruncatedGaussian(as(ipolb(indpolb)),abs(Nsig*as(ipolb(indpolb))),[1 length(indh)]);
                         end
                    else
                        asr=zeros(size(bsr));
                    end
                    
                    for jpolb=1:length(magindexes{imag})  % assign random multipoles to each magnet
                        [pb,pa]=ComputeFieldErr(ring,indh(jpolb),refMult,rho(jpolb),bsr(:,jpolb)',asr(:,jpolb)');
                        
                        L=min([length(pb(1,:)),length(pa(1,:)),20]);
                        
                        BR(indh(jpolb),1:L)=pb(:,1:L);
                        AR(indh(jpolb),1:L)=pa(:,1:L);
                    end
                end
                clear bsr
                clear asr
            end
        else
            warning(['Missing Random multipole data for: ' magfamsnames{imag} ])
        end
        
    end

end

ringmultipoles=ring;


function [Pnew,Panew]=ComputeFieldErr(r,refpos,N,rho,BNn,ANn)
% function [Pnew,Panew]=ComputeFieldErr(r,refpos,N,rho,BNn,ANn)
% 
% r : at lattice
% refpos : index of elements for wich the field error has to be applied
% N main component (2=quadrupole,3=sextupole)
% rho: reference radius
%
% the field errors are defined by the magnet designer with the multipole
% expansion
% B=B_N(rho0)*sum_(n=1)^(infty)((BNn+i*ANn)*(z/rho0)^(n-1))
%
% and by AT as
% B=Brho*sum_(n=1)^(infty)((b_n+i*a_n)*(z)^(n-1))
% 
% the input BNn and ANn are the normal and skew field errors at rho0. 
%    as defined by:
% B^(N)_(n) = radius^(n-N)*b_n/b_N
%
% b_n=radius^(N-n)*b_N*B^(N)_(n)
%
% optional output ,Pnew,Panew the PolynomB and PolynomA set in AT.
% 


if nargin<6
    ANn=BNn*0;
end
P=getcellstruct(r,'PolynomB',refpos,1,N);
Pa=getcellstruct(r,'PolynomA',refpos,1,N);

if N==1 && P(1)==0
    % is a dipole.
    P=getcellstruct(r,'BendingAngle',refpos);% ./getcellstruct(r,'Length',refpos);
end

if length(rho)==1
    Pnew=repmat(rho.^(N-[1:length(BNn)]),length(P),1).*repmat(P,1,length(BNn)).*repmat(BNn,length(P),1);
    Panew=repmat(rho.^(N-[1:length(ANn)]),length(Pa),1).*repmat(P,1,length(ANn)).*repmat(ANn,length(Pa),1); % refer to normal component also skew
elseif length(rho)==size(P,1)
    RNB=[];
    RNA=[];
    
    for irrh=1:size(P,1)
        RNB(irrh,:)=rho(irrh).^(N-[1:length(BNn)]);
        RNA(irrh,:)=rho(irrh).^(N-[1:length(ANn)]);
    end
     
    Pnew=RNB.*repmat(P,1,length(BNn)).*repmat(BNn,length(P),1);
    Panew=RNA.*repmat(P,1,length(ANn)).*repmat(ANn,length(Pa),1);
end



return