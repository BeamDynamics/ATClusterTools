function rerr=GenericErrorList(r0,seed,Nsig,errampl)
%
% function to set a given error list.
%
% 
% errampl=[...
%     [100e-6, 100e-6, 1e-4];... Dip HV, rot, field
%     [50e-6, 50e-6, 5e-4];... Quad HV, rot, field
%     [50e-6, 50e-6, 35e-4];... Sext HV, rot, field
%     [100e-6,    0,      0];... BPM HV
%     ];
%
%see also: atsetrandomerrors

if seed~=0
    disp(['Setting Random Stream to seed: ' num2str(seed)]);
    % set seed
    s = RandStream('mcg16807','Seed',seed);
    RandStream.setGlobalStream(s);
else
    disp('Using previously set random stream')
end

rerr=r0;

%% define random errors structure
ie=1;
errstruct=[];

%% DIPOLES

% % DL
indqm=findcells(r0,'Class','Bend');
errstruct(ie).indx=indqm;
errstruct(ie).type='x';
errstruct(ie).sigma=errampl(1,1);
ie=ie+1;
errstruct(ie).indx=indqm;
errstruct(ie).type='y';
errstruct(ie).sigma=errampl(1,1);
ie=ie+1;
errstruct(ie).indx=indqm;
errstruct(ie).type='psi';
errstruct(ie).sigma=errampl(1,2);
ie=ie+1;
errstruct(ie).indx=indqm;
errstruct(ie).type='dpb1';
errstruct(ie).sigma=errampl(1,3);
ie=ie+1;

% % DQ
indqm=find(atgetcells(r0,'Class','Bend') & ...
    atgetcells(r0,'PolynomB',@(a,f)a.PolynomB(2)~=0))';

errstruct(ie).indx=indqm;
errstruct(ie).type='x';
errstruct(ie).sigma=errampl(2,1);
ie=ie+1;
errstruct(ie).indx=indqm;
errstruct(ie).type='y';
errstruct(ie).sigma=errampl(2,1);
ie=ie+1;
errstruct(ie).indx=indqm;
errstruct(ie).type='psi';
errstruct(ie).sigma=errampl(2,2);
ie=ie+1;
errstruct(ie).indx=indqm;
errstruct(ie).type='dpb1';
errstruct(ie).sigma=errampl(2,3);
ie=ie+1;


%% QUADRUPOLES

%high gradeint quadrupoles
indqm=[findcells(r0,'Class','Quadrupole')];
errstruct(ie).indx=indqm;
errstruct(ie).type='x';
errstruct(ie).sigma=errampl(3,1);
ie=ie+1;
errstruct(ie).indx=indqm;
errstruct(ie).type='y';
errstruct(ie).sigma=errampl(3,1);%70*1e-6;
ie=ie+1;
errstruct(ie).indx=indqm;
errstruct(ie).type='psi';
errstruct(ie).sigma=errampl(3,2);%200*1e-6;
ie=ie+1;
errstruct(ie).indx=indqm;
errstruct(ie).type='dpb2';
errstruct(ie).sigma=errampl(3,3);
ie=ie+1;


%% SEXTUPOLES

inds=findcells(r0,'Class','Sextupole');
errstruct(ie).indx=inds;
errstruct(ie).type='x';
errstruct(ie).sigma=errampl(4,1);%70*1e-6;
ie=ie+1;
errstruct(ie).indx=inds;
errstruct(ie).type='y';
errstruct(ie).sigma=errampl(4,1);
ie=ie+1;
errstruct(ie).indx=inds;
errstruct(ie).type='psi';
errstruct(ie).sigma=errampl(4,2);%500*1e-6;
ie=ie+1;
errstruct(ie).indx=inds;
errstruct(ie).type='dpb3';
errstruct(ie).sigma=errampl(4,3);
ie=ie+1;

indm=findcells(r0,'Class','Monitor');
errstruct(ie).indx=indm;
errstruct(ie).type='bpm';
errstruct(ie).sigma=errampl(5,1);



if ~isempty(errstruct)
    
    %% set errors
    magindex=arrayfun(@(a)a.indx,errstruct,'un',0);
    type=arrayfun(@(a)a.type,errstruct,'un',0);
    sigma=arrayfun(@(a)a.sigma,errstruct,'un',0);
    
    rerr=atsetrandomerrors(...
        rerr,...
        magindex,...
        findcells(r0,'Class','Monitor'),...
        seed,...
        sigma,...
        Nsig,...
        type);
    
end


return