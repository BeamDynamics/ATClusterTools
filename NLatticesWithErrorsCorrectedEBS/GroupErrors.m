function [errvalgrouped,mag_groups]=GroupErrors(errval,mag_groups)
% function [valgrouped,mag_groups]=GroupErrors(val,mag_groups)
%
% makes values in errval idntical within an element of mag_groups {{1,3,4},{213,235,236},...}
% identical to the ones on the FIRST element of the group. 
% 
% Groups are defined in the lattice by the EBS specific function getMagGroups
%
% valgrouped=GroupsErrors(val,mag_groups)
% valgrouped=GroupsErrors(val,ring)
% 
%see also: getMagGroups

for i=1:length(mag_groups)
    
    % group indexes
    ind=mag_groups{i};
    if length(ind)>1
        %get first element in group
        grouperr = errval(ind(1));
        % set all other slices to this value
        errval(ind)=grouperr;
    end
    
end

errvalgrouped = errval;

end


