% test N lattices with errors

warning('This code requires functions not officially part of AT, but in an AT code branch named errors_correction. ')
warning('This code is based on lattice naming used for ESRF-EBS')

% list of correction to apply. see correctionchain.m for help
cororder=[0 2 1 2 3 7 1 2 3 7 1 2 -1];

neigenvectors=[...
    200,... % n eig orbit H
    200,... % n eig orbit V
    150,... % skew quadrupole vertical dispersion correction
    250,... % quadrupole horizontal dispersion correction
    200,... % quad fit
    100,... % dip fit
    200,... % skew fit
    ];

% define errors to be assigned
errorset = 0.01 * [...H, V, s-rot, field, pitch, yaw
    [ 70e-6,  70e-6, 100e-6,  10e-4, 0, 0];... Dip
    [ 70e-6,  70e-6, 100e-6,   5e-4, 0, 0];... DQ
    [ 70e-6,  70e-6, 100e-6,   5e-4, 0, 0];... Quad
    [ 70e-6,  70e-6, 100e-6,   5e-4, 0, 0];... Quad HG
    [ 70e-6,  70e-6, 100e-6,  35e-4, 0, 0];... Sext
    [ 70e-6,  70e-6, 100e-6,  50e-4, 0, 0];... Oct
    [ 50e-6,  50e-6, 100e-6,      0, 0, 0];... BPM (offset, rotation, gain)
    [200e-6, 200e-6, 100e-6,      0, 0, 0];... SH
    ]; %

commands.partition='nice-long';
commands.time=num2str(72*60);

a = load('S28Dmerged.mat');
ring = a.LOW_EMIT_RING_INJ;

% mark elements to be used for optics fit
ring = atsetfieldvalues(ring,atgetcells(ring,'Class','Bend','Quadrupole','Sextupole'),'FitElement',1);

save('LatForErrCor.mat','ring');

RunNLatticesErrorCorrectionEBSArray(...
    'LatForErrCor.mat',...        1
    'ring',...     2
    40,...           5
    cororder,...
    neigenvectors,...
    errorset,... % [] to use default
    commands);

