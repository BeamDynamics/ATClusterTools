function [rerr,ErrTab]=SetEBSErrorTable(r0,seed,varargin)
%[rerr,ErrTab]=SetEBSErrorTable(r0,seed,varargin)
%
% function to set a given error list.
%
% INPUT:
% 'r0', AT LATTICE
% 'seed',@isnumeric
%
% OPTIONAL INPUT, default, conditions:
% 'scale_errors',1.0,@(X)(X>0 && X<=1)
% 'error_amplitudes', deferr, [8x6] rms error values
% 'use_multipoles', boolean, default (true)
% 'N_sigma', 2.5
% 'verbose', false, boolean
% 'survey_errors', false, boolean, use measured survey errors 
% 
% deferr=[...H, V, rot, field
%     [100e-6, 100e-6, 200e-6,  10e-4, 0, 0];... Dip
%     [ 60e-6,  60e-6, 200e-6,   5e-4, 0, 0];... DQ
%     [100e-6,  85e-6, 200e-6,   5e-4, 0, 0];... Quad
%     [ 60e-6,  60e-6, 200e-6,   5e-4, 0, 0];... Quad HG
%     [ 70e-6,  50e-6, 200e-6,  35e-4, 0, 0];... Sext
%     [100e-6, 100e-6, 200e-6,  50e-4, 0, 0];... Oct
%     [ 50e-6,  50e-6,   0,      0, 0, 0];... BPM
%     [200e-6, 200e-6, 200e-6,   0, 0, 0];... SH
%     ]; %
% for magnets the columns correspond to: Dx, Dy, Rot, DK/K, pitch, yaw
% for BPM the columns correspond to: offset X, Y, Rot, Reading, Gain x, Gain y
%
% future developments: 
% replace error matrix, by error table: SearchString,MainOrder,Dx,Dy,Rot,DK/K,Pitch,Yaw,... 
% 
%see also:

%% parse input and set defualts

deferr=[...H, V, rot, field
    [ 70e-6,  70e-6, 100e-6,  10e-4, 0, 0];... Dip
    [ 70e-6,  70e-6, 100e-6,   5e-4, 0, 0];... DQ
    [ 70e-6,  70e-6, 100e-6,   5e-4, 0, 0];... Quad
    [ 70e-6,  70e-6, 100e-6,   5e-4, 0, 0];... Quad HG
    [ 70e-6,  70e-6, 100e-6,  35e-4, 0, 0];... Sext
    [ 70e-6,  70e-6, 100e-6,  50e-4, 0, 0];... Oct
    [ 50e-6,  50e-6, 100e-6,     0, 0, 0];... BPM (offset, rotation, gain)
    [200e-6, 200e-6, 100e-6,      0, 0, 0];... SH
    ]; %

% families
fams = {...
    {'DL\w*','JL\w*','Canting\w*','SMB\w*','SBM\w*','W1\w*'},... % Drifts and markers misaligned, but no effect
    {'DQ\w*','mlDQ\w*'},...
    {'Q[FDIJ][1-5]\w*'},...
    {'Q[FDIJ][6-8]\w*'},...
    {'S[FDIJ]\w*'},...
    {'O[JF]\w*'},...
    {'BPM\w*-\w*','BPM\w*'},... % '-' not in wild card!
    {'SH\w*'},...
    {'ID\w*','DR_\w*'}...
    };

magord=[1,2,2,2,3,4,0,0,0];

p = inputParser;
addRequired(p,'r0',@iscell);
addRequired(p,'seed',@(x)(isnumeric(x) && x<50));
addOptional(p,'scale_errors',1.0,@(X)(X>=0 && X<=1));
addOptional(p,'error_amplitudes',deferr,@isnumeric);
addOptional(p,'N_sigma',2.5,@(X)(X>0));
addOptional(p,'verbose',false,@islogical);
addOptional(p,'survey_errors',false,@islogical);
addOptional(p,'use_multipoles',false,@islogical);

parse(p,r0,seed,varargin{:});

factorerr = p.Results.scale_errors;
errampl = p.Results.error_amplitudes;
Nsig = p.Results.N_sigma;
verb = p.Results.verbose;
alge = p.Results.survey_errors;
mult = p.Results.use_multipoles;

errampl = errampl * factorerr;

errampl(errampl==0)=1e-12;


if seed~=0
    disp(['Setting Random Stream to seed: ' num2str(seed)]);
    % set seed
    s = RandStream('mcg16807','Seed',seed);
    RandStream.setGlobalStream(s);
else
    disp('Using previously set random stream')
end

rerr=r0;

%% define random errors table

% function to group magnet errors according to MagNum field in magnets.
% get groups
mag_groups=getMagGroups(r0);
mag_groups_multipoles=getMagGroupsMultipoles(r0);

% create table of errors
ErrTab = atcreateerrortable(r0);

% MULTIPOLES
if mult
    %[BS,AS,BR,AR]=GetMultipoleErrorsMagDesign_S28E(r0,1,1,40/30); % laminated
    [BS,AS,BR,AR]=GetMultipoleErrorsMagDesign_S28F(r0,1,1,factorerr); % measured for quadrupole and dipoles
else % no multipoles.
    [BS,AS,BR,AR]=GetMultipoleErrorsMagDesign_S28F(r0,1,1,1e-12); % measured for quadrupole and dipoles
    BS = BS*0+1e-12;
    AS = AS*0+1e-12;
    BR = BR*0+1e-12;
    AR = AR*0+1e-12;
end


for ib=1:size(BS,2)
    BS(:,ib)=GroupErrors(BS(:,ib),mag_groups_multipoles);
    AS(:,ib)=GroupErrors(AS(:,ib),mag_groups_multipoles);
    BR(:,ib)=GroupErrors(BR(:,ib),mag_groups_multipoles);
    AR(:,ib)=GroupErrors(AR(:,ib),mag_groups_multipoles);
end

ErrTab.b_n_systematic=BS;
ErrTab.a_n_systematic=AS;
ErrTab.b_n_random=BR;
ErrTab.a_n_random=AR;


% random numbers with given sigma truncated gaussian,
% grouped for sliced magnets
randomdistrib=@(sigma)GroupErrors(...
    TruncatedGaussian(sigma,Nsig*sigma,size(r0)),...
    mag_groups);

% survey ALGE
algedir='/machfs/liuzzo/EBS/';
algeactfile=fullfile(algedir,'Actual_Position_Simu.xlsx');
%algeactfile=fullfile(algedir,'Nominal_Position_Simu.xlsx');
[dxalge,dyalge]=SurveyErrors(r0,algeactfile,'',seed);
if ~alge
    disp('Not using measured survey errors')
    dxalge = dxalge*0;
    dyalge = dyalge*0;
end

for magind=1:length(fams)
    
    % get indexes
    ind=findcells(r0,'FamName',fams{magind}{:});
    
    if ~strcmp(r0{ind(1)}.Class,'Monitor') && ...
            ~strcmp(r0{ind(1)}.Class,'Marker')&& ...
            ~strcmp(r0{ind(1)}.Class,'Drift')
        
        % define errors
        Dx   = GroupErrors(randomdistrib(errampl(magind,1)) + dxalge(:,seed),mag_groups);
        Dy   = GroupErrors(randomdistrib(errampl(magind,2)) + dyalge(:,seed),mag_groups);
        Dpsi = randomdistrib(errampl(magind,3));
        Da   = randomdistrib(errampl(magind,4));
        Dyaw = randomdistrib(errampl(magind,5));
        Dpit = randomdistrib(errampl(magind,6));
        
        % chop very small values
        Dx(abs(Dx)<1e-10)=0.0;
        Dy(abs(Dy)<1e-10)=0.0;
        Dpsi(abs(Dpsi)<1e-10)=0.0;
        Da(abs(Da)<1e-10)=0.0;
        Dyaw(abs(Dyaw)<1e-10)=0.0;
        Dpit(abs(Dpit)<1e-10)=0.0;
        if verb
            disp([fams{magind}{:}])
            disp(['Dx: ' num2str(std(Dx))]);
            disp(['Dy: ' num2str(std(Dy))]);
            disp(['Dpsi: ' num2str(std(Dpsi))]);
            disp(['Da: ' num2str(std(Da))]);
            disp(['Dyaw: ' num2str(std(Dyaw))]);
            disp(['Dpit: ' num2str(std(Dpit))]);
        end
        % assign to table
        ErrTab.X(ind)       = Dx(ind);
        ErrTab.Y(ind)       = Dy(ind);
        ErrTab.Roll(ind)    = Dpsi(ind);
        
        ErrTab.Yaw(ind)     = Dyaw(ind);
        ErrTab.Pitch(ind)   = Dpit(ind);
        
        if magord(magind)>0
            ErrTab.DK_K(ind,magord(magind)) = Da(ind);
        end
        
    elseif strcmp(r0{ind(1)}.Class,'Monitor')  % BPM
        
        % define errors
        Dx   = GroupErrors(dxalge(:,seed),mag_groups);
        Dy   = GroupErrors(dyalge(:,seed),mag_groups);
        Dxo  = randomdistrib(errampl(magind,1));
        Dyo  = randomdistrib(errampl(magind,2));
        Drot = randomdistrib(errampl(magind,3));
        Dxr  = errampl(magind,4);
        Dyr  = errampl(magind,4);
        Dxg  = randomdistrib(errampl(magind,5));
        Dyg  = randomdistrib(errampl(magind,6));
        
        if verb
            disp([fams{magind}{:}])
            disp(['Dx: ' num2str(std(Dx))]);
            disp(['Dy: ' num2str(std(Dy))]);
            disp(['Dox: ' num2str(std(Dxo))]);
            disp(['Doy: ' num2str(std(Dyo))]);
            disp(['Drx: ' num2str(std(Dxr))]);
            disp(['Dry: ' num2str(std(Dyr))]);
            disp(['Dgx: ' num2str(std(Dxg))]);
            disp(['Dgy: ' num2str(std(Dyg))]);
            disp(['Drot: ' num2str(std(Drot))]);
        end
        % assign to table
        ErrTab.X(ind)       = Dx(ind);
        ErrTab.Y(ind)       = Dy(ind);
        ErrTab.BPM_Offset(ind,1)  = Dxo(ind);
        ErrTab.BPM_Offset(ind,2)  = Dyo(ind);
        ErrTab.BPM_Reading(ind,1) = Dxr;
        ErrTab.BPM_Reading(ind,2) = Dyr;
        ErrTab.BPM_Gain(ind,1)    = Dxg(ind);
        ErrTab.BPM_Gain(ind,2)    = Dyg(ind);
        ErrTab.BPM_Rotation(ind)  = Drot(ind);
        
    else % drifts markers
        
        Dx   = GroupErrors(dxalge(:,seed),mag_groups);
        Dy   = GroupErrors(dyalge(:,seed),mag_groups);
        if verb
            disp([fams{magind}{:}])
            disp(['Dx: ' num2str(std(Dx))]);
            disp(['Dy: ' num2str(std(Dy))]);
        end
        ErrTab.X(ind)       = Dx(ind);
        ErrTab.Y(ind)       = Dy(ind);
       
    end
    
end

if verb
    writetable(ErrTab,['./SetErrors' num2str(seed) '.csv']);
end

%% set error table in lattice
rerr=atseterrortable(r0,ErrTab,'verbose',verb);


return