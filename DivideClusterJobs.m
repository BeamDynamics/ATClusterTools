function [jobsperproc]=DivideClusterJobs(njobs,nproc)
% divide njobs into nprocessors
%

div=njobs/nproc;
fdiv=floor(div);
cdiv=ceil(div);
frac=(div-fdiv);%max((div-fdiv),(cdiv-div));
npl=floor(nproc*frac);
nps=ceil(nproc*(1-frac));

if (npl+nps)<nproc
    npl=npl+1;
elseif (npl+nps)>nproc
    nps=nps-1;
end

disp([num2str(njobs) ' jobs for ' num2str(nproc) ' cores, divided as: ' ])
jobsperproc=[repmat(cdiv,npl,1);repmat(fdiv,nps,1)];

difj=sum(jobsperproc)-njobs;
if difj>0
    jobsperproc(end)=jobsperproc(end)+difj;
    disp([num2str(npl) ' long jobs with ' num2str(cdiv) ' processes ' ]);
    disp([num2str(nps-1) ' short jobs with ' num2str(fdiv) ' processes ' ]);
    disp([num2str(1) ' job with ' num2str(jobsperproc(end)) ' processes ' ]);
elseif difj<0
    jobsperproc(end)=jobsperproc(end)-difj;
    disp([num2str(npl) ' long jobs with ' num2str(cdiv) ' processes ' ]);
    disp([num2str(nps-1) ' short jobs with ' num2str(fdiv) ' processes ' ]);
    disp([num2str(1) ' job with ' num2str(jobsperproc(end)) ' processes ' ]);
else
    disp([num2str(npl) ' long jobs with ' num2str(cdiv) ' processes ' ]);
    disp([num2str(nps) ' short jobs with ' num2str(fdiv) ' processes ' ]);
end


return