function datafolder=RunFMAArray(...
    ringfile,... 1
    varringname,... 2
    Nturns,... 3
    Nsteps,... 4
    Nproc,... 5
    xm,... 6
    ym,... 7
    type,... 8
    griddingstyle,... 9
    tunemode, ... 10
    collect) ... 11
% function RunFMAArray(ringfile,varringname)
%
% created specification files to be used by FMapOARrunner
% and run the processes on the ESRF OAR cluster
% 
%     ringfile       : file where the AT lattice is stored
%     varringname    : name of AT lattice variable 
%
%     Nturns=2^11;    % total tracking turns
%     Nsteps=200;     % subdivisions of x and y ranges in the FMA  
%     Nproc=200;      % number of OAR processors to use. 
%     type='d-X';     % type: X-Y or d-X
%     xm=0.05;        % maximum x range. 
%                     %  Nsteps points will be studied between [-xm xm]    
%     ym=0.014;       % maximum y range. 
%                     % Nsteps points will be studied between 
%                     % [0 ym] (if type='X-Y') and 
%                     % [-ym ym] (if type='d-X') 
%     griddingstyle='linear'; % linear or quadratic
%     tunemode        % 'windowinterp' (default), highestpeak or interpolation
%
% routine used should be compiled with
% mcc('-vm','routinename','passmethodlist.m') 
% in the computer where you wish to use this function.
% ex: mcc('-vm','FMapOARrunner.m','passmethodlistfringes.m') 
% passmethodlistfringes.m contains the names of functions used by the
% routine but not explicitly called (as AT passmethods).
% 
% RunFMAArray runs OAR jobs as ARRAYS! 
% oarsub --array 4 ./get_host_info
% cat params.txt
%   Here we put the input parameters of the 1st subjob
%   Here we put the input parameters of the 2nd subjob
%   Here we put the input parameters of the 3rd subjob
% oarsub --array-param-file ./params.txt ./get_host_info
%
%see also: FrequencyMap FMapOARrunner FMADataCollector 
if nargin<11
    collect=false;
end
% this should be input.
routine='FMapClusterrunner'; % compiled routine file name.
%pathtoroutine='/mntdirect/_users/liuzzo/Matlab_Work/ATWORK/atwork-code/physics/Fmaps_findtunetom2'; % compiled routine path.

[pathtoroutine,~,exte]=fileparts(which(routine));

if ~exist(fullfile(pathtoroutine,[routine exte]),'file')
    error([routine ' does not exist in: ' pathtoroutine...
        ' . If this is not the correct directory,'...
        ' you may need to add the directory that contains'...
        routine ' to the path.']);
end

if nargin<10
    tunemode='windowinterp';
end
disp(['Tune mode: ' tunemode]);

%% LATTICE MANIPULATIONS
a=load(ringfile,varringname);
ring=a.(varringname);

switch type
    case 'X-Y'
        tflag='XY';
    case 'd-X'
        tflag='dX';
end

[~,ringfile]=fileparts(ringfile);

commonwork=...
    [ringfile '_nt' num2str(Nturns) '_ns'...
    num2str(Nsteps) '_' griddingstyle '_' tflag '_' tunemode];%
commonworkdir=fullfile(pwd,commonwork);%
mkdir(commonworkdir);

cd(commonworkdir);

datafolder=pwd;

% place here lattice file and RM file
save('latticefile.mat','ring');

%% define error maximum ranges

totprocnum=1;

workdir=pwd;
pfile=fopen('params.txt','w+');

switch griddingstyle
    case 'linear'
        % linear amplitude spacing
        switch type
            case 'X-Y'
                xx=linspace(-xm,xm,Nsteps*2); % x or delta
                yy=linspace(  0,ym,Nsteps  ); % y or x
            case 'd-X'
                xx=linspace(-xm,xm,Nsteps*2); % x or delta
                yy=linspace(-ym,ym,Nsteps*2); % y or x
        end
    case 'quadratic'
        % linear emittance scale
        betax=18;
        betay=3;
        xx=sqrt(betax*linspace(0,xm^2/betax,Nsteps));
        yy=sqrt(betay*linspace(0,ym^2/betay,Nsteps));
        switch type
            case 'X-Y'
                xx=[-xx,xx];
            case 'd-X'
                xx=[-xx,xx];
                yy=[-yy,yy];
        end
end

[xg,yg]=meshgrid(xx,yy);

zz=zeros(size(xg(:)));

switch type
    case 'X-Y'
        X6=[xg(:),zz,yg(:),zz,zz,zz]';
    case 'd-X'
        X6=[yg(:),zz,zz,zz,xg(:),zz]';
end
        
pts=length(xx)*length(yy); %  points to scan

[pointsperprocess]=DivideClusterJobs(pts,Nproc);

save('GlobalTolParameters.mat',...
    'Nsteps','Nturns','ringfile','varringname','xm','ym','type','Nproc',...
    'X6','pts','pointsperprocess','xx','yy','xg','yg');

%% LOOP FMA points
for indproc=1:Nproc %
    
    % prepare SpecsFile
    
    % do not store lattice and RM in this file. they are always the same.
    ringFile=fullfile(workdir,'latticefile.mat');                       %#ok<*NASGU> % AT lattice
    ringFileVarName='ring';                       % AT lattice
    numberofturns=Nturns;     % number of turns for tracking
    outfilename=fullfile(pwd,['outfile_' num2str(indproc,'%0.4d') '.mat']);


    if indproc<Nproc
        X0=X6(:,sum(pointsperprocess(1:indproc-1))+1:sum(pointsperprocess(1:indproc)));
    else %last proces takes left overs
        X0=X6(:,sum(pointsperprocess(1:indproc-1))+1:end);
    end
    
    spfn=['specfile_' num2str(indproc,'%0.4d') '.mat'];
    save(spfn,'ringFile','ringFileVarName','outfilename',...
        'type','X0','Nturns','tunemode', 'collect');
    
    % prepare Cluster script to run TolErrorSetEvaluatorClusterrunner
    % % % script=[' /opt/matlab_2013a ' fullfile(pwd,spfn) '\n'];
    script=[' /sware/com/matlab_2020a ' fullfile(pwd,spfn) '\n'];
    
    %fopen(fullfile(workdir,'params.txt'),'w+')
    fprintf(pfile,script');
    
    totprocnum=totprocnum+1;
    
end% END LOOP processes

routinetorun=[pathtoroutine '/run_' routine '.sh' ];

label=[ringfile '_St' num2str(Nsteps) '_P' num2str(Nproc) '_']; % label for files.

commands.job_name=label;
submit_matlab_to_slurm(routinetorun(1:end-3),'params.txt',commands)

cd('..') % back to Parent of Working Directory

disp([num2str(totprocnum) ' processes with label: ' label ' are running on the cluster']);

return
