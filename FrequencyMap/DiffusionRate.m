function [D,nux,nuy]=DiffusionRate(r,x0,nt,tunemode)
% function [D,nux,nuy]=DiffusionRate(r,x0,nt,tunemode)
% compute diffusion rates as:
%
% D=log(sqrt((nux(1,:)-nux(2,:)).^2+(nuy(1,:)-nuy(2,:)).^2));
%
% r is the at lattice
% x0 the vector of initial coordinate (6XN)
% nt the number of tracking turns. (tune is evaluated on nt/2)
% tunemode : 'highespeak', or 'interpolation' or 'windowinterp' (default)
%
% D is the diffusion rate
% nux is the horizontal tune
% nuy is the vertical tune
%
% to all x0 coordinate a small ofset of 1 um in x and y is added to compute
% the tune even if the coordinate is zero.
%
%see also: findtunetom3 ringpass

if nargin==3
    tunemode='windowinterp';
end

% add offset to garantee tune reconstruction
x0(1,:)=x0(1,:)+1e-6;
x0(3,:)=x0(3,:)+1e-6;

np=size(x0,2); % number of particles

tr=zeros(6,np*nt);

% track particles.
if ~isempty(findcells(r,'PassMethod','CavityPass'))
    
    for jj=1:np % splitting because ringpass seems not to behave correctly for multiple particles.
        tr(:,jj:np:end)=ringpass(r,x0(:,jj),nt);
    end
    
else
    disp('no cavity, using ringpassLimOk')
    
    for jj=1:np
        tr(:,jj:np:end)=ringpassLimOk(r,x0(:,jj),nt);
    end
    
end

%remove last if there are nan.
% nans=find(isnan(tr));
% nans(1)
% tr(:,nans(1):end)=[];% remove nan
% size(tr)
% nt=size(tr,2);


if nt>2
    switch tunemode
        case 'highestpeak'
            
            cc = 1;
        
        case 'interpolation'
            
            cc = 2;
            
        case 'windowinterp'
            
            cc = 3;
        otherwise
            error('not a good tune method: highestpeak, interpolation, windowinterp')
    end
    
    for ipa=1:np
        trpa=tr(:,ipa:np:end);% select one particle
        nux(1,ipa)=(findtune(trpa(1,1:nt/2)',cc));% the 3 meens  'window + interp.'.
        nux(2,ipa)=(findtune(trpa(1,nt/2+1:nt)',cc));
        nuy(1,ipa)=(findtune(trpa(3,1:nt/2)',cc));
        nuy(2,ipa)=(findtune(trpa(3,nt/2+1:nt)',cc));
    end
    
    % compute diffusion rate
    D=log(sqrt((nux(1,:)-nux(2,:)).^2+(nuy(1,:)-nuy(2,:)).^2));
    
else
    disp('not enough turns.')
    disp(nt)
    D=Inf;
    nux=NaN;
    nuy=NaN;
    
end

return

