function plotFrequencyMap(Dgrid,nx,ny,xg,yg,yy,xx,type)
% replace nan and inf for plot.
    Dgridsc(isnan(Dgrid))=max(Dgrid);
    Dgridsc(isinf(Dgrid))=max(Dgrid);
    
    % configuration space
    figure;
    pcolor(xg,yg,reshape(Dgrid,length(yy),length(xx)));
    colorbar;
    if strcmp(type,'X-Y')
        xlabel('x [m]');
        ylabel('y [m]');
    elseif strcmp(type,'d-X')
        xlabel('delta ');
        ylabel('x [m]');
    end
    % tune space
    figure;
    
    scatter(nx(1,:),ny(1,:),20,Dgridsc);
    
    colorbar;
    xlabel('nu x');
    ylabel('nu y');
    