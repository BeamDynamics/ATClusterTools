function [Dgrid,nx,ny,xg,yg,lost,xx,yy,type,M]=FrequencyMap(r,Nturns,type,xm,ym,nstep,DoPlot)
%function [Dgrid,nx,ny,xg,yg,lost]=FrequencyMap(r,Nturns,xm,ym,nstep)
%
% compute and plot Frequency maps
% 
% r, AT lattice
% Nturns number of turns
% type: string 'X-Y' or 'd-X'
% xm maximum range in X
% ym maximum range in Y
% nstep number of steps to make : 
%       nsteps   between -xm and xm 
%       nsteps/2 between   0 and ym
%       
% OUTPUT:
% 
% Dgrid: vector of diffision rates
% nx : vector of tunes 2Xlength(Dgrid) nx(1) is computed on the first half
% turns, nx(2) on the second half of turns
% ny : as nx in y plane
% [xg, yg] points on the configuration space grid
% lost: number of lost particles.
% xx : grid points
% yy : grid points
% type : string 'X-Y' or 'd-X'
% 
%see also: findtunetom2 DiffusionRate 


if nargin<3
    xm=0.01; % 10 cm
    ym=0.005; % 10 cm
    nstep=10; % 10 steps x and 5 in y
    DoPlot=1;
end

if strcmp(type,'X-Y')
    xx=linspace(-xm,xm,nstep);
    yy=linspace(  0,ym,nstep);
    
    [xg,yg]=meshgrid(xx,yy);
    
    zz=zeros(size(xg(:)));
    
    X0=[xg(:),zz,yg(:),zz,zz,zz]';
elseif strcmp(type,'d-X')
    xx=linspace(-xm,xm,nstep); % delta
    yy=linspace(  0,ym,nstep); % x
    
    [xg,yg]=meshgrid(xx,yy);
    
    zz=zeros(size(xg(:)));
    
    X0=[yg(:),zz,zz,zz,xg(:),zz]';
else
    error('Not recognized type. Possible values are: X-Y and d-X')
end

% compute diffusion rates.
[Dgrid,nx,ny]=DiffusionRate(r,X0,Nturns);

M=[X0(1,:);X0(3,:);nx(1,:);ny(1,:);Dgrid];

% get number of lost particles
lost=sum(isnan(Dgrid)|isinf(Dgrid));

% plot
if DoPlot
   plotFrequencyMap(Dgrid,nx,ny,xg,yg,xx,yy,type);
end


return
