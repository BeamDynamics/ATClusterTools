function FMapClusterrunner(inputdatafile)
%FMAPOARRUNNER( inputdatafile, outputdatafile)
%  
%   inputdatafile contains variables: 
%           ringFile           AT lattice file
%           ringFileVarName    AT lattice variable name
%           Nturns,     number of turns to track
%           type,       X-Y or d-X
%           X0          6XN vector of intial coordinate
%           nstep,      points in the range -x x
%           DoPlot      boolena to print out plot
%  
%   produces a file named outputdatafile that contains the variables:
%           D,  Diffusion rates
%           nx,     Tunes 2Xstudied points nx(1) first half turns nx(2) second
%           ny,     as nx
%           X0      6XN vector of intial coordinate
%           type,       X-Y or d-X
% 
%see also  FrequencyMap RunFMAArray

a=load(inputdatafile);

b=load(a.ringFile,a.ringFileVarName);
r=b.(a.ringFileVarName);
Nturns=a.Nturns;
type=a.type;
X0=a.X0;
%nstep=a.nstep;
%DoPlot=0;
outputdatafile=a.outfilename;
tunemode=a.tunemode;

%[Dgrid,nx,ny,xg,yg,lost,xx,yy,type]=FrequencyMap(r,Nturns,type,xm,ym,nstep,DoPlot);
[D,nx,ny]=DiffusionRate(r,X0,Nturns,tunemode);  %#ok<*NASGU,*ASGLU>

save(outputdatafile,'D','nx','ny','X0','type');
if a.collect && a.indproc==1
    CollectFMapClusterData(a.datafolder);
end


return
