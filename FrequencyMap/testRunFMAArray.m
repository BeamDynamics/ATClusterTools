
<<<<<<< HEAD
% this macro runs OAR jobs to compute a Frequency Map
% 
% 
% 
% to collect the data use mergeOARFmapData.m in the folder where the data
% is stored.
% 
% 

load('../S28Dmerged.mat')
ring=LOW_EMIT_RING_INJ;% using injection cell!

Nturns=2^10;    % total tracking turns
Nsteps=51;     % subdivisions of x and y ranges in the FMA  
Nproc=100;      % number of OAR processors to use. 
type='d-X';  %     % type: X-Y or d-X
xm=0.05;        % maximum x range. 
                %  Nsteps points will be studied between [-xm xm]    
ym=0.014;       % maximum y range. 
                % Nsteps points will be studied between 
                % [0 ym] (if type='X-Y') and 
                % [-ym ym] (if type='d-X') 
scale='linear'; % linear or quadratic

ringfile='LatticeForFMA.mat';
save(ringfile,'ring');

%%  

datafolder=RunFMAArray(...
    ringfile,...  % file where the AT lattice is stored
    'ring',...    % name of lattice variable
    Nturns,...
    Nsteps,...
    Nproc,...
    xm,...
    ym,...
    type,...
    scale,'interpolation'); %linear
waitoar=1;
integertunes=[76 27];
abovepointfive=0;
CollectFMapClusterData(datafolder,waitoar,'test1',integertunes,abovepointfive);


%%
type = 'X-Y';
xm=0.010;        % maximum x range. 
ym=0.005;       % maximum y range. 
   
datafolder=RunFMAArray(...
    ringfile,...  % file where the AT lattice is stored
    'LOW_EMIT_RING_INJ',...    % name of lattice variable
    Nturns,...
    Nsteps,...
    Nproc,...
    xm,...
    ym,...
    type,...
    scale,'windowinterp'); %linear
waitoar=1;
integertunes=[76 27];
abovepointfive=0;
[....
    M,...   % matrix of the following variables [x;y;nx;ny;D]'
    x_,...  % positions where the computations have been performed
    y_,...
    nx_,... % tunes (first half turns)
    ny_,...
    D_]=CollectFMapClusterData(datafolder,waitoar,'test2',integertunes,abovepointfive);


return
