function [....
    M,...   % matrix of the following variables [x;y;nx;ny;D]'
    x_,...  % positions where the computations have been performed
    y_,...
    nx_,... % tunes (first half turns)
    ny_,...
    D_]=... % diffusion rate
    CollectFMapClusterData(...
    datafolder,...      location of the data
    waitoar,...         0=do not wait for OAR to finish.
    outputfilename,...  'myfmap' output data is saved in this file
    integertunes,...    [75 27]
    aboveorbelowpointfive) %#ok<*STOUT>
%CollectFMapOARData
%
%  [...
%     M,...   % matrix of the following variables [x;y;nx;ny;D]'
%     x_,...  % positions where the computations have been performed
%     y_,...
%     nx_,... % tunes (first half turns)
%     ny_,...
%     D_]=... % diffusion rate
%     CollectFMapOARData(...
%     datafolder,...      location of the data
%     waitoar,...         0=do not wait for OAR to finish.
%     outputfilename,...  'myfmap' output data is saved in this file
%     integertunes,...    [75 27]
%     aboveorbelowpointfive) %#ok<*STOUT>
%
%see also: RunFMAArray

if nargin<2
    waitoar=1;
end

cd(datafolder)

a=dir(fullfile(datafolder,'specfile*.mat'));
totproc=length(a);
b=dir(fullfile(datafolder,'outfile*.mat'));
finproc=length(b);

% wait for processes to finish
while finproc~=totproc && waitoar==1
    
    b=dir(fullfile(datafolder,'outfile*.mat'));
    
    disp(['waiting for ' num2str(totproc-finproc) '/' num2str(totproc) ' processes to finish.'])
    
    % % % count runnning processes
    b=dir(fullfile(datafolder,'outfile*.mat'));
    finproc=length(b);
    
    % wait
    pause(10);
    
end

% delete OAR files
%delete('*.stdout');
%delete('*.stderr');

ag=load(fullfile(datafolder,'GlobalTolParameters.mat'));

x=[];
y=[];
nx=[];
ny=[];
D=[];
count=0;

for i=1:totproc
    
    ofn=['outfile_' num2str(i,'%04d') '.mat'];
    if exist(fullfile(datafolder,ofn),'file')
        a=load(fullfile(datafolder,ofn));
        
        %disp(i)
        count=count+1;
        switch ag.type
            case 'X-Y'
                x=[x,a.X0(1,:)];
                y=[y,a.X0(3,:)];
            case 'd-X'
                x=[x,a.X0(5,:)];
                y=[y,a.X0(1,:)];
        end
        nx=[nx,a.nx(1,:)];% take tune evaluate at first half turns
        ny=[ny,a.ny(1,:)];
        D=[D,a.D];
    else
        disp([ofn ' does not exist yet.']);
        disp(['Finished jobs: ' num2str(count)])
        % % failed jobs
        s = dir(pwd);
        index = regexp({s.name},'\w*.stderr');
        idx=cellfun(@(x)length(x),index);
        spw=arrayfun(@(a)a.bytes,s(idx~=0));% number of bytes in error file, if non zero, job failed
        countfail=length(spw(spw~=0));
        disp(['Failed jobs: ' num2str(countfail)])
        % total lunched jobs
        totjobs=length(spw);
        
        disp(['Still  running: ' num2str(totjobs-count-countfail) '/' num2str(totjobs)]);
        
        return
    end % if file exist.
    
end


if ~exist(fullfile(datafolder,[outputfilename,'.mat']))
    % matrix variables
    ag.Nsteps*2*ag.Nsteps;
    try
        x_=reshape(x,ag.Nsteps*2,ag.Nsteps);
        y_=reshape(y,ag.Nsteps*2,ag.Nsteps);
        nx_=reshape(nx,ag.Nsteps*2,ag.Nsteps);
        ny_=reshape(ny,ag.Nsteps*2,ag.Nsteps);
        D_=reshape(D,ag.Nsteps*2,ag.Nsteps);
    catch
        x_=reshape(x,ag.Nsteps*2,ag.Nsteps*2);
        y_=reshape(y,ag.Nsteps*2,ag.Nsteps*2);
        nx_=reshape(nx,ag.Nsteps*2,ag.Nsteps*2);
        ny_=reshape(ny,ag.Nsteps*2,ag.Nsteps*2);
        D_=reshape(D,ag.Nsteps*2,ag.Nsteps*2);
    end
    
    M=[x;y;nx;ny;D]';
    
    % delete Cluster files
    delete('*.stdout');
    delete('*.stderr');
    % delete spec and out files
    delete('spec*.mat');
    delete('out*.mat');
    if nargin==2
        outputfilename='AllData';
    end
    
    save(outputfilename,'M','x_','y_','nx_','ny_','D_');
end

if nargin>2
    if ~exist(fullfile(datafolder,[outputfilename,'.mat']),'file')
        
        save(outputfilename,'M','x_','y_','nx_','ny_','D_');
    else
        load(fullfile(datafolder,outputfilename));
        nx=nx_(:);
        ny=ny_(:);
        x=x_(:);
        y=y_(:);
        D=D_(:);
        
    end
    slidefigformat='.png';
    
    %c=pmkmp(256,'CubicYF');
    c=jet(256);
    %cinv=c(end:-1:1,:);
    cinv=c;
    scattersize=11;
    
    [~,titlelat]=fileparts(pwd);
    
    figure('name','FMA configuration space','numbertitle','off')
    sss=surf(x_,y_,D_);view(2);colorbar;shading flat;
    %scatter(x,y,scattersize,D,'s','filled');
    %set(gca,'clim',[4 20]*1e-3);
    colorbar;
    colormap(cinv);% Jet;
    set(gca,'FontSize',12,'FontWeight','b');
    caxis([-13 -1]);
    
    switch ag.type
        case 'X-Y'
            title('FMA configuration space','FontSize',12,'FontWeight','b');
            xlabel('x [m]','FontSize',12,'FontWeight','b');
            ylabel('y [m]','FontSize',12,'FontWeight','b');
            %axis(limits); axis  square;
            set(gca,'Layer','top');
            try
                export_fig(['FMAPConfSpace' titlelat '.jpg'],'-transparent')
            catch
                disp('no pdf');
            end
            saveas(gca,['FMAPConfSpace' titlelat '.fig']);
            
        case 'd-X'
            title('FMA delta-X','FontSize',12,'FontWeight','b');
            xlabel('delta','FontSize',12,'FontWeight','b');
            ylabel('x [m]','FontSize',12,'FontWeight','b');
            %axis(limits); axis  square;
            set(gca,'Layer','top');
            try
                export_fig(['FMAPdeltaXspace' titlelat '.jpg'],'-transparent')
            catch
                disp('no pdf');
            end
            saveas(gca,['FMAPdeltaXspace' titlelat '.fig']);
            
    end
    if nargin<3
        nx=nx+75; %+4;%
        ny=ny+27; %+1;%
    else
        if aboveorbelowpointfive==-1 % if below 0.5
            nx=1-nx;
            ny=1-ny;
        end
        nx=nx+integertunes(1);
        ny=ny+integertunes(2);
    end
    
    
    figure('name','FMA tune space','numbertitle','off')
    scatter(nx,ny,10,D,'s','filled');
    
    colorbar;
    colormap(cinv);% Jet;
    caxis([-13 -1]);
    set(gca,'FontSize',12,'FontWeight','b');
    title('FMA tune space','FontSize',12,'FontWeight','b');
    xlabel('\nu_x','FontSize',12,'FontWeight','b');
    ylabel('\nu_y','FontSize',12,'FontWeight','b');
    
    set(gca,'Layer','top');
    axis tight
    try
        export_fig(['FMAPTuneSpace' titlelat '.jpg'],'-transparent')
    catch
        disp('no pdf');
    end
    saveas(gca,['FMAPTuneSpace' titlelat '.fig']);
    
    hold off;
    tunediagLF([1:4],[min(nx) max(nx) min(ny) max(ny)],[1 1],16);% 16 is number of superpeiods
    hold on;
    scatter(nx,ny,3,D,'s','filled');
    try
        export_fig(['FMAPTuneSpaceResPlot' titlelat '.jpg'],'-transparent');
    catch
        disp('no pdf');
    end
    saveas(gca,['FMAPTuneSpaceResPlot' titlelat '.fig']);
    
    
end



cd ..

return
