function [dist,atdist]=atmkdist(numbpart,betax,alphax,epsx,betaz,alphaz,epsz,Edev,Blength,angle)
%[dist,atdist]=atmkdist(numbpart,betax,alphax,epsx,betaz,alphaz,epsz,Edev,Blength,angle)
%creats a particule distribution of NUMBPART particls with parameters:
%BETAX/Z ALPHAX/Z EPSX/Z Edev=rms relative energy deviation
%BLENGTH=rms bunch length in ct unite (m)
%angle allows to tilt the beam in the X-Z plane to simulate some
%coupling.(not very clean)
%all distributions are gaussian
randn('state',sum(100*clock))
xn=sqrt(epsx)*randn(numbpart,1);%defines the 2D gaussian distribution
randn('state',sum(530*clock))
dxn=sqrt(epsx)*randn(numbpart,1);

randn('state',sum(300*clock))
zn=sqrt(epsz)*randn(numbpart,1);%defines the 2D gaussian distribution
randn('state',sum(150*clock));
dzn=sqrt(epsz)*randn(numbpart,1);

randn('state',sum(100*clock))

ct=(Blength)*randn(numbpart,1);
randn('state',sum(530*clock));
dpp=(Edev)*randn(numbpart,1);

x=xn*sqrt(betax);
z=zn*sqrt(betaz);
dx=dxn/sqrt(betax)-alphax*x/(betax);
dz=dzn/sqrt(betaz)-alphaz*z/betaz;


%figure(2)
%subplot(2,1,1)
%hist(xn,50)
%subplot(2,1,2)
%hist(x,50)


xf=x*cos(angle)+z*sin(angle);
zf=z*cos(angle)-x*sin(angle);
dxf=dx*cos(angle)+dz*sin(angle);
dzf=dz*cos(angle)-dx*sin(angle);
dist=[xf dxf zf dzf];
atdist=[xf dxf zf dzf dpp ct]'; 
if numbpart==1
  x=sqrt(betax*epsx);
  z=sqrt(betaz*epsz);
  dx=sqrt(epsx)/sqrt(betax)-alphax*x/(betax);
dz=sqrt(epsz)/sqrt(betaz)-alphaz*z/betaz;
  
  atdist=[x dx z dz dpp Blength]';
end  