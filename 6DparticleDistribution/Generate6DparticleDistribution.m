function [part1coor]=Generate6DparticleDistribution(...
    ring,tl2,...
    obselement,npart,varargin)
% parameters: 
% ring  (booster lattice), 
% tl2 (transfer line booster to storage ring lattice), can be empty [] 
% varargin (bunch length, vertical emittance, seed), 
% obselement (element where you need the particles cloud in ring)
%
%see also: mvnrnd

if length(obselement)>1, 
    error('only one position in listelem');
    part1coor=[];
end

if ~isempty(findcells(ring,'PassMethod','CavityPass'))

    warning('off', 'all');
    [~, param] = atx(ring, 0, 1:length(ring));
    
    param.modemittance
    if isempty(varargin)
        emity = param.modemittance(2);
        sigmasratio = 1;
        seed=1;
    else
        if strcmp(varargin{1}, 'auto')
            sigmas = param.blength;
            sigmasratio = 1;
        else
            sigmas = varargin{1}; sigmasratio = varargin{1}/param.blength;
            
        end
        if strcmp(varargin{2}, 'auto')
            emity = param.modemittance(2);
        else
            emity = varargin{2};
        end
        if nargin>6
            seed = varargin{3};
        else
            seed=1;
        end
    end
    warning('on', 'all');

    [M66, Ts, co]    = findm66(atradoff(ring,''),obselement);
    
    if ~isempty(tl2)
        [~, Ts]    = findm66(tl2,length(tl2),co);
    else
        disp('no tl2 transport');
    end
    
    [G1, G2, G3] = find_inv_G( M66 );
    S22 = [0 1;-1 0];
    S66 = [S22 zeros(2,4); zeros(2,2) S22 zeros(2,2); zeros(2,4) S22];
    beam66_s0  = -S66 * ( param.modemittance(1)*G1 + emity*G2 + param.modemittance(3)*G3 ) * S66;

    beam66_s0(6,:) = sigmasratio * beam66_s0(6,:);
    beam66_s0(:,6) = sigmasratio * beam66_s0(:,6);
    listbeam66 = arrayfun( @(el) Ts(:,:,el) * beam66_s0 * Ts(:,:,el).', 1:length(obselement), 'Uniformoutput', false );
    
    % random generation at element el
    
    s = RandStream('mcg16807','Seed',seed);
    RandStream.setGlobalStream(s);
    
    part1coor = atbeam(npart,listbeam66{1},co);
    
    indrf = findcells(ring,'TimeLag');
    if ~isempty(indrf)
        tl = ring{indrf(1)}.TimeLag;
        part1coor(6,:) = part1coor(6,:) - tl;
    end
else
    disp('Please insert a cavity element in your ring.')
    part1coor=[];
end

end

% TEST
% test=atsetfieldvalues(ring, findcells(ring,'Class','RFCavity'),'PassMethod','CavityPass');
% test=atsetcavity(test, 7e6,0,992);
% dist=Generate6DparticleDistribution(test,[],10,200);figure;plot(dist(6,:));hold on
% test=atsetcavity(test, 7e6,1,992);
% dist=Generate6DparticleDistribution(test,[],10,200);plot(dist(6,:), 'r ');hold off
