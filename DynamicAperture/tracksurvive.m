function [xx,zz]=tracksurvive(ring,nt,pos1,pos2,dimension,dpp,X0offset)
%function [xx,zz]=tracksurvive(ring,nt,pos1,pos2,dim,dpp,X0offset)
% X0offset, to track DA off axis
%
% tracks particles descirbed by positions pos1 (x) and pos2 (y) vectors
% containing the coordinates of the dimension in dimension=[1-6,1-6]
% dimesion specify to wich cordinate pos2 and pos2 refer
%
% if the particle survive the final position are returned if not NaN are
% returned
%
% output xx and zz are the same size asn order as pos1 and pos2
%
%see also: ringpass

if isempty(pos2)
    dimension(2)=[]; %restrict to one dimension
end
ndim=length(dimension);

if ndim~=1 && ndim~=2
    error('1 or 2 dimensions are allowed')
end

if find([dimension>6, dimension<1])
    error('DIMENSION input must be a vector of length 1 or 2 with values between 1 and 6');
end

% closed orbit off momentum
if isnumeric(dpp)
    clorb=[findorbit4(ring(:),dpp);dpp;0];
else
    clorb=findorbit6(ring(:));
end

xx=zeros(size(pos1));
zz=xx;

for i=1:length(pos1)
    
    % assign position for tracking
    X0=zeros(6,1)+10e-6; % 10 um/urad in all directions
    X0(dimension(1))=pos1(i);
    if ndim==2
        X0(dimension(2))=pos2(i);
    end
    
    % perform tracking
    tr=ringpass(ring,clorb+X0offset+X0,nt);% tracking about closed orbit
    
    % assign final data
    xx(i)=tr(dimension(1),end);
    if ndim==2
        zz(i)=tr(dimension(2),end);
    else
        zz(i)=NaN;
    end
    
end

return