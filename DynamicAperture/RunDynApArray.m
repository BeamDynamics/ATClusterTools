function datafolder=RunDynApArray(...
    ringfile,...        1
    varringname,...     2
    Nturns,...          3
    Nsteps,...          4
    Nproc,...           5
    xm,...              6    
    ym,...              7
    type,...            8
    dimen,...           9
    dpp,...             10
    commands,...   11
    X0offset,...        12
    collect)           %13
% function RunDynApArray(ringfile,varringname)
% 
% created specification files to be used by DynApClusterrunner
% and run the processes on the ESRF cluster
% 
%     ringfile       : file where the AT lattice is stored
%     varringname    : name of AT lattice variable 
%
%     Nturns=2^11;    % total tracking turns
%     Nsteps=[20 20]; % subdivisions of x and y ranges in the Dyn. Ap.  
%     Nproc=200;      % number of Cluster processors to use. 
%     type='grid';    % type: 'grid' or 'radial' or 'radialnonlin'
%     dimen=[1,3];    % dimen: [1-6 ,1-6] or [1-6] % (in future 3D option)
%     xm=[-0.05 0.05];% dimen(1) range. 
%                     % Nsteps points will be studied between this limits    
%     ym=[0 0.003];   % maximum dimen(2) range. 
%                     % Nsteps points will be studied between this limits
%     dpp=0.00        % fixed energy deviation      
%     commands   % optional, string of comands for Cluster submision. 
%                     % default= '-l walltime=6 -q asd'
%                     % usefull:
%                     % -l walltime=72 (automtic que is nice not asd) 
%                     % -p "host like ''hpc2-01%%'' OR host like ''hpc2-02%%''"
%     X0offset        % 6x1 vector of coordinates to use as start point for
%                     % tracking
%     collect         % ture/false(default) autocollect in job number 1.
%
%
%
% the routine will wait for the cluster to finish the jobs and return a
% file with the resulting data
% outputfile contains:
% 
% 
% 
% 
% routine used should be compiled with
% mcc('-vm','routinename','passmethodlist.m') 
% in the computer where you wish to use this function.
% ex: mcc('-vm','DynApClusterrunner.m','passmethodlist.m') 
% passmethodlist.m contains the names of functions used by the
% routine but not explicitly called (as AT passmethods).
% 
% RunDynApArray runs Cluster jobs as ARRAYS! 
% 
%
%see also: tracksurvive DynApClusterrunner  

% this should be input.
routine='DynApClusterrunner'; % compiled routine file name.

[pathtoroutine,~,exte]=fileparts(which(routine));

if nargin<12
    X0offset=zeros(6,1);
end

if nargin<13
    collect=false;
end

if ~exist(fullfile(pathtoroutine,[routine exte]),'file')
     error([routine ' does not exist in: ' pathtoroutine...
        ' . If this is not the correct directory,'...
        ' you may need to add the directory that contains'...
        routine ' to the path.']);
end

%% LATTICE MANIPULATIONS
a=load(ringfile,varringname);
ring=a.(varringname);
tflag=type;
 
[~,ringfile]=fileparts(ringfile);

commonwork=...
    [ringfile '_' varringname '_nt' num2str(Nturns) '_ns'...
    num2str(Nsteps,'%.4d_') '_dim' num2str(dimen,'%d_') ...
    '_dpp' strrep(num2str(dpp),'.','p') ...
    '_' tflag];%
commonworkdir=fullfile(pwd,commonwork);%
mkdir(commonworkdir);

cd(commonworkdir);

datafolder=pwd;

% place here lattice file and RM file
save(fullfile(datafolder,'latticefile.mat'),'ring');

%% define error maximum ranges

totprocnum=1;

pfile=fopen(fullfile(datafolder,'params.txt'),'w+');
xx=[];
yy=[];
th=[];
rh=[];

% define grid of points to check for dynamic aperture
switch type
    
    case 'grid' %linear grid of points
        xx=linspace(xm(1),xm(2),Nsteps(1)); % x
        yy=linspace(ym(1),ym(2),Nsteps(2)); % y
        [xg,yg]=meshgrid(xx,yy);
        X6=[xg(:),yg(:)]'+10e-6; % add ten micron to all points
        pts=length(xx)*length(yy); %  points to scan

    case 'radial' % polar grid of points. 
        [thm,rhm]=cart2pol([xm xm],[ym(1) ym(1) ym(2) ym(2)]);
        th=linspace(0,max(thm),Nsteps(1)); % theta
        rh=linspace(0,max(rhm),Nsteps(2)); % rho
        [thg,rhg]=meshgrid(th,rh);
        [xg,yg]=pol2cart(thg(:),rhg(:));
        if ym(1)>=0
        X6=[xg,yg.*((ym(2)-ym(1))/max(rhm))]';%
        else % negative y range but radius only positive.
        X6=[xg,yg.*((ym(2)-ym(1))/2/max(rhm))]'+100e-6; % add ten micron to all points%
        end
        pts=length(th)*length(rh); %  points to scan
        
    case 'radialnonlin' % polar grid of points.
        [thm,rhm]=cart2pol([xm xm],[ym(1) ym(1) ym(2) ym(2)]);
        th=linspace(0,max(thm),Nsteps(1)); % theta
        rh=linspace(0,max(rhm),Nsteps(2)); % rho
        
        % quadratic scaling of distances.
        rhsq=sqrt(linspace(0,max(rh)^2,Nsteps(2)));
        
        [thg,rhg]=meshgrid(th,rhsq);
        [xg,yg]=pol2cart(thg(:),rhg(:));
        if ym(1)>=0
        X6=[xg,yg.*((ym(2)-ym(1))/max(rhm))]';%
        else % negative y range but radius only positive.
        X6=[xg,yg.*((ym(2)-ym(1))/2/max(rhm))]'+100e-6; % add ten micron to all points;%
        end
        pts=length(th)*length(rh); %  points to scan

end


[pointsperprocess]=DivideClusterJobs(pts,Nproc);

save(fullfile(datafolder,'GlobalTolParameters.mat'),...
    'Nsteps','Nturns','ringfile','varringname','xm','ym','type','Nproc',...
    'X6','pts','pointsperprocess','xx','yy','xg','yg');

%% LOOP Dynamic Aperture points
for indproc=1:Nproc %
    
    % prepare SpecsFile
    
    % do not store lattice and RM in this file. they are always the same.
    ringFile=fullfile(datafolder,'latticefile.mat');                       %#ok<*NASGU> % AT lattice
    ringFileVarName='ring';                       % AT lattice
    numberofturns=Nturns;     % number of turns for tracking
    outfilename=fullfile(pwd,['outfile_' num2str(indproc,'%0.4d') '.mat']);
    

    if indproc<Nproc
        X0=X6(:,sum(pointsperprocess(1:indproc-1))+1:sum(pointsperprocess(1:indproc)));
    else %last proces takes left overs
        X0=X6(:,sum(pointsperprocess(1:indproc-1))+1:end);
    end
    
    spfn=['specfile_' num2str(indproc,'%0.4d') '.mat'];
    save(fullfile(datafolder,spfn),'ringFile','ringFileVarName','outfilename',...
        'type','X0','Nturns','dimen','dpp','indproc','collect','datafolder','X0offset');
    
    % prepare Cluster script to run DynApClusterrunner
    script=[' /sware/com/matlab_2020a ' fullfile(pwd,spfn) '\n'];
    
    %fopen(fullfile(datafolder,'params.txt'),'w+')
    fprintf(pfile,script');
    
    totprocnum=totprocnum+1;
    
end% END LOOP processes
totprocnum=totprocnum-1;% remove last update of totprocnum

routinetorun=[pathtoroutine '/run_' routine '.sh' ];
            
save(fullfile(datafolder,'GlobalTolParameters.mat'),...
    'Nsteps','Nturns','ringfile','varringname','dpp','xm','ym','type','Nproc',...
    'X6','pts','pointsperprocess','xx','yy','xg','yg','totprocnum');

% hpc
 label=[ringfile '_St' num2str(Nsteps,'%.4d_') '_P' num2str(Nproc) '_']; % label for files.

if nargin<11
   commands = struct();
   commands.time = num2str(12*60);
   commands.partition='nice';
end

fclose('all');

% run on slurm
% commands.time=num2str(72*60); % time in minutes
commands.job_name=label;
submit_matlab_to_slurm(routinetorun(1:end-3),'params.txt',commands)

cd('..') % back to Parent of Working Directory

disp([num2str(totprocnum) ' processes with label: ' label ' are running on slurm ']);

return