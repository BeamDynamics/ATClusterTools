function DynApClusterrunner(inputdatafile)
%DynApClusterrunner( inputdatafile, outputdatafile)
%
%  this is the function evaluated by each single process on the Cluster
%
%   inputdatafile contains variables:
%           ringFile            AT lattice file
%           ringFileVarName     AT lattice variable name
%           Nturns,             number of turns to track
%           dimen,               [1-6, 1-6] dimensions to scan
%           type,               'grid','radial'
%           X0                  2XN vector of intial coordinate
%           dpp                 fixed value off energy computation
%           outputdatafile      file name for output
%
%   produces a file named outputdatafile that contains the variables:
%           xx          1XN final dimen(1) coordinate, NaN if lost
%           zz          1XN final dimen(2) coordinate, NaN if lost
%           Nturns,     number of turns to track
%           dimen,       [1-6, 1-6] dimensions to scan
%           X0          2XN vector of intial coordinate
%           dpp         fixed value off energy computation
%
%see also: tracksurvive RunDynApArray

a=load(inputdatafile);

b=load(a.ringFile,a.ringFileVarName);
ring=b.(a.ringFileVarName);
Nturns=a.Nturns;
type=a.type;
dimen=a.dimen;
X0=a.X0;
dpp=a.dpp;

outputdatafile=a.outfilename;

[xx,zz]=tracksurvive(ring,Nturns,X0(1,:),X0(2,:),dimen,dpp,a.X0offset); %#ok<*NASGU,ASGLU>

save(outputdatafile,'xx','zz','X0','dimen','type','dpp','Nturns');
if a.collect && a.indproc==1
    CollectDynApClusterData(a.datafolder,'Results.mat');
end
end
