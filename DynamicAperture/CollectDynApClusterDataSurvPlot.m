function [xa,ya,survived]=CollectDynApClusterDataSurvPlot(datafolder)
%
% [x,y,survived]=CollectDynApClusterDataSurvPlot(datafolder)
% 
% datafolder: folder of data generated with RunDynApArray
% outputfilename: file where to save the data
% x,y : grid points for particles survived
% to plot DA: plot(xc,yc,'x-');
% 
%see also: RunDynApArray

cd(datafolder)

a=dir('specfile*.mat');
totproc=length(a);
b=dir('outfile*.mat');
finproc=length(b);

% wait for processes to finish
while finproc~=totproc
    b=dir('outfile*.mat');

    disp(['waiting for ' num2str(totproc-finproc) '/' num2str(totproc) ' processes to finish.'])
    
    % % % count runnning processes
    b=dir('outfile*.mat');
    finproc=length(b);

    % wait
    pause(10);
    
end

% delete Cluster files
%delete('*.stdout');
%delete('*.stderr');

load('GlobalTolParameters.mat','type','Nsteps');
xa=[];
ya=[];
notok=[];
for iproc=1:totproc

    load(['outfile_' num2str(iproc,'%0.4d') '.mat'],'xx','zz','X0');
    notok=[notok (isnan(xx) | isnan(zz))];
    xa=[xa X0(1,:)];
    ya=[ya X0(2,:)];
    
end

survived=notok==0;

cd ..

if nargout>0
% remove data direcotry
system(['rm -r ' datafolder]); 
end


return