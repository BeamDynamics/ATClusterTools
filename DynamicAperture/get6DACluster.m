function df=get6DACluster(rcor,foldlabel,limdim,Clusterspec,Nproc)
% 
% computed Dynamic aperture x-y , x-x' y y' ct-dpp x'-y' dpp-x using ASD cluster
% 
%see also:  RunDynApArray CollectDynApClusterData

Nturns=2^10;

if nargin<3
limdim=[1e-2 1e-3 1e-2 1e-3 0.01 0.3]; % limits in all dimensions for scan
end

if nargin<4
    Clusterspec='-l walltime=2 -q nice';
end

if nargin<5
Nproc=50;
end

InjIndex=findcells(rcor,'FamName','S3_Septum')+1;
if isempty(InjIndex)
    InjIndex=1;
end
rcor=atrotatelattice(rcor,InjIndex);

latfilename=[foldlabel '.mat'];
save(latfilename,'rcor');


tic;
%% x - y

datafolderXY=RunDynApArray(...
    latfilename,...'latfile.mat',...
    'rcor',...
    Nturns,...         % number of turns
    [50 50],...    % steps are theta and radial
    Nproc,...          % number of processes on Cluster
    [-limdim(1) 0],...% dimension(1) limits
    [-limdim(3) limdim(3)],...    % dimension(2) limits
    'grid',...'radialnonlin',...    % grid in polar coordinate
    [1 3],...       % dimensions to scan
    0.0,...            % dpp
    Clusterspec);           % Cluster specifcation

%% x - x'

datafolderXXp=RunDynApArray(...
    latfilename,...'latfile.mat',...
    'rcor',...
    Nturns,...         % number of turns
    [50 50],...    % steps are theta and radial
    Nproc,...          % number of processes on Cluster
    [-limdim(1) 0 ],...% dimension(1) limits
    [-limdim(2) limdim(2)],...    % dimension(2) limits
    'grid',...'radialnonlin',...    % grid in polar coordinate
    [1 2],...       % dimensions to scan
    0.0,...            % dpp
    Clusterspec);           % Cluster specifcation

%% y - y'

datafolderYYp=RunDynApArray(...
    latfilename,...'latfile.mat',...
    'rcor',...
    Nturns,...         % number of turns
    [50 50],...    % steps are theta and radial
    Nproc,...          % number of processes on Cluster
    [-limdim(3) limdim(3)],...% dimension(1) limits
    [-limdim(4) limdim(4)],...    % dimension(2) limits
    'grid',...'radialnonlin',...    % grid in polar coordinate
    [3 4],...       % dimensions to scan
    0.0,...            % dpp
    Clusterspec);           % Cluster specifcation

%% ct delta
datafolderCtD=RunDynApArray(...
    latfilename,...'latfile.mat',...
    'rcor',...
    Nturns,...         % number of turns
    [50 50],...    % steps are theta and radial
    Nproc,...          % number of processes on Cluster
    [-limdim(6) limdim(6)],...% dimension(1) limits
    [-limdim(5) limdim(5)],...    % dimension(2) limits
    'grid',...'radialnonlin',...    % grid in polar coordinate
    [6 5],...       % dimensions to scan
    0.0,...            % dpp
    Clusterspec);           % Cluster specifcation

%% x' - y'
datafolderXpYp=RunDynApArray(...
    latfilename,...'latfile.mat',...
    'rcor',...
    Nturns,...         % number of turns
    [50 50],...    % steps are theta and radial
    Nproc,...          % number of processes on Cluster
    [-limdim(2) limdim(2)],...% dimension(1) limits
    [-limdim(4) limdim(4)],...    % dimension(2) limits
    'grid',...'radialnonlin',...    % grid in polar coordinate
    [2 4],...       % dimensions to scan
    0.0,...            % dpp
    Clusterspec);           % Cluster specifcation



%% x - d
datafolderXD=RunDynApArray(...
    latfilename,...'latfile.mat',...
    'rcor',...
    Nturns,...         % number of turns
    [50 50],...    % steps are theta and radial
    Nproc,...          % number of processes on Cluster
    [-limdim(5) limdim(5)],...% dimension(1) limits
    [-limdim(1) 0],...    % dimension(2) limits
    'grid',...'radialnonlin',...    % grid in polar coordinate
    [5 1],...       % dimensions to scan
    0.0,...            % dpp
    Clusterspec);           % Cluster specifcation



% delete lattice file used for tracking
delete(latfilename);

df={datafolderXY,datafolderXXp,datafolderYYp,datafolderCtD,datafolderXpYp,datafolderXD};

[x,y,survived]=CollectDynApClusterDataSurvPlot(datafolderXY);
save(['DAXYdata' latfilename],'x','y','survived');
[x,y,survived]=CollectDynApClusterDataSurvPlot(datafolderXXp);
save(['DAXXpdata' latfilename],'x','y','survived');
[x,y,survived]=CollectDynApClusterDataSurvPlot(datafolderYYp);
save(['DAYYpdata' latfilename],'x','y','survived');
[x,y,survived]=CollectDynApClusterDataSurvPlot(datafolderCtD);
save(['DACtDdata' latfilename],'x','y','survived');
[x,y,survived]=CollectDynApClusterDataSurvPlot(datafolderXpYp);
save(['DAXpYpdata' latfilename],'x','y','survived');
[x,y,survived]=CollectDynApClusterDataSurvPlot(datafolderXD);
save(['DAXDdata' latfilename],'x','y','survived');
toc;
    %rmdir(datafolder);
    %rmdir(datafolderdpp);
