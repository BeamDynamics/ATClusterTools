function [x,y,dd,xd,datafolder,datafolderdpp]=getDACluster(rcor,foldlabel,maxX,maxDPP,maxY,clusterspec,Nproc)
% 
% computed Dynamic aperture x-y  and dpp-x using ASD cluster
% 
%see also:  RunDynApArray CollectDynApClusterData

if nargin<7
Nproc=50;
end

Nturns=2^10;

if nargin<3
    maxX=0.017;
    maxDPP=0.10;
    maxY=0.008;
end
if nargin<6
    clusterspec=struct();
    clusterspec.time = num2str(12*60);
end

latfilename=[foldlabel '.mat'];
save(latfilename,'rcor');

tic;
datafolderdpp=RunDynApArray(...
    latfilename,...'latfile.mat',...
    'rcor',...
    Nturns,...         % number of turns
    [60 60],...       % steps in dimension(1) and dimension(2)
    Nproc,...          % number of processes on Cluster
    [-maxDPP maxDPP],...% dimension(1) limits
    [0 maxX],...    % dimension(2) limits
    'grid',...      % grid in cartesian coordinates
    [5 1],...       % dimensions to scan
    0.0,...            % dpp
    clusterspec);

datafolder=RunDynApArray(...
    latfilename,...'latfile.mat',...
    'rcor',...
    Nturns,...         % number of turns
    [33 60],...    % steps are theta and radial
    Nproc,...          % number of processes on Cluster
    [-maxX maxX],...% dimension(1) limits
    [0 maxY],...    % dimension(2) limits
    'radial',...'radialnonlin',...    % grid in polar coordinate
    [1 3],...       % dimensions to scan
    0.0,...            % dpp
    clusterspec);           % Cluster specifcation

% delete lattice file used for tracking
delete(latfilename);

x=NaN;
y=NaN;
dd=NaN;
xd=NaN;

if nargout==4
    [x,y,a,b,c,d]=CollectDynApClusterData(datafolder);
    [dd,xd,a_d,b_d,c_d,d_d]=CollectDynApClusterData(datafolderdpp);
    toc;
    save(['DAdata' latfilename],'x','y','dd','xd','a','b','c','d','a_d','b_d','c_d','d_d');
    %rmdir(datafolder);
    %rmdir(datafolderdpp);
end
