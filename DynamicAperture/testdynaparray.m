% test RunDynApArray.m
collect=true;
% tic;
commands=struct();
commands.time = num2str(12*60);
commands.partition='nice';

datafolder=RunDynApArray(...
    '../S28Dmerged.mat',...
    'LOW_EMIT_RING_INJ',...
    512,...         % number of turns
    [50 50],...       % steps in dimension(1) and dimension(2)
    50,...          % number of processes on Cluster
    [-0.03 0.03],...% dimension(1) limits
    [0 0.008],...    % dimension(2) limits
    'grid',...      % grid in cartesian coordinates
    [1 3],...       % dimensions to scan
    0.0,...
    commands,...
    [0 0 0 0 0 0]',...
    collect);            % dpp
% CollectDynApClusterData(datafolder,fullfile(datafolder,'../testonenergy.mat'));
%rmdir(datafolder);
% toc;

